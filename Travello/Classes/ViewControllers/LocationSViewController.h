//
//  LocationSViewController.h
//  Travello
//
//  Created by Balbir Shokeen on 2013/08/31.
//  Copyright (c) 2013 Balbir Shokeen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CommonViewController.h"
#import "LocationsCell.h"
#import "HelpCell.h"
#import "LocationViewController.h"
#import "CoreDataOperations.h"
#import "BVReorderTableView.h"
#import "HelpBubble.h"

@protocol LocationSViewControllerDelegate;

// Help running this view for selection or editing and going further to detail ui
typedef enum { SingleSelection=1, MultipleSelection=2, EditSingleSelection=3, ManualEntrySingleSelection=4 } LocationRunInMode;
// Type of Location Field being edited
typedef enum { NotSet=0, FromLocation=1, ToLocation=2, SelectedLocation=3 } LocationType;




@interface LocationSViewController : CommonViewController <LocationViewControllerDelegate, UIAlertViewDelegate, ReorderTableViewDelegate>
{
    HelpBubble *helpView;
    NSString *helpString;
}

// Delegate added to pass on completion messages if requried
@property (weak, nonatomic) id <LocationSViewControllerDelegate> delegate;
@property (strong, nonatomic) Location *selectedLocation;
@property (nonatomic) LocationRunInMode runInMode;

@property (nonatomic) LocationType locationType;
- (IBAction)AddLocationActionBtn:(id)sender;

@end


@protocol LocationSViewControllerDelegate <NSObject>

- (void) locationSViewControllerDidCancel: (LocationSViewController *) controler;
- (void) locationSViewControllerDidFinish: (LocationSViewController *) controller OnLocation:(Location *)location LocationType: (int) type;

@end
