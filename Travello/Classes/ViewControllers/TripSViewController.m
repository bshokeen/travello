//
//  TripSViewController.m
//  Travello
//
//  Created by Balbir Shokeen on 2014/01/27.
//  Copyright (c) 2014 Balbir Shokeen. All rights reserved.
//

//
//  RoutesLogViewController.m
//  Travello
//
//  Created by Balbir Shokeen on 2013/12/04.
//  Copyright (c) 2013 Balbir Shokeen. All rights reserved.
//

#import "TripSViewController.h"
#import "Toast+UIView.h"

@interface TripSViewController ()

@property bool helpAlertAppeardOnce;

@property NSIndexPath *tmpIndxPathToShareBetAPI; // To pass indexp between apis
@property Trip *tmpLogVar; // To hold the value when passing it between API calls

@end

@implementation TripSViewController

@synthesize helpAlertAppeardOnce = _helpAlertAppeardOnce, tmpIndxPathToShareBetAPI = _tmpIndxPathToShareBetAPI, tmpLogVar = _tmpLogVar;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Inform Flurry
    [Flurry logEvent:@"Tab Access: Trip"];
    
    self.thisInterfaceEntityName = @"Trip";
    
    self.tableView.sectionHeaderHeight = HEADER_HEIGHT; // Setting a custom height to get proper height to have a visible notice of the section header.
    
    // Search Bar Handling: Show Search Bar as hidden in the begining
    //    self.uiSearchBar = [[UISearchBar alloc] initWithFrame:CGRectMake(0, 0, 320, 44)];
    self.uiSearchBar.showsCancelButton = NO;
    self.uiSearchBar.delegate = self;
    self.tableView.tableHeaderView = self.uiSearchBar;
    
}

-(void) viewWillAppear:(BOOL)animated {
    // Localized the Cancel Button as for this app custom language it was appearing in english
    [[UIButton appearanceWhenContainedIn:[UISearchBar class], nil]
     setTitle:NSLocalizedString(@"Cancel", @"Cancel") forState:UIControlStateNormal];
    
    //    [self.tableView setContentOffset:CGPointMake(0, 44)]; // Bug Fix: It was difficult to know where the user was last if we reset the focus every time.
    // test: [self showPopTipView];
}

-(void) viewWillDisappear:(BOOL)animated {
    
    [self.uiSearchBar resignFirstResponder];
    
}

// Hide the keyboard
-(void) searchBarCancelButtonClicked:(UISearchBar *)searchBar {
    self.uiSearchBar.text = @"";
    
    //    [self.tableView setContentOffset:CGPointMake(0, 44) animated:YES]; // Tried few times and saw -20 works fine
    // On landscape and other mode it did not work well so ignored using it for now
    [searchBar resignFirstResponder];
    
    self.uiSearchBar.showsCancelButton=NO;
    [self forceReloadThisUI];
    NSLog(@"SearchBarCanced");
}

- (void) searchBarTextDidBeginEditing:(UISearchBar *)theSearchBar {
    //    ULog(@"BeginEditing");
    
    if (!self.uiSearchBar.showsCancelButton) {
        self.uiSearchBar.showsCancelButton = YES;
    }
}


-(void) searchBar:(UISearchBar *) searchBar textDidChange:(NSString *)Tosearch{
    
    [self forceReloadThisUI];
}


- (void) forceReloadThisUI
{
    // Set Nil so that during fetch it goes and fetch fresh data.
    self.fetchedResultsControllerObj = nil;
    
    // Need to see if I really need it all places to reload.
    [self.tableView reloadData];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"TripSCell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    // call to update the cell details
    [self configureCell:cell atIndexPath:indexPath];
    return cell;
}


- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    
    id <NSFetchedResultsSectionInfo> sectionInfo = [[self.fetchedResultsController sections] objectAtIndex:section];
    return [sectionInfo name];
}

-(UIView*)tableView:(UITableView*)tableView viewForHeaderInSection:(NSInteger)section {
    NSLog(@"View Header In Section:%i   Start", (int)section);
    
    id <NSFetchedResultsSectionInfo> sectionInfo = [[self.fetchedResultsController sections] objectAtIndex:section];
    NSString *sectionName =  [sectionInfo name];
    if (sectionName == nil) {
        sectionName = @"Test";
    }
    NSInteger sectionItemCount = [sectionInfo numberOfObjects];
    
    // Initialize custom header section view using custom prototype cell
    
    RouteSectionHeaderView *sectionHeaderView = [[RouteSectionHeaderView alloc]
                                                 initWithFrame:CGRectMake(0.0, 0.0, self.tableView.bounds.size.width, self.tableView.bounds.size.height)                                                                                              title:sectionName
                                                 section:section
                                                 sectionItemCount:sectionItemCount
                                                 HeaderIndex:(section + 1)
                                                 TagNumber: (SECTION_NUMBER_STARTFROM_ROUTE + section) //// To identify a group section for updates later
                                                 delegate:self];
    
    NSLog(@"viewForHeaderInSection: %i   ItemCount:%i End", (int)section, (int)sectionItemCount);
    return sectionHeaderView;
}


/* Disable Section Index
 - (NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView {
 return [self.fetchedResultsController sectionIndexTitles];
 }
 
 - (NSInteger)tableView:(UITableView *)tableView sectionForSectionIndexTitle:(NSString *)title atIndex:(NSInteger)index {
 return [self.fetchedResultsController sectionForSectionIndexTitle:title atIndex:index];
 }
 */

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    UITableViewCell * cell = [self.tableView  cellForRowAtIndexPath:indexPath];
    [cell setSelected:NO animated:YES];
    
    if (indexPath.section==0 && indexPath.row == 0) { // Adding setting the first cell values as during scroll the values were wiping out. Need to see the core reason using it as workaround for now
        TripCell *leCell = (TripCell *) cell;
        leCell.startLbl.text = @"Started";
        leCell.aroundLbl.text = @"Around";
    }
    /* Disable going to any other view as it is not functional as of now.
    // Switch Cell Selection to take you to editing instead of Location
    [self performSegueWithIdentifier:@"ShowRouteViewController" sender:self];
     */
    
}

- (void)tableView:(UITableView *)tableView1 accessoryButtonTappedForRowWithIndexPath:(NSIndexPath *)indexPath {
    self.tmpIndxPathToShareBetAPI = indexPath;
    [self performSegueWithIdentifier:@"ShowManualEntryViewController" sender:self];
}


#pragma mark - Segue management
- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    /*
    if ([[segue identifier] isEqualToString:@"ShowRouteViewController"]) {
        
        RouteViewController *controller = (RouteViewController *)[segue destinationViewController];
        
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        
        Trip *trip = (Trip *)[[self fetchedResultsController] objectAtIndexPath:indexPath];
        
        // to get the events, had to set the delegate as self. Set the location we want to view
        controller.delegate = self;
        controller.trip = trip;
        controller.routeVCRunInMode = EDIT;
        
        // Perform deselection to avoid setting last selection when pushing from add button
        [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
    }
    else */
    if ( [[segue identifier] isEqualToString:@"ShowLocationSViewController_ManualEntry"]) {
        LocationSViewController *controller = (LocationSViewController *)[segue destinationViewController];
        
        // to get the events, had to set the delegate as self. Set the location we want to view
        controller.delegate = self;
        controller.runInMode = ManualEntrySingleSelection;
    }
    else if ( [[segue identifier] isEqualToString:@"ShowManualEntryViewController"]) {
        /*
         ManualEntryViewController *controller = (ManualEntryViewController *)[segue destinationViewController];
         
         LocationLog *locationLog = (LocationLog *)[[self fetchedResultsController] objectAtIndexPath:self.tmpIndxPathToShareBetAPI];
         
         controller.selectedLocationLog = locationLog;
         */
        
        [segue destinationViewController];
        //        ManualEntryViewController *controller = (ManualEntryViewController *)[segue destinationViewController];
        //        controller.selectedLocationLog = self.tmpLogVar; // NOT SET YET
    }
}
#pragma mark - Fetched results controller

- (NSFetchedResultsController *)fetchedResultsController
{
    if (self.fetchedResultsControllerObj != nil) {
        return self.fetchedResultsControllerObj;
    }
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    // Edit the entity name as appropriate.
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Trip" inManagedObjectContext:APP_DELEGATE_MGDOBJCNTXT];
    [fetchRequest setEntity:entity];
    
    // Set the batch size to a suitable number.
    [fetchRequest setFetchBatchSize:20];
    
    // Edit the sort key as appropriate.
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"created_on" ascending:NO];
    NSArray *sortDescriptors = @[sortDescriptor];
    
    [fetchRequest setSortDescriptors:sortDescriptors];
    
    
    // Filter Search Text: In task name and notes
    if ([self.uiSearchBar.text length] > 0 ) {
        NSPredicate *searchPredicate = [NSPredicate predicateWithFormat:@"fromLocation.name CONTAINS[cd] %@ || toLocation.name CONTAINS[cd] %@ ", self.uiSearchBar.text, self.uiSearchBar.text];
        [fetchRequest setPredicate:searchPredicate];
    }
    
    // Edit the section name key path and cache name if appropriate.
    // nil for section name key path means "no sections".
    NSFetchedResultsController *aFetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest managedObjectContext:APP_DELEGATE_MGDOBJCNTXT sectionNameKeyPath:@"sectionName" cacheName:nil];
    aFetchedResultsController.delegate = self;
    self.fetchedResultsControllerObj = aFetchedResultsController;
    
	NSError *error = nil;
	if (![self.fetchedResultsController performFetch:&error]) {
        // Replace this implementation with code to handle the error appropriately.
        // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
	    NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
	    abort();
	}
    
    // Show a toast alert if applicable (no location monitored and not shown an alert already
    if (self.fetchedResultsControllerObj != nil) {
        NSUInteger count = [[self.fetchedResultsControllerObj fetchedObjects] count];
        if ( count == 0 && !self.helpAlertAppeardOnce && [self.uiSearchBar.text length]==0) { // added searhbar len else message was coming during record filter
            
            /*
             // Make toast with an image
             [self.view makeToast:@"Entries would appear automatically on ENTER or EXIT from configured locations."
             duration:15.0
             position:@"center"
             image:nil];
             */
            
            [AppUtilities showSimpleAlertWithMessage:@"Please leave the app running in background!!!\n\nYou will see your trip duration from a configured location to another with your device moving around them." Title:@"IMPORTANT"];
            
            self.helpAlertAppeardOnce = YES;
        }
    }
    return aFetchedResultsController;
}

- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    // Configure the cell to show the Tasks's details
    TripCell *leCell = (TripCell *) cell;
    [leCell resetInitialText];
    
    // For simplicity of UI clear few static labels
    if (indexPath.row != 0) {
        leCell.startLbl.text  = @"";
        leCell.aroundLbl.text = @"";
    }
    
    Trip *trip  = (Trip *)[self.fetchedResultsController objectAtIndexPath:indexPath];
    
    static NSDateFormatter *formatterDate = nil;
    if (formatterDate == nil) {
        formatterDate = [[NSDateFormatter alloc] init];
        [formatterDate setDateStyle:NSDateFormatterMediumStyle];
    }
    static NSDateFormatter *formatterTime = nil;
    if (formatterTime == nil) {
        formatterTime = [[NSDateFormatter alloc] init];
        [formatterTime setDateStyle:NSDateFormatterNoStyle];
        [formatterTime setTimeStyle:NSDateFormatterShortStyle];
    }
    
    // In case from name is not there bcz no  location is set, see if we got a value dumped in additional name column
    if (trip.fromLocation.name)
        leCell.fromLocnNameLbl.text   = trip.fromLocation.name;
    else
        leCell.fromLocnNameLbl.text = trip.from_locname;
    
    if (trip.toLocation.name)
        leCell.toLocnNameLbl.text = trip.toLocation.name;
    else
        leCell.toLocnNameLbl.text = trip.to_locname;
    
    
    
    NSString *tmpLeftAtStr = (trip.from_at == nil) ? APP_SPACED_HYPHEN : [formatterTime stringFromDate:trip.from_at];
    NSString *tmpReachedAtStr  = (trip.to_at  == nil) ? APP_SPACED_HYPHEN : [formatterTime stringFromDate:trip.to_at];
    
    leCell.leftAtTimeLbl.text = tmpLeftAtStr;
    leCell.reachedAtTimeLbl.text = tmpReachedAtStr;
    
    // Get a more readable time for showing up on UI
    leCell.timeDurationLbl.text = [AppUtilities getReadableTimeWithStartDate:trip.from_at WithEndDate:trip.to_at];
    
    NSString *fromLcnImgStr = trip.fromLocation.extra2_str == nil? trip.from_locn_img : trip.fromLocation.extra2_str;
    if (fromLcnImgStr == nil && [trip.finished intValue] == 1) //don't show default icon if trip is open
        fromLcnImgStr = DEFAULT_LOCN_IMGNAME;
    UIImage *fromLcnimg = [[AppSharedData getInstance] getLocationIconImageByName:fromLcnImgStr];
    [leCell.fromLocnImgView setImage:fromLcnimg];
    
    NSString *toLcnImgStr = trip.toLocation.extra2_str == nil? trip.to_locn_img : trip.toLocation.extra2_str;
    if (toLcnImgStr == nil && [trip.finished intValue] == 1) //don't show default icon if trip is open
        toLcnImgStr = DEFAULT_LOCN_IMGNAME; 
    UIImage *toLcnimg = [[AppSharedData getInstance] getLocationIconImageByName:toLcnImgStr];
    [leCell.toLocnImgView setImage:toLcnimg];
    
    
    
    // Added to enable receiving the editEntyBtnClick which handled later in this program
    leCell.delegate = self;
    leCell.trip = trip;
    if ( trip.from_at != nil && trip.to_at != nil) {
        leCell.editEntryBtn.hidden = YES; // Hide this button if both the dates are present
    } else {
        leCell.editEntryBtn.hidden = NO;
    }
    
    // iOS7 onwards the color was very odd so setting up manually
    leCell.selectedBackgroundView            = [AppUtilities getSelectedBackgroundView];
    leCell.textLabel.highlightedTextColor    = [AppUtilities getSelectedTextHighlightColor];
    
    /*
     UIButton* pencil = [UIButton buttonWithType:UIButtonTypeCustom];
     [pencil setImage:[UIImage imageNamed:@"Pencil.png"] forState:UIControlStateNormal];
     pencil.frame = CGRectMake(5, 5, 5, 5);
     pencil.userInteractionEnabled = YES;
     [pencil addTarget:self action:@selector(didTapEditButton:)      forControlEvents:UIControlEventTouchDown];
     //    [leCell.accessoryView convertRect:CGRectMake(0, 0, 10, 10) fromView:leCell.accessoryView];
     //    leCell.accessoryView.frame = CGRectMake(0, 0, 10, 10);
     leCell.accessoryView = pencil;
     */
}

#pragma mark - Section Header View Delegate
-(void) sectionHeaderViewFinish:(RouteSectionHeaderView *)sectionHeaderView Section:(NSInteger)section Operation:(int)sectionOperation {
    // NOT USED YET
}


#pragma mark - RouteViewControllerDelegate API

-(void) routeViewControllerDidFinish:(RouteViewController *)routeViewController FromLocation:(Location *)fromLocation ToLocation:(Location *)toLocation {
    NSLog(@"SAVE ROUTE");
}

#pragma mark - LocationSViewController Delegate APIs
-(void) locationSViewControllerDidCancel:(LocationSViewController *)controler {
    NSLog(@"LocationLogViewController: locationSViewController performed CANCEL");
}
-(void) locationSViewControllerDidFinish:(LocationSViewController *)controller OnLocation:(Location *)location LocationType:(int)type {
    NSLog(@"LocationLogViewController: locationSViewController performed FINISH");
}

#pragma mark - TravelogCellDelgate API
-(void) notifyTravelogCelBtnClick:(Trip *)trip {
    self.tmpLogVar = trip; // First keep a temporary variable to use before doing the segue in the segue handling.
    [self performSegueWithIdentifier:@"ShowManualEntryViewController" sender:self];
}

- (IBAction)entryBtnClicked:(id)sender {
    // Manually
    
}

@end

