//
//  LocationDetailViewController.h
//  Travello
//
//  Created by Balbir Shokeen on 2013/08/31.
//  Copyright (c) 2013 Balbir Shokeen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import <MapKit/MapKit.h>
#import "CoreDataOperations.h"
#import "AppUtilities.h"
#import "AppSharedData.h"
#import "LocationIconViewController.h"



@protocol LocationViewControllerDelegate;

// Help running this view for selection or editing and going further to detail ui
typedef enum { LocationEDIT=1, LocationADD=2, LocationREADONLY=3 } LocationOperationMode;


//Specify this app delegate complies with the location manager delegate
@interface LocationViewController : UITableViewController <UITextFieldDelegate, MKMapViewDelegate, LocationIconViewControllerDelegate>

// Set Below attributes when launching this UI via segue from another UI to specify who launched it (to send back the finish/cancel operations) and for what location is to be edited.
@property (weak, nonatomic) id <LocationViewControllerDelegate> delegate;
@property (strong, nonatomic) Location *selectedLocation;
@property (nonatomic) LocationOperationMode locOperationMode;

// Interface Elements
@property (weak, nonatomic) IBOutlet UITextField *uitfLocnName;
@property (weak, nonatomic) IBOutlet UITextField *uitfLocnAddress;
@property (weak, nonatomic) IBOutlet MKMapView *mkMapView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *loadAddressActivityView;
@property (weak, nonatomic) IBOutlet UIButton *locationIconBtn;


// TO hold core data entity name
@property (nonatomic, strong) NSString *thisInterfaceEntityName;

@property (nonatomic, strong) MKLocalSearch *localSearch;

- (IBAction)dropPinToCenter;
- (IBAction)saveBtnClick:(id)sender;

@end


@protocol LocationViewControllerDelegate <NSObject>

- (void) locationViewControllerDidCancel: (LocationViewController *) controler;
- (void) locationViewControllerDidFinish: (LocationViewController *) controller OnLocation:(Location *)location OperationType: (int) type;

@end

