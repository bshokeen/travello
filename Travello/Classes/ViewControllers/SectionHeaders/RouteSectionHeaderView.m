//
//  RouteSectionHeaderView.m
//  Travello
//
//  Created by Balbir Shokeen on 2013/12/04.
//  Copyright (c) 2013 Balbir Shokeen. All rights reserved.
//

#import "RouteSectionHeaderView.h"

@implementation RouteSectionHeaderView

@synthesize indexLbl = _indexLbl, nameLbl = _nameLbl, countLbl = _countLbl;
@synthesize sectionIdx = _sectionIdx, sectionItemCount = _sectionItemCount;


-(id) initWithFrame:(CGRect)frame title:(NSString *)title section:(NSInteger)sectionNumber sectionItemCount:(NSInteger)sectionItemCount HeaderIndex:(NSInteger)headerIndex TagNumber:(NSInteger)tagNumber delegate:(id<RouteSectionHeaderViewDelegate>)delegate {
    
    self = [super initWithFrame:frame];
    if (self) {
        
    }
    
    NSLog(@"SectionHeader: Index:%li Name: %@ Count:%i Frame x:%f y:%f W:%f H:%f", (long)headerIndex, title, (int)sectionItemCount,frame.origin.x, frame.origin.y, frame.size.width, frame.size.height);
    
    /** ATTRIBUTES initialized **/
    self.sectionItemCount = sectionItemCount;
    self.sectionIdx = sectionNumber;
    self.tag        = tagNumber;
    
    /** Common variables prepared **/
    UIColor *textColor =  [UIColor colorWithHue:0.548 saturation:1 brightness:0.79 alpha:1.0];
    UIColor *bgColor   = [UIColor colorWithHue:0.66 saturation:0.02 brightness:0.96 alpha:1.0];
    /*
     // [UIColor colorWithHue:0.565 saturation:0.10 brightness:1 alpha:1.0];  // Light Sky Blue
     // [UIColor colorWithHue:0.16 saturation:0.0 brightness:0.81 alpha:1.0]; // Light Grey Color
     // [UIColor colorWithHue:0.557 saturation:1.0 brightness:1 alpha:1.0];   // iGotTodo Background Cell Color
     // [UIColor colorWithHue:0.548 saturation:1 brightness:0.79 alpha:1.0];  // iGotTodo Font Color
     */
    
    /** CREATE THE SECTION INTERFACE **/
    UILabel *indexLabel = [[UILabel alloc] initWithFrame:CGRectMake(0,0,18,24)];//
    indexLabel.font = [UIFont systemFontOfSize:10.0];
    indexLabel.textColor = textColor;
    indexLabel.backgroundColor = [UIColor clearColor];
    indexLabel.textAlignment = NSTextAlignmentRight;
    //    indexLabel.layer.borderColor = [UIColor blueColor].CGColor;
    //    indexLabel.layer.borderWidth = 3.0;
    
    // Bug Fix: Changed the width from 235 to width - size of the count label. With Device orientation change alignment was not proper. For few it was in center and others starting from left as control was not autoresized by that time and center alignment was for less width control.
    UILabel *nameLabel = [[UILabel alloc] initWithFrame:CGRectMake(22,0, frame.size.width-55,24)];
    nameLabel.font = [UIFont boldSystemFontOfSize:14.0];
    nameLabel.textColor = textColor;
    nameLabel.backgroundColor = [UIColor clearColor];
    nameLabel.lineBreakMode = NSLineBreakByTruncatingTail;
    nameLabel.text = title;
    nameLabel.textAlignment = NSTextAlignmentCenter;
    nameLabel.autoresizesSubviews = YES;
    nameLabel.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    //    nameLabel.layer.borderColor = [UIColor purpleColor].CGColor;
    //    nameLabel.layer.borderWidth = 3.0;
    
    //BUG FIX: Keeping fixed coordinate made the count label appear in middle on different orientation. So tracking its x from with width     UILabel *cntLabel = [[UILabel alloc] initWithFrame:CGRectMake(270, 0, 40, 24)];
    UILabel *cntLabel = [[UILabel alloc] initWithFrame: CGRectMake(frame.size.width -50, 0,40,24)];
    cntLabel.font = [UIFont fontWithName:@"Helvetica" size:12.0];
    cntLabel.textColor = [UIColor lightGrayColor];
    cntLabel.layer.backgroundColor = [UIColor clearColor].CGColor;
    cntLabel.backgroundColor = [UIColor clearColor];
    cntLabel.textAlignment = NSTextAlignmentRight;
    cntLabel.text = [NSString stringWithFormat:@"%ld", (long)sectionItemCount];
    cntLabel.autoresizesSubviews = YES;
    cntLabel.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin;
    
    self.indexLbl = indexLabel;
    self.nameLbl  = nameLabel;
    self.countLbl = cntLabel;
    
    [self addSubview:self.indexLbl];
    [self addSubview:self.nameLbl];
    [self addSubview:self.countLbl];
    
    self.indexLbl.text = [NSString stringWithFormat:@"%li", (long)headerIndex ] ;
    self.nameLbl.text  = title;
    self.countLbl.text = [NSString stringWithFormat:@"%li", (long)sectionItemCount];
    
    self.viewForBaselineLayout.backgroundColor = bgColor; // Set the view background color.
    
    return self;
}

-(void) updateCount:(int)updateCount {
    self.sectionItemCount = self.sectionItemCount + updateCount ;
    self.countLbl.text = [NSString stringWithFormat:@"%ld", (long)self.sectionItemCount];
    NSLog(@"updateCountOnDelete: Section: %@ Idx:%i ItemCount:%i", self.countLbl.text, (int)self.sectionIdx , (int)self.sectionItemCount);
}

@end
