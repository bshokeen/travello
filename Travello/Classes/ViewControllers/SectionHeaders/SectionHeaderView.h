//
//  TravelogSectionHeaderView.h
//  Travello
//
//  Created by Balbir Shokeen on 2013/12/01.
//  Copyright (c) 2013 Dasheri. All rights reserved.
//

#import <UIKit/UIKit.h>


@protocol SectionHeaderViewDelegate;

@interface SectionHeaderView : UIView

@property (nonatomic, weak) UILabel *indexLbl;
@property (nonatomic, weak) UILabel *nameLbl;
@property (nonatomic, weak) UILabel *countLbl;

@property (nonatomic, assign) NSInteger sectionIdx;
@property (nonatomic, assign) NSInteger sectionItemCount;

@property (nonatomic, weak) id <SectionHeaderViewDelegate> delegate;


-(id)initWithFrame:(CGRect)frame title:(NSString*)title section:(long)sectionNumber sectionItemCount:(long) sectionItemCount HeaderIndex:(long)headerIndex TagNumber:(long)tagNumber delegate:(id <SectionHeaderViewDelegate>)delegate;


-(void) updateCount:(int)updateCount;

@end


@protocol SectionHeaderViewDelegate <NSObject>

//@optional
-(void)sectionHeaderViewFinish:(SectionHeaderView*)sectionHeaderView Section:(long) section Operation:(long) sectionOperation;

@end