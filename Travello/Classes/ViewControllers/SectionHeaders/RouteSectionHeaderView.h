//
//  RouteSectionHeaderView.h
//  Travello
//
//  Created by Balbir Shokeen on 2013/12/04.
//  Copyright (c) 2013 Balbir Shokeen. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol RouteSectionHeaderViewDelegate;

@interface RouteSectionHeaderView : UIView

@property (nonatomic, weak) UILabel *indexLbl;
@property (nonatomic, weak) UILabel *nameLbl;
@property (nonatomic, weak) UILabel *countLbl;

@property (nonatomic, assign) NSInteger sectionIdx;
@property (nonatomic, assign) NSInteger sectionItemCount;

@property (nonatomic, weak) id <RouteSectionHeaderViewDelegate> delegate;


-(id)initWithFrame:(CGRect)frame title:(NSString*)title section:(NSInteger)sectionNumber sectionItemCount:(NSInteger) sectionItemCount HeaderIndex:(NSInteger)headerIndex TagNumber:(NSInteger)tagNumber delegate:(id <RouteSectionHeaderViewDelegate>)delegate;


-(void) updateCount:(int)updateCount;

@end


@protocol RouteSectionHeaderViewDelegate <NSObject>

//@optional
-(void)sectionHeaderViewFinish:(RouteSectionHeaderView*)sectionHeaderView Section:(NSInteger) section Operation:(int) sectionOperation;

@end