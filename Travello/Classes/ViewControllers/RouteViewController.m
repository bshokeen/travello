//
//  RouteViewController.m
//  Travello
//
//  Created by Balbir Shokeen on 2013/12/04.
//  Copyright (c) 2013 Balbir Shokeen. All rights reserved.
//

#import "RouteViewController.h"

@interface RouteViewController ()

@property (nonatomic, strong) Location *fromLocation;
@property (nonatomic, strong) Location *toLocation;

@end

@implementation RouteViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    self.fromLocation = self.route.fromLocation;
    self.toLocation   = self.route.toLocation;
    
    self.fromLocationLbl.text = self.route.fromLocation.name;
    self.toLocationLbl.text = self.route.toLocation.name;
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    UITableViewCell * cell = [self.tableView  cellForRowAtIndexPath:indexPath];
    [cell setSelected:NO animated:YES];
    
    // Switch Cell Selection to take you to editing instead of Location
    [self performSegueWithIdentifier:@"ShowLocationSViewController_ManualEntry" sender:self];
    
}


#pragma mark - Navigation

// In a story board-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"ShowLocationSViewController_ManualEntry"]) {
        
        LocationSViewController *controller = (LocationSViewController *)[segue destinationViewController];
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        
        switch (indexPath.row) {
            case 0: {
                controller.selectedLocation = self.fromLocation;
                controller.locationType = FromLocation;
                break;
            }
            case 1:{
                controller.selectedLocation = self.toLocation;
                controller.locationType = ToLocation;
                break;
            }
            default:
                break;
        }
        
        // to get the events, had to set the delegate as self.
        controller.delegate = self;
        controller.runInMode = SingleSelection;
        
    }
}

#pragma mark - LocationSViewController Delegate APIs
-(void) locationSViewControllerDidCancel:(LocationSViewController *)controler {
    NSLog(@"LocationLogViewController: locationSViewController performed CANCEL");
}
-(void) locationSViewControllerDidFinish:(LocationSViewController *)controller OnLocation:(Location *)location LocationType:(int)type {
    switch (type) {
        case FromLocation: {
            self.fromLocation = location;
            self.fromLocationLbl.text = location.name;
            break;
        }
        case ToLocation: {
            self.toLocation = location;
            self.toLocationLbl.text = location.name;
        }
        default:
            break;
    }
    NSLog(@"LocationLogViewController: locationSViewController performed FINISH");
}

- (IBAction)saveBtnClick:(id)sender {
    
    [CoreDataOperations addRouteWithFromLocation: self.fromLocation WithToLocation:self.toLocation WithSortOrder:0 WithImagePath:Nil WithImageType:0];
    
    [self.navigationController popViewControllerAnimated:YES];
    
    [self.delegate routeViewControllerDidFinish:self FromLocation: self.fromLocation ToLocation:self.toLocation];
    
}
@end
