//
//  SettingsViewController.h
//  Travello
//
//  Created by Balbir Shokeen on 2013/09/11.
//  Copyright (c) 2013 Balbir Shokeen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>
#import <MessageUI/MFMailComposeViewController.h>
#import <StoreKit/StoreKit.h>


@interface SettingsViewController : UITableViewController <MFMailComposeViewControllerDelegate,SKStoreProductViewControllerDelegate>

@property (weak, nonatomic) IBOutlet UISwitch *showDidEnterExitNotificationsSwitch;
@property (weak, nonatomic) IBOutlet UISwitch *autoDeleteOldLogsSwitch;
@property (weak, nonatomic) IBOutlet UILabel *versionLbl;

- (IBAction)showDidEnterExitSwitch_ValueChanged:(id)sender;
- (IBAction)autoDeleteOldLogsSwitch_ValueChanged:(id)sender;


@end
