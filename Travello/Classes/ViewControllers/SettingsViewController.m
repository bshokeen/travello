//
//  SettingsViewController.m
//  Travello
//
//  Created by Balbir Shokeen on 2013/09/11.
//  Copyright (c) 2013 Balbir Shokeen. All rights reserved.
//

#import "SettingsViewController.h"
#import "AppSharedData.h"

@interface SettingsViewController ()

@end

@implementation SettingsViewController

@synthesize showDidEnterExitNotificationsSwitch = _showDidEnterExitNotificationsSwitch, autoDeleteOldLogsSwitch = _autoDeleteOldLogsSwitch, versionLbl = _versionLbl;



- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Inform Flurry
    [Flurry logEvent:@"Tab Access: Settings"];

}

- (void) viewWillAppear:(BOOL)animated {
     
    if ( [[AppSharedData getInstance].appSettings showDidEnterExitNotifications] ) {
        [self.showDidEnterExitNotificationsSwitch setOn:YES];
    }
    else {
        [self.showDidEnterExitNotificationsSwitch setOn:NO];
    }
    
    if ( [[AppSharedData getInstance].appSettings autoDeleteOldLogs] ) {
        [self.autoDeleteOldLogsSwitch setOn:YES];
    }
    else {
        [self.autoDeleteOldLogsSwitch setOn:NO];
    }
    
    // Update the version number
    NSDictionary *infoDictionary = [[NSBundle mainBundle] infoDictionary];
    NSString *majorVersionName = [infoDictionary objectForKey:@"CFBundleShortVersionString"];
    
    NSLog(@"Version Info: %@", majorVersionName );
    [self.versionLbl setText: [@"v" stringByAppendingString:majorVersionName]];

}

- (void) viewDidDisappear:(BOOL)animated {
    
   
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)showDidEnterExitSwitch_ValueChanged:(id)sender {
    if ([_showDidEnterExitNotificationsSwitch isOn]) {
        
        [[AppSharedData getInstance].appSettings saveSettingShowDidEnterExitNotifications: 1];
        // Inform Flurry
        [Flurry logEvent:@"Clicked: Notification ON"];
    }
    else {
        [[AppSharedData getInstance].appSettings saveSettingShowDidEnterExitNotifications: 0];
        // Inform Flurry
        [Flurry logEvent:@"Clicked: Notification OFF"];
    }
}

- (IBAction)autoDeleteOldLogsSwitch_ValueChanged:(id)sender {
    if ([_autoDeleteOldLogsSwitch isOn]) {
        
        [[AppSharedData getInstance].appSettings saveSettingForAutoDeleteOldLogs: 1];
    }
    else {
        [[AppSharedData getInstance].appSettings saveSettingForAutoDeleteOldLogs: 0];
    }
}

- (void) mailToSupport {
    
    // Inform Flurry
    [Flurry logEvent:@"Clicked: Support"];
    
    if([MFMailComposeViewController canSendMail]) {
        MFMailComposeViewController *mailCont = [[MFMailComposeViewController alloc] init];
        mailCont.mailComposeDelegate = self;
        
        [mailCont setSubject:[NSString stringWithFormat:@"%@: %@", APP_NAME, @"Support!"]];
        [mailCont setToRecipients:[NSArray arrayWithObject:@"support@dasherisoft.com"]];
        //        [mailCont setMessageBody:@"Don't ever want to give you up" isHTML:NO];
        [mailCont setMessageBody:@"" isHTML:NO];
        [self presentViewController:mailCont animated:YES completion:nil];
        
    }
}

// Then implement the delegate method
- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error {
    
    // Notifies users about errors associated with the interface
    switch (result)
    {
        case MFMailComposeResultCancelled:
            NSLog(@"Mail Sending - Canceled");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"Mail Sending - Saved");
            break;
        case MFMailComposeResultSent:
            NSLog(@"Mail Sending - Sent");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail Sending - Failed");
            break;
            
        default:
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Email" message:@"Sending Failed."
                                                           delegate:self cancelButtonTitle:NSLocalizedString(@"OK",@"OK") otherButtonTitles: nil];
            [alert show];
        }
            
            break;
    }

    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell * cell = [self.tableView  cellForRowAtIndexPath:indexPath];
    [cell setSelected:NO animated:YES];

    switch (indexPath.section) {
        case 0:
        {
            // Inform Flurry
            [Flurry logEvent:@"Clicked: RateApp"];
            
        SKStoreProductViewController *storeProductViewController = [[SKStoreProductViewController alloc] init];
            storeProductViewController.delegate=self;
            
        NSDictionary *productParameters = @{ SKStoreProductParameterITunesItemIdentifier : @"772046819" };
        [storeProductViewController loadProductWithParameters:productParameters completionBlock:^(BOOL result, NSError *error) {
                if (result) {
                    [self presentViewController:storeProductViewController animated:YES completion:nil];
                } else {
                    [[[UIAlertView alloc] initWithTitle:@"Uh oh!" message:@"There was a problem displaying the app" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil] show];
                }
            }];
            
            // Option to allow rating this app
//            NSString* url = [NSString stringWithFormat: @"itms-apps://ax.itunes.apple.com/WebObjects/MZStore.woa/wa/viewContentsUserReviews?type=Purple+Software&id=%@", @"697736419"];
//            [[UIApplication sharedApplication] openURL: [NSURL URLWithString: url]];
            break;
        }
        case 3:{
            [Flurry logEvent:@"Clicked: FAQ"];
            switch (indexPath.row) {
                case 0:
                    [self mailToSupport];
                    break;
                    
                default:
                    break;
            }
            break;

        }
    }
}

#pragma mark - SKStoreProductViewController Delegate Methods

-(void)productViewControllerDidFinish:(SKStoreProductViewController *)viewController
{
    [viewController dismissViewControllerAnimated:YES completion:^{
        
    }];
}

@end
