//
//  TravelogViewController.h
//  Travello
//
//  Created by Balbir Shokeen on 2013/09/06.
//  Copyright (c) 2013 Balbir Shokeen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CommonViewController.h"
#import "TravelogCell.h"
#import "LocationViewController.h"
#import "LocationSViewController.h"
#import "SectionHeaderView.h"
#import "ManualEntryViewController.h"

@interface TravelogViewController : CommonViewController  <LocationViewControllerDelegate, UISearchBarDelegate, SectionHeaderViewDelegate, LocationSViewControllerDelegate, TravelogCellDelegate>

@property (strong, nonatomic) IBOutlet UISearchBar *uiSearchBar;
- (IBAction)entryBtnClicked:(id)sender;

@end
