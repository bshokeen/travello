//
//  TravelogCell.m
//  Travello
//
//  Created by Balbir Shokeen on 2013/09/06.
//  Copyright (c) 2013 Balbir Shokeen. All rights reserved.
//

#import "TravelogCell.h"
#import "ManualEntryViewController.h"
@implementation TravelogCell

@synthesize locationLog = _locationLog;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void) resetInitialText {
    
    //    [[self uilToTime] setText:@"" ];
    
    self.locationName.text = APP_BLANK;
    self.didEnterTime.text = APP_BLANK;
    self.didExitTime.text = APP_BLANK;
    self.duration.text = APP_BLANK;
}

// Notify Tableview that editEntry button is clicked
- (IBAction)editEntry:(id)sender {
    if (self.locationLog != nil) // Safety check
        [self.delegate notifyTravelogCelBtnClick:self.locationLog];
 }

@end
