//
//  RouteLogCell.m
//  Travello
//
//  Created by Balbir Shokeen on 2013/12/04.
//  Copyright (c) 2013 Balbir Shokeen. All rights reserved.
//

#import "TripCell.h"

@implementation TripCell

@synthesize trip = _trip;

-(void) resetInitialText {
    
    //    [[self uilToTime] setText:@"" ];
//    self.startLbl.text = APP_BLANK;
//    self.aroundLbl.text = APP_BLANK;
    
    self.fromLocnNameLbl.text = APP_BLANK;
    self.toLocnNameLbl.text = APP_BLANK;
    self.leftAtTimeLbl.text = APP_BLANK;
    self.reachedAtTimeLbl.text = APP_BLANK;
    self.timeDurationLbl.text = APP_BLANK;
}

// Notify Tableview that editEntry button is clicked
- (IBAction)editEntry:(id)sender {
    if (self.trip != nil) // Safety check
        [self.delegate notifyTravelogCelBtnClick:self.trip];
}

@end
