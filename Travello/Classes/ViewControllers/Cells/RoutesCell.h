//
//  RouteCell.h
//  Travello
//
//  Created by Balbir Shokeen on 2013/09/01.
//  Copyright (c) 2013 Balbir Shokeen. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RoutesCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *startLbl;
@property (weak, nonatomic) IBOutlet UILabel *aroundLbl;
@property (weak, nonatomic) IBOutlet UILabel *uilFromLocation;
@property (weak, nonatomic) IBOutlet UILabel *uilToLocation;
@property (weak, nonatomic) IBOutlet UIImageView *fromLocnImgView;
@property (weak, nonatomic) IBOutlet UIImageView *toLocnImgView;
@property (weak, nonatomic) IBOutlet UILabel *countIdxLbl;
@property (weak, nonatomic) IBOutlet UILabel *routeTripsCountLbl;
@property (weak, nonatomic) IBOutlet UILabel *routeTripsAvgTimeLbl;

-(void) resetInitialText;

@end
