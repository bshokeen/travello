//
//  FaQViewController.m
//  Travello
//
//  Created by Vivek Sehrawat on 2/1/14.
//  Copyright (c) 2014 Balbir Shokeen. All rights reserved.
//

#import "FAQViewController.h"
#import "Reachability.h"

@interface FAQViewController ()

@end

@implementation FAQViewController
@synthesize webView,activityIndicator;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Added activity indicator on navigation bar
    UIBarButtonItem * barButton = [[UIBarButtonItem alloc] initWithCustomView:activityIndicator];
    [self navigationItem].rightBarButtonItem = barButton;

	// Do any additional setup after loading the view.
    
    if ([self connected]) {
        NSURL *websiteUrl = [NSURL URLWithString:@"http://dasherisoft.com/_custompages/apps/travello/mobiledata/FAQ.html"];
        NSURLRequest *urlRequest = [NSURLRequest requestWithURL:websiteUrl];
        webView.delegate=self;
        [webView loadRequest:urlRequest];

    }
    else{
        NSString *htmlFile = [[NSBundle mainBundle] pathForResource:@"FAQ" ofType:@"html"];
        NSString* htmlString = [NSString stringWithContentsOfFile:htmlFile encoding:NSUTF8StringEncoding error:nil];
        [webView loadHTMLString:htmlString baseURL:nil];
    }

}
- (BOOL)connected
{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    return networkStatus != NotReachable;
}
- (void)webViewDidStartLoad:(UIWebView *)webView{
    
    [activityIndicator startAnimating];

    
}//a web view starts loading
- (void)webViewDidFinishLoad:(UIWebView *)webView{
    
    [activityIndicator stopAnimating];

    
}//web view finishes loading
- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error{
    [activityIndicator stopAnimating];

    
}//web view failed to load



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
