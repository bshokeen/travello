//
//  TravelogCell.h
//  Travello
//
//  Created by Balbir Shokeen on 2013/09/06.
//  Copyright (c) 2013 Balbir Shokeen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LocationLog.h"

@protocol TravelogCellDelegate;


@interface TravelogCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *inLbl;
@property (weak, nonatomic) IBOutlet UILabel *outLbl;

@property (weak, nonatomic) IBOutlet UILabel *locationName;
@property (weak, nonatomic) IBOutlet UILabel *didEnterTime;
@property (weak, nonatomic) IBOutlet UILabel *didExitTime;
@property (weak, nonatomic) IBOutlet UILabel *duration;
@property (weak, nonatomic) IBOutlet UIImageView *locIconImageView;
@property (weak, nonatomic) IBOutlet UIButton *editEntryBtn;

// To store the location Log object related ot this cell. Added to notify parent the localog for which edit button is clicked
@property (strong, nonatomic) LocationLog *locationLog;

@property (weak, nonatomic) id <TravelogCellDelegate> delegate;

-(void) resetInitialText;

- (IBAction)editEntry:(id)sender;
@end

@protocol TravelogCellDelegate <NSObject>

-(void) notifyTravelogCelBtnClick: (LocationLog *) editLocationLog;

@end