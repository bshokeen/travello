//
//  LogEntryCell.m
//  Travello
//
//  Created by Balbir Shokeen on 2013/08/31.
//  Copyright (c) 2013 Balbir Shokeen. All rights reserved.
//

#import "LogEntryCell.h"
#import "AppUtilities.h"

@implementation LogEntryCell

@synthesize uilDate = _uilDate;
@synthesize uilFromLocation = _uilFromLocation;
@synthesize uilFromTime = _uilFromTime;
@synthesize uilToLocation = _uilToLocation;
@synthesize uilToTime = _uilToTime;
@synthesize uiimgDirection = _uiimgDirection;
@synthesize uibtnEdit = _uibtnEdit;
@synthesize uibtnDelete = _uibtnDelete;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void) resetInitialText {
    
    [[self uilDate] setText:@""];
    [[self uilFromLocation] setText:@""];
    [[self uilToLocation] setText:@"" ];
    [[self uilFromTime] setText:@"" ];
    [[self uilToTime] setText:@"" ];
  
    //            mvaCell.statusButton2.backgroundColor = [UIColor blueColor];
    [self.uibtnDelete setImage:[UIImage imageNamed:[AppUtilities getImageName:@"deleteLog"]] forState:UIControlStateNormal];
    self.uibtnDelete.contentMode = UIViewContentModeScaleToFill;
    [self.uibtnDelete sizeToFit];
}
@end
