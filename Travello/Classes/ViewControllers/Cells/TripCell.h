//
//  RouteLogCell.h
//  Travello
//
//  Created by Balbir Shokeen on 2013/12/04.
//  Copyright (c) 2013 Balbir Shokeen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Trip.h"

@protocol TripCellDelegate;

@interface TripCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *startLbl;
@property (weak, nonatomic) IBOutlet UILabel *aroundLbl;
@property (weak, nonatomic) IBOutlet UIImageView *fromLocnImgView;
@property (weak, nonatomic) IBOutlet UIImageView *toLocnImgView;
@property (weak, nonatomic) IBOutlet UILabel *fromLocnNameLbl;
@property (weak, nonatomic) IBOutlet UILabel *toLocnNameLbl;
@property (weak, nonatomic) IBOutlet UILabel *leftAtTimeLbl;
@property (weak, nonatomic) IBOutlet UILabel *reachedAtTimeLbl;
@property (weak, nonatomic) IBOutlet UILabel *timeDurationLbl;
@property (weak, nonatomic) IBOutlet UIButton *editEntryBtn;

// To store the location Log object related ot this cell. Added to notify parent the localog for which edit button is clicked
//@property (strong, nonatomic) RouteLog *locationLog;
@property (strong, nonatomic) Trip *trip;

@property (weak, nonatomic) id <TripCellDelegate> delegate;

-(void) resetInitialText;

- (IBAction)editEntry:(id)sender;
@end

@protocol TripCellDelegate <NSObject>

-(void) notifyTravelogCelBtnClick: (Trip *) trip;


@end
