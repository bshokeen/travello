//
//  LocationIconCell.h
//  Travello
//
//  Created by Balbir Shokeen on 2013/09/14.
//  Copyright (c) 2013 Dasheri. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LocationIconCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *locImageView;
@property (weak, nonatomic) IBOutlet UIImageView *locSelectedImageView;

@end
