//
//  FaQViewController.h
//  Travello
//
//  Created by Vivek Sehrawat on 2/1/14.
//  Copyright (c) 2014 Balbir Shokeen. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FAQViewController : UIViewController<UIWebViewDelegate>
@property (weak, nonatomic) IBOutlet UIWebView *webView;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;

@end
