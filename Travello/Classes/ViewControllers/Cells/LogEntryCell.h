//
//  LogEntryCell.h
//  Travello
//
//  Created by Balbir Shokeen on 2013/08/31.
//  Copyright (c) 2013 Balbir Shokeen. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LogEntryCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *uilDate;
@property (weak, nonatomic) IBOutlet UILabel *uilFromLocation;
@property (weak, nonatomic) IBOutlet UILabel *uilFromTime;
@property (weak, nonatomic) IBOutlet UILabel *uilToLocation;
@property (weak, nonatomic) IBOutlet UILabel *uilToTime;
@property (weak, nonatomic) IBOutlet UIImageView *uiimgDirection;
@property (weak, nonatomic) IBOutlet UIButton *uibtnEdit;
@property (weak, nonatomic) IBOutlet UIButton *uibtnDelete;


-(void) resetInitialText;
@end
