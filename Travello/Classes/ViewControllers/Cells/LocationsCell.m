//
//  LocationsCell.m
//  Travello
//
//  Created by Balbir Shokeen on 2013/09/01.
//  Copyright (c) 2013 Balbir Shokeen. All rights reserved.
//

#import "LocationsCell.h"

@implementation LocationsCell

@synthesize uilLocationName = _uilLocationName;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void) resetInitialText {
    
//    [[self uilToTime] setText:@"" ];
    self.uilLocationName.text = APP_BLANK;
    self.uilLocationSubtitle.text = APP_BLANK;
    [self.imgVStatus setImage: [UIImage imageNamed:@"BT_Red"]];
    

    
}
@end
