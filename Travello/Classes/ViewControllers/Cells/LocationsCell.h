//
//  LocationsCell.h
//  Travello
//
//  Created by Balbir Shokeen on 2013/09/01.
//  Copyright (c) 2013 Balbir Shokeen. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LocationsCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *uilLocationName;
@property (weak, nonatomic) IBOutlet UILabel *uilLocationSubtitle;
@property (weak, nonatomic) IBOutlet UIImageView *imgVStatus;
@property (weak, nonatomic) IBOutlet UIImageView *locIconImageView;

-(void) resetInitialText;
@end
