//
//  HelpCell.m
//  Travello
//
//  Created by Balbir Shokeen on 2013/12/09.
//  Copyright (c) 2013 Balbir Shokeen. All rights reserved.
//

#import "HelpCell.h"

@implementation HelpCell

@synthesize uiWebView = _uiWebView;

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        // Initialization code
        
        /*
        NSURL *url = [[NSURL alloc] initWithString:@"http://dasherisoft.com"];
        NSURLRequest *request = [[NSURLRequest alloc] initWithURL:url];
        
        self.uiWebView.scalesPageToFit = YES;
        [self.uiWebView loadRequest:request];
         
         */
        
        NSString *htmlFile = [[NSBundle mainBundle] pathForResource:@"Help_LocationS" ofType:@"html" inDirectory:nil];
        NSString* htmlString = [NSString stringWithContentsOfFile:htmlFile encoding:NSUTF8StringEncoding error:nil];
        
        //Append javascript
        NSString *script = @"<script>alert(\"This is an alert!!\");</script>";
        htmlString = [htmlString stringByAppendingString:script];
        
        [self.uiWebView loadHTMLString:htmlString baseURL:nil];
    }
    return self;
}

-(void) resetInitialText {
    
    //    [[self uilToTime] setText:@"" ];
       
    
}




- (BOOL)webView:(UIWebView*)webView shouldStartLoadWithRequest:(NSURLRequest*)request navigationType:(UIWebViewNavigationType)navigationType {
    NSURL *URL = [request URL];
    if ([[URL scheme] isEqualToString:@"callmycode"]) {
        NSString *urlString = [[request URL] absoluteString];
        NSArray *urlParts = [urlString componentsSeparatedByString:@":"];
        //check to see if we just got the scheme
        if ([urlParts count] > 1) {
            NSArray *parameters = [[urlParts objectAtIndex:1] componentsSeparatedByString:@"&"];
            NSString *methodName = [parameters objectAtIndex:0];
            NSString *variableName = [parameters objectAtIndex:1];
            NSString *message = [NSString stringWithFormat:@"Obj-c from js with methodname=%@ and variablename=%@", methodName, variableName];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Great" message:message delegate: self cancelButtonTitle: nil otherButtonTitles: @"OK",nil, nil];
            [alert show];
            
            [self.uiWebView stringByEvaluatingJavaScriptFromString:@"alert('Trigger the JS!');"];
        }
    }
    return YES;
}

@end
