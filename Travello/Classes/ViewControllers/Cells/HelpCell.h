//
//  HelpCell.h
//  Travello
//
//  Created by Balbir Shokeen on 2013/12/09.
//  Copyright (c) 2013 Balbir Shokeen. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HelpCell : UIView <UIWebViewDelegate>

@property (weak, nonatomic) IBOutlet UIWebView *uiWebView ;

-(void) resetInitialText;

@end
