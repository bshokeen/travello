//
//  AllRegionsViewController.m
//  Travello
//
//  Created by Balbir Shokeen on 2013/09/06.
//  Copyright (c) 2013 Balbir Shokeen. All rights reserved.
//

#import "RegionSViewController.h"

@interface RegionSViewController ()
// DidUpdateLocation keep updating users current location and we want to get it once time only when the UI Starts
@property (nonatomic) bool hasRecievedUserCurrentLocationOnce;
@property (nonatomic) CLLocationCoordinate2D lastUpdatedUserLoc;

@end

@implementation RegionSViewController

@synthesize hasRecievedUserCurrentLocationOnce = _hasRecievedUserCurrentLocationOnce, lastUpdatedUserLoc = _lastUpdatedUserLoc;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Inform Flurry
    [Flurry logEvent:@"Tab Access: RegionS"];
    
    [self.navigationItem setRightBarButtonItem:nil animated:NO]; // Hide the right bar button item for now.
    
    self.hasRecievedUserCurrentLocationOnce = NO; //move ffrom appear to load as it was annoying to get it updated each time as user moved
    
    // Added for debugging, need to add more attributes to log for testing.
    [[AppSharedData getInstance].appLocationMgr logLocationMgrAttributes];
    
    // Associate Double Tap Gesture Handling
    [self registerTapGestureOnTitle]; // NOT WORKING
}

-(void) registerTapGestureOnTitle {
    // Add the ability to touble tap and reset the filter
    UITapGestureRecognizer* tapGest = [[UITapGestureRecognizer alloc]
                                                      initWithTarget:self action:@selector(navigationBarTitleDoubleTap:)];
    
    tapGest.numberOfTapsRequired = 2;
    [[self.navigationController.navigationBar.subviews objectAtIndex:1] setUserInteractionEnabled:YES];
    [[self.navigationController.navigationBar.subviews objectAtIndex:1] addGestureRecognizer:tapGest];

}

// Double Tap to Reset the Region view and go back to the default viewing level
-(void) navigationBarTitleDoubleTap:(UIGestureRecognizer*)recognizer {
    
    // Inform Flurry
    [Flurry logEvent:@"Clicked: RegionS DoubleTap"];
    
    [self moveMapFocusToCoordinate:self.lastUpdatedUserLoc];
    
}

- (void) viewDidAppear:(BOOL)animated {

    [self.mkMapView removeAnnotations:self.mkMapView.annotations];
    [self.mkMapView removeOverlays:self.mkMapView.overlays];
    
    [self plotAnnotationsOnMap];

    // Get the title of tab bar and add number of regions monitored for display only on the navigation bar. self.setTitle does it for tabBar also
    [self refreshTitle];
}

-(void) refreshTitle {
    long count = [[AppSharedData getInstance].appLocationMgr getAllRegionsCount];
    NSString *title = [@"Regions" stringByAppendingFormat:@" (%li)", count];
    [self.navigationItem setTitle: title];
}

-(NSInteger) plotAnnotationsOnMap {
    // Initialize the Map Location - Get all regions monitored, find the one related to this location, add annotation and move focus to that region.
    NSArray *regions = [[AppSharedData getInstance].appLocationMgr getAllRegions];
    
    for (int i = 0; i < [regions count]; i++) {
        CLRegion *region = [regions objectAtIndex:i];
        
        Location *loc = [CoreDataOperations getLocationByIdentifier:region.identifier];
        NSString *tmpName = nil;
        if (loc) {
            tmpName = loc.name;
        } else {
            // Create a dummy to help identify on the map
            tmpName = APP_EMPTY;
        }
        
        RegionAnnotation *regionAnnotation = [[RegionAnnotation alloc] initWithCLRegion:region Name:tmpName];;
        [self.mkMapView  addAnnotation:regionAnnotation];
        
        NSLog(@"AllRegions: plotAnnotations: LOADED - RegionID: %@ ", region.identifier);
    }
    return [regions count];
}

/* Shift Focus of the map to the specified coordinate */
-(void) moveMapFocusToCoordinate: (CLLocationCoordinate2D) coordinate {
    MKCoordinateSpan span; span.latitudeDelta = 1; span.longitudeDelta = 1; // 100 was too much
    MKCoordinateRegion mkcregion = {coordinate, span};
    
    [self.mkMapView setCenterCoordinate:coordinate];
    [self.mkMapView setRegion:mkcregion animated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) mapView:(MKMapView *)mapView didUpdateUserLocation:(MKUserLocation *)userLocation {
    // If it's a relatively recent event, turn off updates to save power
    NSDate* eventDate = userLocation.location.timestamp;
    NSTimeInterval howRecent = [eventDate timeIntervalSinceNow];
    if (abs(howRecent) < 15.0 && !self.hasRecievedUserCurrentLocationOnce) {
        // If the event is recent, do something with it.
        NSLog(@"Location: didUpdateUserLocation: Recent Entry - latitude %+.6f, longitude %+.6f\n",
              userLocation.location.coordinate.latitude,
              userLocation.location.coordinate.longitude);
        
        // Focus map to the latest user location coordinate
        CLLocationCoordinate2D latestCoordinate = userLocation.location.coordinate; // Get the latest coordinate
        [self moveMapFocusToCoordinate:latestCoordinate];
        
        self.lastUpdatedUserLoc = latestCoordinate; // Set it for reference later to let user double tap and reset focus
        
        // [self dropPinToCenter]; It is confusing to user, rather let him drop the pin when required
        self.hasRecievedUserCurrentLocationOnce = YES;
    }
    


}

- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id <MKAnnotation>)annotation {
    NSLog(@"Location: viewForAnnotation: Kind:%@ - Start", NSStringFromClass([annotation class]));
    
	if([annotation isKindOfClass:[RegionAnnotation class]]) {
		RegionAnnotation *currentAnnotation = (RegionAnnotation *)annotation;
		NSString *annotationIdentifier = currentAnnotation.title;
		RegionAnnotationView *regionView = (RegionAnnotationView *)[self.mkMapView dequeueReusableAnnotationViewWithIdentifier:annotationIdentifier];
		
		if (!regionView) {
			regionView = [[RegionAnnotationView alloc] initWithAnnotation:annotation];
			regionView.map = self.mkMapView;
          	
            // Create a button for the left callout accessory view of each annotation to remove the annotation and region being monitored.
            UIButton *removeRegionButton = [UIButton buttonWithType:UIButtonTypeCustom];
            [removeRegionButton setFrame:CGRectMake(0., 0., 25., 25.)];
            [removeRegionButton setImage:[UIImage imageNamed:@"RemoveRegion"] forState:UIControlStateNormal];
            
            regionView.leftCalloutAccessoryView = removeRegionButton;

		} else {
			regionView.annotation = annotation;
			regionView.theAnnotation = annotation;
		}
		
		// Update or add the overlay displaying the radius of the region around the annotation.
		[regionView updateRadiusOverlay];
		
		return regionView;
	}
	
	return nil;
}

- (MKOverlayView *)mapView:(MKMapView *)mapView viewForOverlay:(id <MKOverlay>)overlay {
    NSLog(@"Location: viewForOverlay: Kind:%@ - Start", NSStringFromClass([overlay class]));
    
	if([overlay isKindOfClass:[MKCircle class]]) {
        
		// Create the view for the radius overlay.
		MKCircleView *circleView = [[MKCircleView alloc] initWithOverlay:overlay];
		circleView.strokeColor = [UIColor purpleColor];
        circleView.lineWidth = 1;
		circleView.fillColor = [[UIColor purpleColor] colorWithAlphaComponent:0.4];
		
		return circleView;
	}
	
	return nil;
}

- (void)mapView:(MKMapView *)mapView didSelectAnnotationView:(MKAnnotationView *)view {
    NSLog(@"AllRegions: didSelect");
    // Disable Dragging of annotation on this view.
    view.draggable = NO;
}
- (void)mapView:(MKMapView *)mapView annotationView:(MKAnnotationView *)view calloutAccessoryControlTapped:(UIControl *)control {
    NSLog(@"AllRegions: calloutAccessoryControlTapped - START");
    
    RegionAnnotationView *regionView = (RegionAnnotationView *)view;
	RegionAnnotation *regionAnnotation = (RegionAnnotation *)regionView.annotation;
	
    /*// Holding Location Delete to Users Discretion & logs as well
    // Find the location associated with this region
    Location *location = [CoreDataOperations getLocationByIdentifier:regionAnnotation.region.identifier];
    if (location) {
        NSLog(@"locat: %i", [location.locationLogS count]);
        
        NSArray *locationLogs = [location.locationLogS allObjects];
        for (LocationLog *locLog in locationLogs) {
            [CoreDataOperations deleteItemByEntityName:@"LocationLog" Item:locLog];
        }
        
        [CoreDataOperations deleteItemByEntityName:@"Location" Item:location];

    }        */
    [self removeRegionMonitoring:regionAnnotation];
    [self refreshTitle];
}

-(void) removeOverlay: (CLLocationCoordinate2D) regionCoordinate {
    for (id overlay in [self.mkMapView overlays]) {
		if ([overlay isKindOfClass:[MKCircle class]]) {
			MKCircle *circleOverlay = (MKCircle *)overlay;
			CLLocationCoordinate2D coord = circleOverlay.coordinate;
			
			if (coord.latitude == regionCoordinate.latitude && coord.longitude == regionCoordinate.longitude) {
				[self.mkMapView removeOverlay:overlay];
			}
		}
	}
}

- (void) removeRegionMonitoring: (RegionAnnotation *) regionAnnotation {
    // Stop monitoring the region, remove the radius overlay, and finally remove the annotation from the map.
	[[AppSharedData getInstance].appLocationMgr stopMonitoringRegion:regionAnnotation.region];
    
    // Remove it's overlay
    [self removeOverlay:regionAnnotation.coordinate];
    
    // Remove Annotation
	[self.mkMapView removeAnnotation:regionAnnotation];
}


- (IBAction)deleteAllBtnClick:(id)sender {
    NSArray *regions = [[AppSharedData getInstance].appLocationMgr getAllRegions];
    for (int i = 0; i < [regions count]; i++) {
        CLRegion *region = [regions objectAtIndex:i];
        
        Location *loc = [CoreDataOperations getLocationByIdentifier:region.identifier];
        RegionAnnotation *regionAnnotation = [[RegionAnnotation alloc] initWithCLRegion:region Name:loc.name];

        [self removeRegionMonitoring: regionAnnotation];
        
        NSLog(@"AllRegions: deleteAll: DELTED - RegionID: %@ ", region.identifier);
        
        
//        [self.view setNeedsDisplay]
    }
}
@end
