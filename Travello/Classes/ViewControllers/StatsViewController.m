//
//  StatsViewController.m
//  Travello
//
//  Created by Balbir Shokeen on 2013/11/26.
//  Copyright (c) 2013 Dasheri. All rights reserved.
//

#import "StatsViewController.h"
#import "PNChart.h"
#import "CoreDataOperations.h"
#import "Toast+UIView.h"

@interface StatsViewController ()
//    @property (nonatomic, strong) NSMutableDictionary *graphData;
@property (nonatomic) int topX;
@end

@implementation StatsViewController

//@synthesize graphData = _graphData;
@synthesize  topX = _topX; // Based on orientation decide how many items to fetch for display on graph

- (void)viewDidLoad
{
    [super viewDidLoad];

    // Inform Flurry
    [Flurry logEvent:@"Tab Access: Summary/Graph"];
    
    [self.switchGraphSegment addTarget:self
                                  action:@selector(didChangeGroupSegmentControl:)
                        forControlEvents:UIControlEventValueChanged];

}

-(void) viewDidAppear:(BOOL)animated { // After view has appeared
    
    // By default switch to overall graph data and load data for it
    [self.switchGraphSegment setSelectedSegmentIndex:3];
    [self reLoadGraph:self.switchGraphSegment.selectedSegmentIndex]; //BugFix: Moved graphLoad to viewdidappear, to have the remaining components well loaded and PNGraph get dimentions based on orientation to load the graph properly.
    
    //    [self loadCorePlotGraph:0];

}
- (void)didChangeGroupSegmentControl:(UISegmentedControl *)control {
    
//    [self removeOldGraphs];
    
    [self reLoadGraph:control.selectedSegmentIndex ];
//    [self loadCorePlotGraph:control.selectedSegmentIndex];
}

-(void) removeOldGraphs{
    NSArray* subviews = [self.chartScrollView.subviews copy];
    for(UIView* v in subviews)
    {
        if ( [v isKindOfClass: [PNChart class]] || [v isKindOfClass:[UILabel class]] )
            [v removeFromSuperview];
    }
}


-(void) reLoadGraph: (NSInteger) selSegIdx {
    
    [self removeOldGraphs];
    
    NSMutableDictionary *locnSummaryData =  [CoreDataOperations getGraphSummary: (int) selSegIdx];

    NSMutableDictionary *tripSummaryData = [CoreDataOperations getTrip_GraphSummary: (int)selSegIdx];
    
    self.topX = -1;
    UIInterfaceOrientation interfaceOrientation = [[UIApplication sharedApplication] statusBarOrientation];
    if ( UIInterfaceOrientationIsLandscape(interfaceOrientation)) {
        self.topX = 8;
    }
    else if ( UIInterfaceOrientationIsPortrait(interfaceOrientation)) {
        self.topX = 5;
    }
    locnSummaryData = [AppUtilities getTopDescendingItemCount:self.topX From:locnSummaryData];
    tripSummaryData = [AppUtilities getTopDescendingItemCount:self.topX From:tripSummaryData];

    int posY=0; // Variable to help track Y axis for adding next item
    
    posY = [self drawTripSummaryGraphs:posY locnSummaryData:tripSummaryData];
    posY = [self drawLocnSummaryGraphs:posY locnSummaryData:locnSummaryData];

    if (posY == 0) {
        // Make toast with an image
        [self.view makeToast:@"Not enough data to display"
                    duration:2.0
                    position:@"center"
                       image:nil];
    }
}

- (int)drawTripSummaryGraphs:(int)posY locnSummaryData:(NSMutableDictionary *)tripSummaryData {

    if (tripSummaryData && [tripSummaryData count] > 0) {
        
        NSArray *tripSum_XLabels = tripSummaryData.allKeys;
        NSArray *tripSum_YValues = tripSummaryData.allValues;
        /*
        NSArray *tripSum_XLabels = [tripSummaryData objectForKey:@"orderedkeys"];
        NSArray *tripSum_YValues = [tripSummaryData objectForKey:@"orderedValues"];
        */
        UILabel * lineChartTitle = [[UILabel alloc] initWithFrame:CGRectMake(0, posY=posY+0, self.tableView.bounds.size.width, 30)];
        //        lineChartTitle.text = @"No. of hours per location";
        lineChartTitle.text = @"Total Hours Spent Per Trip";
        lineChartTitle.textColor = PNFreshGreen;
        lineChartTitle.font = [UIFont fontWithName:@"Avenir-Medium" size:14.0];
        lineChartTitle.textAlignment = NSTextAlignmentCenter;
        
        // Add BarChart
        PNChart * pnBarChart = [[PNChart alloc] initWithFrame:CGRectMake(0, posY=posY+0, self.tableView.bounds.size.width, 160.0)];
        pnBarChart.backgroundColor = [UIColor clearColor];
        pnBarChart.type = PNBarType;
        
        [pnBarChart setXLabels:tripSum_XLabels];
        [pnBarChart setYValues:tripSum_YValues];
        [pnBarChart strokeChart];
        
        //Add Graph Separator Label
        UILabel * lineChartLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, posY=posY+140, self.tableView.bounds.size.width, 30)];
        lineChartLabel.text = @"____________________________";
        lineChartLabel.textColor = PNFreshGreen;
        lineChartLabel.font = [UIFont fontWithName:@"Avenir-Medium" size:23.0];
        lineChartLabel.textAlignment = NSTextAlignmentCenter;
        
        //Add Line Chart
        PNChart * lineChart = [[PNChart alloc] initWithFrame:CGRectMake(5, posY=posY+10, self.tableView.bounds.size.width, 200.0)];
        lineChart.backgroundColor = [UIColor clearColor];
        [lineChart setXLabels:tripSum_XLabels];
        [lineChart setYValues:tripSum_YValues];
        [lineChart strokeChart];
        
        //Add Graph Section Separator Label
        UILabel *sectionSeparatorLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, posY=posY+200, self.tableView.bounds.size.width, 30)];
        sectionSeparatorLabel.text = @"____________________________";
        sectionSeparatorLabel.textColor = PNBlue;
        sectionSeparatorLabel.font = [UIFont fontWithName:@"Avenir-Medium" size:23.0];
        sectionSeparatorLabel.textAlignment = NSTextAlignmentCenter;

        
        [self.chartScrollView addSubview:pnBarChart];
        [self.chartScrollView addSubview:lineChartLabel];
        [self.chartScrollView addSubview:lineChartTitle];
        [self.chartScrollView addSubview:lineChart];
        [self.chartScrollView addSubview:sectionSeparatorLabel];
        
    }
    return posY;
}

- (int)drawLocnSummaryGraphs:(int)posY locnSummaryData:(NSMutableDictionary *)locnSummaryData {
    
    if ( locnSummaryData && [locnSummaryData count] >0) {
        
        NSArray *locnSum_XLabels = locnSummaryData.allKeys;
        NSArray *locnSum_YValues = locnSummaryData.allValues;
        /*
        NSArray *locnSum_XLabels = [locnSummaryData objectForKey:@"orderedkeys"];
        NSArray *locnSum_YValues = [locnSummaryData objectForKey:@"orderedvalues"];
        */
        UILabel * lineChartTitle = [[UILabel alloc] initWithFrame:CGRectMake(0, posY=posY+25, self.tableView.bounds.size.width, 30)];
        //        lineChartTitle.text = @"No. of hours per location";
        lineChartTitle.text = @"Total Hours Spent At A Location";
        lineChartTitle.textColor = PNFreshGreen;
        lineChartTitle.font = [UIFont fontWithName:@"Avenir-Medium" size:14.0];
        lineChartTitle.textAlignment = NSTextAlignmentCenter;
        
        // Add BarChart
        PNChart * pnBarChart = [[PNChart alloc] initWithFrame:CGRectMake(0, posY=posY+10, self.tableView.bounds.size.width, 160.0)];
        pnBarChart.backgroundColor = [UIColor clearColor];
        pnBarChart.type = PNBarType;
        
        [pnBarChart setXLabels:locnSum_XLabels];
        [pnBarChart setYValues:locnSum_YValues];
        [pnBarChart strokeChart];
        
        //Add Graph Separator Label
        UILabel * lineChartLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, posY=posY+140, self.tableView.bounds.size.width, 30)];
        lineChartLabel.text = @"____________________________";
        lineChartLabel.textColor = PNFreshGreen;
        lineChartLabel.font = [UIFont fontWithName:@"Avenir-Medium" size:23.0];
        lineChartLabel.textAlignment = NSTextAlignmentCenter;
        
        //Add Line Chart
        PNChart * lineChart = [[PNChart alloc] initWithFrame:CGRectMake(5, posY=posY+20, self.tableView.bounds.size.width, 200.0)];
        lineChart.backgroundColor = [UIColor clearColor];
        [lineChart setXLabels:locnSum_XLabels];
        [lineChart setYValues:locnSum_YValues];
        [lineChart strokeChart];
        
        //Add Graphs Section Separator Label
        UILabel *sectionSeparatorLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, posY=posY+180, self.tableView.bounds.size.width, 30)];
        sectionSeparatorLabel.text = @"____________________________";
        sectionSeparatorLabel.textColor = PNBlue;
        sectionSeparatorLabel.font = [UIFont fontWithName:@"Avenir-Medium" size:23.0];
        sectionSeparatorLabel.textAlignment = NSTextAlignmentCenter;
        
        [self.chartScrollView addSubview:pnBarChart];
        [self.chartScrollView addSubview:lineChartLabel];
        [self.chartScrollView addSubview:lineChartTitle];
        [self.chartScrollView addSubview:lineChart];
        [self.chartScrollView addSubview:sectionSeparatorLabel];
        
    }
    
    return posY;
}

-(void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning]; // Releases the view if it doesn't have a superview
    // Release anything that's not essential, such as cached data
}


#pragma mark - Table view data source




/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a story board-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}

 */

-(BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
{
    return YES;
}

-(void) didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation {
    // Bug Fix: to get proper graph on realignment
    [self reLoadGraph:self.switchGraphSegment.selectedSegmentIndex];
    //    [self loadCorePlotGraph:0];
}

@end
