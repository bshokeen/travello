//
//  TripSViewController.h
//  Travello
//
//  Created by Balbir Shokeen on 2014/01/27.
//  Copyright (c) 2014 Balbir Shokeen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CommonViewController.h"
#import "RouteViewController.h"
#import "RouteLog.h"
#import "Route.h"
#import "LocationSViewController.h"
#import "ManualEntryViewController.h"
#import "RouteSectionHeaderView.h"
#import "TripCell.h"

@interface TripSViewController : CommonViewController <RouteViewControllerDelegate, UISearchBarDelegate,  RouteSectionHeaderViewDelegate, LocationSViewControllerDelegate, TripCellDelegate>

@property (weak, nonatomic) IBOutlet UISearchBar *uiSearchBar;
- (IBAction)entryBtnClicked:(id)sender;
@end