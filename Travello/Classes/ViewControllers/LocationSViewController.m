//
//  LocationSViewController.m
//  Travello
//
//  Created by Balbir Shokeen on 2013/08/31.
//  Copyright (c) 2013 Balbir Shokeen. All rights reserved.
//

#import "LocationSViewController.h"
#import "AppConstants.h"
#import "Toast+UIView.h"
#import "ManualEntryViewController.h"

@interface LocationSViewController ()
// location which is now selected by user.
@property (nonatomic, strong) Location *currentLocationSelected;
@property (nonatomic, strong) NSIndexPath *lastSelLocnIdxPath;

@property (nonatomic, strong) NSArray *products;
@property (nonatomic, strong) NSNumberFormatter * priceFormatter;
@property bool loadInProgressHoldOperations;

@property bool helpAlertAppeardOnce;

@end

@implementation LocationSViewController

@synthesize delegate = _delegate, selectedLocation=_selectedLocation, runInMode = _runInMode;
@synthesize currentLocationSelected = _locationSelected, lastSelLocnIdxPath= _lastSelLocnIdxPath;
@synthesize locationType=_locationType;
@synthesize products = _products, priceFormatter = _priceFormatter, loadInProgressHoldOperations = _loadInProgressHoldOperations;
@synthesize helpAlertAppeardOnce = _helpAlertAppeardOnce;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

// When launching from tab this API is called so setting the default runMode here
-(id) initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        // Enable editing on click of left navbaritem by setting it as edit button
        self.runInMode = EditSingleSelection; // Multiple selection is not allowed as of now.
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    // Inform Flurry
    [Flurry logEvent:@"Tab Access: LocationS"];
    
    // Initialize its entity name to support common operations in its parent class
    self.thisInterfaceEntityName = @"Location";

    if (self.runInMode == ManualEntrySingleSelection) {
        // Avoid showing the add button else it is confusing what to do.
        self.navigationItem.rightBarButtonItem = nil;
        self.navigationItem.leftBarButtonItem = nil; // Hide the regions button as well
    }
//    self.navigationItem.leftBarButtonItem = self.editButtonItem; // Now we have dynamic ordering so disabled edit button
    
    self.currentLocationSelected = self.selectedLocation;

    if (INAPP_ENABLED) {
    // Adding ability to load the do in-app purchase and hence refresh the status
    self.refreshControl = [[UIRefreshControl alloc] init];
    [self.refreshControl addTarget:self action:@selector(reload) forControlEvents:UIControlEventValueChanged];
//    [self reload];
    
    _priceFormatter = [[NSNumberFormatter alloc] init];
    [_priceFormatter setFormatterBehavior:NSNumberFormatterBehavior10_4];
    [_priceFormatter setNumberStyle:NSNumberFormatterCurrencyStyle];
    }
 }

-(void) reload { // Added in case user performs a pull down, clear it up after some time
    [self.refreshControl endRefreshing];
}
- (void)leftSwipeHandle
{
    ULog(@"Left Swipe");
}

-(void) viewWillAppear:(BOOL)animated {
    // Show the bottom bar when this appears.
//    self.hidesBottomBarWhenPushed = NO;
    
    [self hideHelpBubble];
    [self showHelpBubble];
    
}

- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {
    
    
    CGFloat constrainHeight = MAXFLOAT;
    CGFloat constrainWidth  = 250.0;
    CGSize constrainSize = CGSizeMake(constrainWidth, constrainHeight);
    CGSize labelSize     = [helpString sizeWithFont:[UIFont systemFontOfSize:18.0f]
                            constrainedToSize:constrainSize
                                lineBreakMode:NSLineBreakByCharWrapping];
    CGFloat Fheight=MAX(70.0, labelSize.height);
    CGFloat Fwidth=labelSize.width;

    if (toInterfaceOrientation == UIInterfaceOrientationLandscapeLeft || toInterfaceOrientation == UIInterfaceOrientationLandscapeRight) {
        helpView.frame=CGRectMake(self.view.bounds.size.width-Fwidth, 0,Fwidth,Fheight+20);

    } else {
        helpView.frame=CGRectMake(self.view.bounds.size.width-Fwidth, 0,Fwidth,Fheight+20);
    }
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = nil;
    
    NSInteger count = [[[self fetchedResultsController] fetchedObjects] count];
    
    if ( count == 0 ) {
        // Show the HelpCell
        static NSString *cellIdentifier = @"HelpCell";
        
        cell= [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        
        // call to update the cell details
        [self configureHelpCell:cell atIndexPath:indexPath];

    }
    else {
        // Show the required cell
        static NSString *cellIdentifier = @"LocationsCell";
        
        cell= [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        
        // call to update the cell details
        [self configureCell:cell atIndexPath:indexPath];

    }
    
    return cell;
}


#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    Location *location = (Location *) [[self fetchedResultsController] objectAtIndexPath:indexPath];
    
    if (self.runInMode == SingleSelection ) {
        UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
        [cell setSelected:NO animated:YES];
        
        // Reset Previous Selected Index if any
        if (self.lastSelLocnIdxPath && ![self.lastSelLocnIdxPath isEqual:indexPath]) {
            UITableViewCell * lastCell = [self.tableView  cellForRowAtIndexPath:self.lastSelLocnIdxPath];
            [lastCell setSelected:NO animated:YES];
            lastCell.accessoryType = UITableViewCellAccessoryNone;
        }
        
        if (cell.accessoryType == UITableViewCellAccessoryNone) {
            // Set current cell as checked
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
            
            // remember current IndexPath now and the corresponding group
            self.lastSelLocnIdxPath = indexPath;
            self.currentLocationSelected = location;
        }
        else if (cell.accessoryType == UITableViewCellAccessoryCheckmark ) {
            cell.accessoryType = UITableViewCellAccessoryNone;
            
        }
        else {
            NSLog(@"LocationSViewController: Some error");
        }
        
        [self finishSelection];
    }
    else if (self.runInMode == EditSingleSelection) {
        UITableViewCell * cell = [self.tableView  cellForRowAtIndexPath:indexPath];
        [cell setSelected:NO animated:NO];
        [self performSegueWithIdentifier:@"ShowLocationViewController" sender:self];
    }
    else if (self.runInMode == ManualEntrySingleSelection ) {
        UITableViewCell * cell = [self.tableView  cellForRowAtIndexPath:indexPath];
        [cell setSelected:NO animated:NO];
        [self performSegueWithIdentifier:@"ShowManualEntryViewController" sender:self];
    }
}


- (BOOL) tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return  YES;
}
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}
- (BOOL)tableView:(UITableView *)tableview shouldIndentWhileEditingRowAtIndexPath:(NSIndexPath *)indexPath {
    return NO;
}

// NOT used since drag n drop reordering is active, the delegate of BVReoder is being called and used
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
    // If no change return
    if ( (fromIndexPath.row == toIndexPath.row) && (fromIndexPath.section == toIndexPath.section ) ) {
        return;
    }
    
    // Get the Tasks which are moving for reference and logging
    Location *fromGroup = (Location *)[[self fetchedResultsController] objectAtIndexPath:fromIndexPath];
    Location *toGroup   = (Location *)[[self fetchedResultsController] objectAtIndexPath:toIndexPath];
    NSLog(@"itemMoved: %@ to: %@ FsortOrder: %i toSO: %i FromIdx: %li to: %li", fromGroup.name, toGroup.name, [fromGroup.sort_order intValue], [toGroup.sort_order intValue], (long)fromIndexPath.row, (long)toIndexPath.row);
    
    
    // Find out the direction we moving the objects. Use the direction to update the sort_order of the impacted objects
    int moveDirection = 1; // UP => +1 MOVE DOWN => -1, 0 => NOT SET
    
    // Retrieve the lower & higher row indexes. We will move from top to bottom updating the items impacted, consider excluding the item actually moving
    NSIndexPath *lowerIndexPath  = toIndexPath;
    NSIndexPath *higherIndexPath = fromIndexPath;
    if (fromIndexPath.row < toIndexPath.row) {
        // Move items one position upwards
        moveDirection = -1; // MOVE ITEM DOWN
        
        lowerIndexPath = fromIndexPath;
        higherIndexPath = toIndexPath;
    }
    
    Location *a = (Location *) [self.fetchedResultsControllerObj objectAtIndexPath:lowerIndexPath];
    Location *b = (Location *) [self.fetchedResultsControllerObj objectAtIndexPath:higherIndexPath];
    
    // Prepare sortOrder from lower to higher to prepare right query extracting all items impacted. Excluding the item which is impacted bcz for it, would be a value jump
    // Either the top item when moving item down or the last item when moving up
    // Before doing this hold the value of the final destination item to use later as this item is getting updated in a sequential update
    int toSortOrderBak = [toGroup.sort_order intValue];
    
/* Disabled the logic of skipping the moved record as that was leading to a stray entry, the last row, after the move was completed.
    int sortOrderA = 0;
    int sortOrderB = 0;
    if ( moveDirection == 1 ) { // MOVING UP
        sortOrderA = [a.sort_order intValue];
        sortOrderB = [b.sort_order intValue] - 1; // excluding the item itself
    }
    else {
        sortOrderA = [a.sort_order intValue] + 1; // excluding the item itself
        sortOrderB = [b.sort_order intValue];
    }
 */
    int sortOrderA = [a.sort_order intValue] ;
    int sortOrderB = [b.sort_order intValue] ;
    NSLog(@"ItemMoved: FsortOrder: %d toSO: %d FromIdx: %ld to: %ld Direction:%i SortOrderRange(%i:%i) %@ to: %@ ", [fromGroup.sort_order intValue], [toGroup.sort_order intValue], (long)fromIndexPath.row, (long)toIndexPath.row, (int)moveDirection, sortOrderA, sortOrderB, fromGroup.name, toGroup.name);
    
    
    // Search items which can be updated in a sequential flow
    NSArray *fetchedResults = fetchedResults = [CoreDataOperations locationBetweenSortOrder:sortOrderA AndSortOrder:sortOrderB];
 
    for (int i=0; i < [fetchedResults count]; i++)         {
        
        Location *item = (Location *) [fetchedResults objectAtIndex:i];
        NSNumber *newPosition = [NSNumber numberWithInt: sortOrderA + i + moveDirection];
        
        if ( moveDirection == 1 ) { // MOVING UP
            if ( i == [fetchedResults count]-1 ) {
                newPosition = [NSNumber numberWithInt: toSortOrderBak];
                NSLog(@"Moving UP: Last Item Handled.");
            }
        }
        else {
            if (i == 0 ) {
                newPosition = [NSNumber numberWithInt: toSortOrderBak];
                NSLog(@"Moving DOWN: First Item Handled.");
            }
        }
        
        NSLog(@"ItemMoved: InSeqItem CurSortOrder:%i NewSortOrder:%d Name:%@",  [item.sort_order intValue], [newPosition intValue], item.name);
        
        item.sort_order =  newPosition;
    }
    
    // Using the from group here was the primary culprit in the problem, hopefully, so including the moved item in teh search and handling its update in the for loop above.
    // Finally modify the item which is actually moved using the value to the destination item
//    NSLog(@"ItemMoved: MovedItem CurSortOrder:%i NewSortOrder:%d Name:%@", [fromGroup.sort_order intValue], toSortOrderBak, fromGroup.name);
//    fromGroup.sort_order = [NSNumber numberWithInt: toSortOrderBak];
    
    //    [self forceReloadThisUI];
    //  [tableView reloadData]; // It continued giving duplicate rows when scrolling more than 5 or more records or moving down ful and moving back up. TO prevent, refershing the load.
    
    // Saving the context at the end when rearranging is finally completed. doing it here was leading to some rows getting deleted but not getting inserted.
    //    [AppUtilities saveContext];
//    [CoreDataOperations saveContext]; // Saving here was leaving a last row not included when we press the done button and try edit again. so disbaled and saving on done button click.
    NSLog(@"ItemMoved: Updated Sort Order for impacted items, including completed tasks even if they are visible or not.");
}

-(void)setEditing:(BOOL)editing animated:(BOOL)animated {
    
    if (editing) {
        [super setEditing:YES animated:YES];  //Do something for edit mode
    }
    else {
        [super setEditing:NO animated:YES];  //Do something for non-edit mode
        NSError *error;
        if (![self.fetchedResultsControllerObj.managedObjectContext save:&error]) {
            NSLog(@"GroupAddEditViewController: Could not save context: %@", [error description]);
        }
    }
}

- (void) finishSelection {
    
    [self.delegate locationSViewControllerDidFinish:self OnLocation:self.currentLocationSelected LocationType:self.locationType];
    
    [self.navigationController popViewControllerAnimated:YES];
}


#pragma mark - Segue management
- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"ShowLocationViewController"]) {
        
        LocationViewController *controller = (LocationViewController *)[segue destinationViewController];
        
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        
        Location *location = (Location *)[[self fetchedResultsController] objectAtIndexPath:indexPath];

        // to get the events, had to set the delegate as self. Set the location we want to view
        controller.delegate = self;
        controller.selectedLocation = location;
        if (location == nil) {
            // User clicked on add button and we need to run add mode
            controller.locOperationMode = LocationADD;
        }
        else {
            controller.locOperationMode = LocationEDIT;
        }
        // Perform deselection to avoid setting last selection when pushing from add button
        [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
        
        // Hides the bottom bar when pushed.
//        self.hidesBottomBarWhenPushed = YES;
    }
    else if ( [[segue identifier] isEqualToString:@"ShowManualEntryViewController"] ) {
        
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        Location *loc = (Location *)[[self fetchedResultsController] objectAtIndexPath:indexPath];
        ManualEntryViewController *editLocationObj = [segue destinationViewController];
        editLocationObj.selectedLocation = loc;
    }
}


#pragma mark - Fetched results controller

-(void) hideHelpBubble {
    if (helpView)
        [helpView removeFromSuperview];
}

- (void)showHelpBubble
{
    // Show a toast alert if applicable (no location monitored and not shown an alert already
    NSInteger count = [[[self fetchedResultsController] fetchedObjects] count];
    
    if (self.runInMode == ManualEntrySingleSelection) {
        // Show Toast
        if ( count == 0 && !self.helpAlertAppeardOnce ) {
            
            // Make toast with an image
            [self.view makeToast:@"Please add locations for which you would like to automatically track your visits!"
                        duration:10.0
                        position:@"top"
                           image:nil];
            
            self.helpAlertAppeardOnce = YES;
        }
    }
    else {
        // Show Help Bubble
        if (count<1) {
            
//            helpView=[[HelpBubble alloc]init];

//            
//            
//            CGFloat constrainHeight = MAXFLOAT;
//            CGFloat constrainWidth  = 250.0;
            helpString       = @"Start by adding a location to monitor.";
//            CGSize constrainSize = CGSizeMake(constrainWidth, constrainHeight);
//            CGSize labelSize     = [helpString sizeWithFont:[UIFont systemFontOfSize:18.0f]
//                                          constrainedToSize:constrainSize
//                                              lineBreakMode:NSLineBreakByCharWrapping];
//            CGFloat Fheight=MAX(70.0, labelSize.height+20);
//            CGFloat Fwidth=labelSize.width+20;
//            //    NSLog(@"height=%f   width=%f",Fheight,labelSize.width);
//            helpView.detailStr=helpString;
//            helpView.backgroundColor=[UIColor clearColor];
//            helpView.frame=CGRectMake(self.view.bounds.size.width-Fwidth, 0,Fwidth,Fheight+20);
//            
//            CGFloat constrainHeight = MAXFLOAT;
//            CGFloat constrainWidth  = 220.0;
//            
//            CGSize constrainSize = CGSizeMake(constrainWidth, constrainHeight);
//            CGSize labelSize     = [helpString sizeWithFont:[UIFont systemFontOfSize:17.0f]
//                                        constrainedToSize:constrainSize
//                                            lineBreakMode:NSLineBreakByCharWrapping];
//            
//            CGFloat Fheight=MAX(50.0, labelSize.height);
//            CGFloat Fwidth=labelSize.width;
//            helpView.frame=CGRectMake(self.view.bounds.size.width-Fwidth, 0,Fwidth,Fheight+20);
           
            CGFloat constrainHeight = MAXFLOAT;
            CGFloat constrainWidth  = 250.0;
            CGSize constrainSize = CGSizeMake(constrainWidth, constrainHeight);
            CGSize labelSize     = [helpString sizeWithFont:[UIFont systemFontOfSize:18.0f]
                                          constrainedToSize:constrainSize
                                              lineBreakMode:NSLineBreakByCharWrapping];
            CGFloat Fheight=MAX(70.0, labelSize.height);
            CGFloat Fwidth=labelSize.width;


            CGRect frame=CGRectMake(self.view.bounds.size.width-Fwidth, 0,Fwidth,Fheight+20);
            helpView=[[HelpBubble alloc]initWithFrame:frame strTitle:helpString pointDirection:UpperRightPoint];
            helpView.backgroundColor=[UIColor clearColor];

            [self.view addSubview:helpView];
        }
    }
}

#pragma mark - FetchedResultController
- (NSFetchedResultsController *)fetchedResultsController
{
    if (self.fetchedResultsControllerObj != nil) {
        return self.fetchedResultsControllerObj;
    }
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    // Edit the entity name as appropriate.
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Location" inManagedObjectContext:APP_DELEGATE_MGDOBJCNTXT];
    [fetchRequest setEntity:entity];
    
    // Set the batch size to a suitable number.
    [fetchRequest setFetchBatchSize:20];
    
    // Edit the sort key as appropriate.
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"sort_order" ascending:YES];
    NSArray *sortDescriptors = @[sortDescriptor];
    
    [fetchRequest setSortDescriptors:sortDescriptors];
    
    // Edit the section name key path and cache name if appropriate.
    // nil for section name key path means "no sections".
    NSFetchedResultsController *aFetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest managedObjectContext:APP_DELEGATE_MGDOBJCNTXT sectionNameKeyPath:nil cacheName:nil]; // Set cache as nil else ui was not refreshing
    aFetchedResultsController.delegate = self;
    self.fetchedResultsControllerObj = aFetchedResultsController;
    
    
	NSError *error = nil;
	if (![self.fetchedResultsController performFetch:&error]) {
        // Replace this implementation with code to handle the error appropriately.
        // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
	    NSLog(@"LocationS: Unresolved error %@, %@", error, [error userInfo]);
	    abort();
	}

    return aFetchedResultsController;
}



- (void)configureHelpCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    HelpCell *helpCell = (HelpCell *) cell;
    [helpCell resetInitialText];
}

- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    // Configure the cell to show the Tasks's details
    LocationsCell *leCell = (LocationsCell *) cell;
    [leCell resetInitialText];
    
    Location *location  = (Location *)[self.fetchedResultsController objectAtIndexPath:indexPath];

    leCell.uilLocationName.text = location.name;
    leCell.uilLocationSubtitle.text = location.address;
    
    /*
    if ( [self.delegate isKindOfClass:[RouteViewController class]] &&  ([[location objectID] isEqual: [self.selectedLocation objectID]])) {
        leCell.accessoryType = UITableViewCellAccessoryCheckmark;
    }*/

    CLRegion *region = [[AppSharedData getInstance].appLocationMgr getRegion:location.region_identifier];
    if (  region == nil )
        [leCell.imgVStatus setImage: [UIImage imageNamed:@"BT_Red"]];
    else
        [leCell.imgVStatus setImage: [UIImage imageNamed:@"BT_Green"]];

    // Extra2 is used for storing the image name
    UIImage *img = [[AppSharedData getInstance] getLocationIconImageByName:location.extra2_str];
    [leCell.locIconImageView setImage:img];
    
    // iOS7 onwards the color was very odd so setting up manually
    leCell.selectedBackgroundView            = [AppUtilities getSelectedBackgroundView];
    leCell.textLabel.highlightedTextColor    = [AppUtilities getSelectedTextHighlightColor];
}

# pragma mark Handle Finish/Cancel Invokation
-(void)willMoveToParentViewController:(UIViewController *)parent {
//    NSLog(@"LocationSViewController: This VC has has been pushed popped OR covered");
    
    if (!parent) {
        NSLog(@"LocationSViewController: moving Back");
        
        [[self delegate] locationSViewControllerDidCancel:self];
    }
}

#pragma mark - Delage Method Implementation


- (void) locationViewControllerDidCancel: (LocationViewController *) controler {
        NSLog(@"LocationSViewController: locationDetailViewController performed CANCEL");
           [self.tableView reloadData];
}
- (void) locationViewControllerDidFinish: (LocationViewController *) controller OnLocation:(Location *)location OperationType: (int) type {
    
        NSLog(@"LocationSViewController: locationDetailViewController performed FINISH");
    
    // No need to do anything with the location sent, simply reload
    
        [self.tableView reloadData];
}



// Action Event called on click. Handle In-App Purchase case
- (IBAction)AddLocationActionBtn:(id)sender {
    
    if( self.loadInProgressHoldOperations) // Do nothing
        return;
    
    if ( !INAPP_ENABLED ) {
        [self performSegueWithIdentifier:@"ShowLocationViewController" sender:self];
        return;
    }
    
    bool allowAddLocation = NO;
    
    if ( [[AppSharedData getInstance].appLocationMgr getAllRegionsCount] < MAX_FREE_REGION_ALLOWED ) {
        allowAddLocation = YES;
        NSLog(@"LocationS: Allow Adding Location - Within the range of allowed free locations.");
    }
    if ( [[AppSharedData getInstance].appInAppMgr productPurchased:INAPP_ProdIdentifier_EXT_LOCN] ) {
        allowAddLocation = YES;
        NSLog(@"LocationS: Allow Adding Location - Already purchased UNLOCK.");
    }
    
    
    // If number of regions already monitoried reached optimil point
    if ( allowAddLocation) {
        [self performSegueWithIdentifier:@"ShowLocationViewController" sender:self];
    }
    else {
        [self.refreshControl beginRefreshing];
        self.loadInProgressHoldOperations = YES;
        
        // Programmetically Show the Refresh Control
        [self.tableView setContentOffset:CGPointMake(0, -120) animated:YES];
        
        self.products = nil;
        [[AppSharedData getInstance].appInAppMgr requestProductsWithCompletionHandler:^(BOOL success, NSArray *argProducts) {
            if (success) {
                
                SKProduct * product = (SKProduct *) argProducts[0];
                NSLog(@"Name: %@", product.localizedTitle);
                self.products = argProducts;
                
                [_priceFormatter setLocale:product.priceLocale];
                
                NSString *priceStr = [_priceFormatter stringFromNumber:product.price];
                NSString *messageStr = [@"To add more locations you need to purchase unlock for: " stringByAppendingString:priceStr];
                
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Unlock Locations" message:messageStr delegate:self cancelButtonTitle:NSLocalizedString(APP_CANCEL, APP_CANCEL) otherButtonTitles: @"Buy", @"Restore", nil];
                [alert show];
            }
            [self.refreshControl endRefreshing];
            self.loadInProgressHoldOperations = NO;
        }];
    }
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    switch (buttonIndex) {

        case 1:  { // Buy
            [[AppSharedData getInstance].appInAppMgr buyProduct:[self.products objectAtIndex:0]];
            NSLog(@"Selected Option - Buy: 1");
            break;
        }
        case 2: { // Restore
            [[AppSharedData getInstance].appInAppMgr restoreCompletedTransactions];
            NSLog(@"Selected Option - Restore: 2");
            break;
        }
        case 0: {// Cancel
            NSLog(@"Selected Option - Cancel: 0");
            break;
        }
        default:
            break;
    }
}


#pragma mark - Support for Drag & Drop Reordering
// 1. select the tableview in the storyboard and use the custom class
// 2. Implement its delegate
// 3. Copied the code from my move api to this move API.
// This method is called when starting the re-ording process. You insert a blank row object into your
// data source and return the object you want to save for later. This method is only called once.
- (id)saveObjectAndInsertBlankRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSLog(@"Index: row:%i section:%i", (int)indexPath.row, (int)indexPath.section);
    //    id <NSFetchedResultsSectionInfo> sectionInfo = [[self.fetchedResultsController sections] objectAtIndex:indexPath.section];
    
    
    NSIndexPath *curIndexPath = [NSIndexPath indexPathForRow:indexPath.row inSection:indexPath.section];
    Location *curObj = (Location *) [self.fetchedResultsControllerObj objectAtIndexPath:curIndexPath];
    
    //    [_fetchedResultsController replaceObjectAtIndex:indexPath.row withObject:@"DUMMY"];
    
    /*
     NSMutableArray *section = (NSMutableArray *)[data objectAtIndex:indexPath.section];
     id object = [section objectAtIndex:indexPath.row];
     [section replaceObjectAtIndex:indexPath.row withObject:@"DUMMY"];
     */
    return curObj;
}


// This method is called when the selected row is dragged to a new position. You simply update your
// data source to reflect that the rows have switched places. This can be called multiple times
// during the reordering process.
- (void)moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
    

    NSInteger moveDirection = 1;
    NSIndexPath *lowerIndexPath = toIndexPath;
    NSIndexPath *higherIndexPath = fromIndexPath;
    if (fromIndexPath.row < toIndexPath.row) {
        // Move items one position upwards
        moveDirection = -1;
        lowerIndexPath = fromIndexPath;
        higherIndexPath = toIndexPath;
    }
    
    
    Location *fromGroup = (Location *)[[self fetchedResultsController] objectAtIndexPath:fromIndexPath];
    Location *toGroup = (Location *)[[self fetchedResultsController] objectAtIndexPath:toIndexPath];
    NSLog(@"itemMoved: %@ to: %@ FsortOrder: %i toSO: %i FromIdx: %li to: %li", fromGroup.name, toGroup.name, [fromGroup.sort_order intValue], [toGroup.sort_order intValue], (long)fromIndexPath.row, (long)toIndexPath.row);
    
    // Move all items between fromIndexPath and toIndexPath upwards or downwards by one position
    for (NSInteger i=lowerIndexPath.row; i<=higherIndexPath.row; i++) {
        NSIndexPath *curIndexPath = [NSIndexPath indexPathForRow:i inSection:fromIndexPath.section];
        Location *curObj = (Location *) [self.fetchedResultsControllerObj objectAtIndexPath:curIndexPath];
        NSNumber *newPosition = [NSNumber numberWithInteger:i+moveDirection+1];
        
        
        //        NSLog(@"CurrentObjec: %@ sortOrder:%d toOrder: %d", curObj.name, [newPosition intValue], [curObj.sort_order intValue]);
        curObj.sort_order = newPosition;
    }
    
    Location *movedObj = (Location *) [self.fetchedResultsControllerObj objectAtIndexPath:fromIndexPath];
    movedObj.sort_order = [NSNumber numberWithInteger:toIndexPath.row+1];
    /*
     NSMutableArray *section = (NSMutableArray *)[data objectAtIndex:fromIndexPath.section];
     id object = [section objectAtIndex:fromIndexPath.row];
     [section removeObjectAtIndex:fromIndexPath.row];
     
     NSMutableArray *newSection = (NSMutableArray *)[data objectAtIndex:toIndexPath.section];
     [newSection insertObject:object atIndex:toIndexPath.row];
     */
}



// This method is called when the selected row is released to its new position. The object is the same
// object you returned in saveObjectAndInsertBlankRowAtIndexPath:. Simply update the data source so the
// object is in its new position. You should do any saving/cleanup here.
- (void)finishReorderingWithObject:(id)object atIndexPath:(NSIndexPath *)indexPath; {
    NSLog(@"Index: row:%li section:%li", (long)indexPath.row, (long)indexPath.section);
    /*
     NSMutableArray *section = (NSMutableArray *)[data objectAtIndex:indexPath.section];
     [section replaceObjectAtIndex:indexPath.row withObject:object];
     // do any additional cleanup here
     */
}


@end

