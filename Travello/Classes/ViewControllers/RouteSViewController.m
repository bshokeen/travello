//
//  RouteSViewController.m
//  Travello
//
//  Created by Balbir Shokeen on 2013/12/04.
//  Copyright (c) 2013 Balbir Shokeen. All rights reserved.
//

#import "RouteSViewController.h"

@interface RouteSViewController ()

@end

@implementation RouteSViewController

@synthesize headerView = _headerView;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    // Inform Flurry
    [Flurry logEvent:@"Tab Access: Routes"];
    
    self.thisInterfaceEntityName = @"TripSummary";
    
    // Search Bar Handling: Show Search Bar as hidden in the begining
    self.uiSearchBar.showsCancelButton = NO;
    self.uiSearchBar.delegate = self;
    self.tableView.tableHeaderView = self.uiSearchBar;
    
    self.navigationItem.rightBarButtonItem = nil; // Hiding the navigation button for now. RouteS UI: Hide the right navigation button for now as it is not fully implemented in a meaningful way.
}

-(void) viewWillAppear:(BOOL)animated {
    // Localized the Cancel Button as for this app custom language it was appearing in english
    [[UIButton appearanceWhenContainedIn:[UISearchBar class], nil]
     setTitle:NSLocalizedString(@"Cancel", @"Cancel") forState:UIControlStateNormal];
    
    //    [self.tableView setContentOffset:CGPointMake(0, 44)]; // Bug Fix: It was difficult to know where the user was last if we reset the focus every time.
    // test: [self showPopTipView];
}

-(void) viewWillDisappear:(BOOL)animated {
    
    [self.uiSearchBar resignFirstResponder];
    
}

// Hide the keyboard
-(void) searchBarCancelButtonClicked:(UISearchBar *)searchBar {
    self.uiSearchBar.text = @"";
    
    //    [self.tableView setContentOffset:CGPointMake(0, 44) animated:YES]; // Tried few times and saw -20 works fine
    // On landscape and other mode it did not work well so ignored using it for now
    [searchBar resignFirstResponder];
    
    self.uiSearchBar.showsCancelButton=NO;
    [self forceReloadThisUI];
    NSLog(@"SearchBarCanced");
}

- (void) searchBarTextDidBeginEditing:(UISearchBar *)theSearchBar {
    //    ULog(@"BeginEditing");
    
    if (!self.uiSearchBar.showsCancelButton) {
        self.uiSearchBar.showsCancelButton = YES;
    }
}


-(void) searchBar:(UISearchBar *) searchBar textDidChange:(NSString *)Tosearch{
    
    [self forceReloadThisUI];
}

- (void) forceReloadThisUI
{
    // Set Nil so that during fetch it goes and fetch fresh data.
    self.fetchedResultsControllerObj = nil;
    
    // Need to see if I really need it all places to reload.
    [self.tableView reloadData];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell * cell = [self.tableView  cellForRowAtIndexPath:indexPath];
    [cell setSelected:NO animated:YES];
    
    if (indexPath.section==0 && indexPath.row == 0) { // Adding setting the first cell values as during scroll the values were wiping out. Need to see the core reason using it as workaround for now
        RoutesCell *leCell = (RoutesCell *) cell;
        leCell.startLbl.text = @"Start Around";
        leCell.aroundLbl.text = @"Reach Towards";
    }
    
    /* Disable as of now as it is not fucntional as of now
    if ( indexPath.row == 0 || indexPath.row == 1 )
        [self performSegueWithIdentifier:@"ShowRouteViewController" sender:self];
     */
}

- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    /*
    if ([[segue identifier] isEqualToString:@"ShowRouteViewController"]) {
        
        RouteViewController *controller = (RouteViewController *)[segue destinationViewController];
        
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        
        Route *route = (Route *)[[self fetchedResultsController] objectAtIndexPath:indexPath];
        
        // to get the events, had to set the delegate as self. Set the location we want to view
        controller.delegate = self;
        controller.route = route;
        controller.routeVCRunInMode = EDIT;
        
        // Perform deselection to avoid setting last selection when pushing from add button
        [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    }*/
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"RoutesCell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    // call to update the cell details
    [self configureCell:cell atIndexPath:indexPath];
    
    return cell;
}

- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    // Configure the cell to show the Tasks's details
    RoutesCell *leCell = (RoutesCell *) cell;
    [leCell resetInitialText];
    
    // For simplicity of UI clear few static labels
    if (indexPath.row != 0) {
        leCell.startLbl.text = @"";
        leCell.aroundLbl.text = @"";
    }
    
    TripSummary *tripSummary  = (TripSummary *)[self.fetchedResultsController objectAtIndexPath:indexPath];

    leCell.uilFromLocation.text = tripSummary.from_locn_name;
    leCell.uilToLocation.text   = tripSummary.to_locn_name;
    

    // Try using the additional variable if relationship is not found
    NSString *fromLcnImgStr = tripSummary.fromLocation.extra2_str == nil? tripSummary.from_locn_img : tripSummary.fromLocation.extra2_str;
    if (fromLcnImgStr == nil) fromLcnImgStr = DEFAULT_LOCN_IMGNAME;
    UIImage *fromLcnimg = [[AppSharedData getInstance] getLocationIconImageByName:fromLcnImgStr];
    [leCell.fromLocnImgView setImage:fromLcnimg];

    NSString *toLcnImgStr = tripSummary.fromLocation.extra2_str == nil? tripSummary.to_locn_img : tripSummary.toLocation.extra2_str;
    if (toLcnImgStr == nil) toLcnImgStr = DEFAULT_LOCN_IMGNAME;
    UIImage *toLcnimg = [[AppSharedData getInstance] getLocationIconImageByName:toLcnImgStr];
    [leCell.toLocnImgView setImage:toLcnimg];
    
    leCell.countIdxLbl.text = [NSString stringWithFormat:@"%i", (int)(indexPath.row + 1)];
    
    leCell.routeTripsCountLbl.text = [NSString stringWithFormat:@"%li", [tripSummary.total_trips longValue]];
    
    // Calculate the average time and get a readable date for display
    double time = [tripSummary.total_time doubleValue] / [tripSummary.total_trips intValue]; // total time/no of entries
    leCell.routeTripsAvgTimeLbl.text = [AppUtilities getReadableTime:time];
    
    // iOS7 onwards the color was very odd so setting up manually
    leCell.selectedBackgroundView            = [AppUtilities getSelectedBackgroundView];
    leCell.textLabel.highlightedTextColor    = [AppUtilities getSelectedTextHighlightColor];
}

- (NSFetchedResultsController *)fetchedResultsController
{
    if (self.fetchedResultsControllerObj != nil) {
        return self.fetchedResultsControllerObj;
    }
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    // Edit the entity name as appropriate.
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"TripSummary" inManagedObjectContext:APP_DELEGATE_MGDOBJCNTXT];
    [fetchRequest setEntity:entity];
    
    // Set the batch size to a suitable number.
    [fetchRequest setFetchBatchSize:20];
    
    // Edit the sort key as appropriate.
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"total_time" ascending:NO];
    NSArray *sortDescriptors = @[sortDescriptor];
    
    [fetchRequest setSortDescriptors:sortDescriptors];
    
    // Filter Search Text: In task name and notes
    if ([self.uiSearchBar.text length] > 0 ) {
        NSPredicate *searchPredicate = [NSPredicate predicateWithFormat:@"fromLocation.name CONTAINS[cd] %@ || toLocation.name CONTAINS[cd] %@ ", self.uiSearchBar.text, self.uiSearchBar.text];
        [fetchRequest setPredicate:searchPredicate];
    }
    
    // Edit the section name key path and cache name if appropriate.
    // nil for section name key path means "no sections".
    NSFetchedResultsController *aFetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest managedObjectContext:APP_DELEGATE_MGDOBJCNTXT sectionNameKeyPath:nil cacheName:nil]; // Set cache as nil else ui was not refreshing
    aFetchedResultsController.delegate = self;
    self.fetchedResultsControllerObj = aFetchedResultsController;
    
    
	NSError *error = nil;
	if (![self.fetchedResultsController performFetch:&error]) {
        // Replace this implementation with code to handle the error appropriately.
        // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
	    NSLog(@"LocationS: Unresolved error %@, %@", error, [error userInfo]);
	    abort();
	}
    
    if ( self.fetchedResultsControllerObj != nil && [[self.fetchedResultsControllerObj fetchedObjects] count] > 0) {
        //    self.tableView.tableHeaderView = [UI]
//        self.tableView.tableHeaderView = nil;
    }
    /*else {
        self.tableView.tableHeaderView = self.headerView;
    } DID NOT HELP */
    return aFetchedResultsController;
}


#pragma mark - RouteViewControllerDelegate
-(void) routeViewControllerDidFinish:(RouteViewController *)routeViewController FromLocation:(Location *)fromLocation ToLocation:(Location *)toLocation {
    
}

@end
