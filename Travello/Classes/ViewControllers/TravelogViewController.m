//
//  TravelogViewController.m
//  Travello
//
//  Created by Balbir Shokeen on 2013/09/06.
//  Copyright (c) 2013 Balbir Shokeen. All rights reserved.
//

#import "TravelogViewController.h"
#import "Toast+UIView.h"

@interface TravelogViewController ()

    @property bool helpAlertAppeardOnce;

@property NSIndexPath *tmpIndxPathToShareBetAPI; // To pass indexp between apis

    @property LocationLog *tmpLogVar; // To hold the value when passing it between API calls

@end

@implementation TravelogViewController

@synthesize helpAlertAppeardOnce = _helpAlertAppeardOnce, tmpIndxPathToShareBetAPI = _tmpIndxPathToShareBetAPI, tmpLogVar = _tmpLogVar;
@synthesize uiSearchBar = _uiSearchBar;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Inform Flurry
    [Flurry logEvent:@"Tab Access: Travelog"];
    
    self.thisInterfaceEntityName = @"LocationLog";
    
    self.tableView.sectionHeaderHeight = HEADER_HEIGHT; // Setting a custom height to get proper height to have a visible notice of the section header.
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Display an Edit button in the navigation bar for this view controller.
//    self.navigationItem.leftBarButtonItem = self.editButtonItem; // As now we use this view in sequence from another, we don't want edit to appear instead of back button.
    
    // Search Bar Handling: Show Search Bar as hidden in the begining
//    self.uiSearchBar = [[UISearchBar alloc] initWithFrame:CGRectMake(0, 0, 320, 44)];
    self.uiSearchBar.showsCancelButton = NO;
    self.uiSearchBar.delegate = self;
    self.tableView.tableHeaderView = self.uiSearchBar;
}

-(void) viewWillAppear:(BOOL)animated {
    // Localized the Cancel Button as for this app custom language it was appearing in english
    [[UIButton appearanceWhenContainedIn:[UISearchBar class], nil]
     setTitle:NSLocalizedString(@"Cancel", @"Cancel") forState:UIControlStateNormal];
    
//    [self.tableView setContentOffset:CGPointMake(0, 44)]; // Bug Fix: It was difficult to know where the user was last if we reset the focus every time.
    // test: [self showPopTipView];
}

-(void) viewWillDisappear:(BOOL)animated {
    
    [self.uiSearchBar resignFirstResponder];
    
}

// Hide the keyboard
-(void) searchBarCancelButtonClicked:(UISearchBar *)searchBar {
    self.uiSearchBar.text = @"";
    
//    [self.tableView setContentOffset:CGPointMake(0, 44) animated:YES]; // Tried few times and saw -20 works fine
    // On landscape and other mode it did not work well so ignored using it for now
    [searchBar resignFirstResponder];
    
    self.uiSearchBar.showsCancelButton=NO;
    [self forceReloadThisUI];
    NSLog(@"SearchBarCanced");
}

- (void) searchBarTextDidBeginEditing:(UISearchBar *)theSearchBar {
    //    ULog(@"BeginEditing");
    
    if (!self.uiSearchBar.showsCancelButton) {
        self.uiSearchBar.showsCancelButton = YES;
    }
}


-(void) searchBar:(UISearchBar *) searchBar textDidChange:(NSString *)Tosearch{

    [self forceReloadThisUI];
}


- (void) forceReloadThisUI
{
    // Set Nil so that during fetch it goes and fetch fresh data.
    self.fetchedResultsControllerObj = nil;
    
    // Need to see if I really need it all places to reload.
    [self.tableView reloadData];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"TravelogCell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    // call to update the cell details
    [self configureCell:cell atIndexPath:indexPath];
    return cell;
}


- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    
    id <NSFetchedResultsSectionInfo> sectionInfo = [[self.fetchedResultsController sections] objectAtIndex:section];
    return [sectionInfo name];
}

-(UIView*)tableView:(UITableView*)tableView viewForHeaderInSection:(NSInteger)section {
    NSLog(@"View Header In Section:%i   Start", (int)section);
    
    id <NSFetchedResultsSectionInfo> sectionInfo = [[self.fetchedResultsController sections] objectAtIndex:section];
    NSString *sectionName =  [sectionInfo name];
    
    NSInteger sectionItemCount = [sectionInfo numberOfObjects];
    
    // Initialize custom header section view using custom prototype cell
    
    SectionHeaderView *sectionHeaderView = [[SectionHeaderView alloc]
                                                    initWithFrame:CGRectMake(0.0, 0.0, self.tableView.bounds.size.width, self.tableView.bounds.size.height)                                                                                              title:sectionName
                                                    section:section
                                                    sectionItemCount:sectionItemCount
                                                    HeaderIndex:(section + 1)
                                                    TagNumber: (SECTION_NUMBER_STARTFROM + section) //// To identify a group section for updates later
                                                    delegate:self];
    
    NSLog(@"viewForHeaderInSection: %i   ItemCount:%i End", (int)section, (int)sectionItemCount);
    return sectionHeaderView;
}


/* Disable Section Index
- (NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView {
    return [self.fetchedResultsController sectionIndexTitles];
}

- (NSInteger)tableView:(UITableView *)tableView sectionForSectionIndexTitle:(NSString *)title atIndex:(NSInteger)index {
    return [self.fetchedResultsController sectionForSectionIndexTitle:title atIndex:index];
}
 */

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell * cell = [self.tableView  cellForRowAtIndexPath:indexPath];
    [cell setSelected:NO animated:YES];
    
    if (indexPath.section==0 && indexPath.row == 0) { // Adding setting the first cell values as during scroll the values were wiping out. Need to see the core reason using it as workaround for now
        TravelogCell *leCell = (TravelogCell *) cell;
        leCell.inLbl.text = @"IN";
        leCell.outLbl.text = @"OUT";
    }
    // Switch Cell Selection to take you to editing instead of Location
    [self performSegueWithIdentifier:@"ShowLocationViewController" sender:self];
    
}

- (void)tableView:(UITableView *)tableView1 accessoryButtonTappedForRowWithIndexPath:(NSIndexPath *)indexPath {
    self.tmpIndxPathToShareBetAPI = indexPath;
    [self performSegueWithIdentifier:@"ShowManualEntryViewController" sender:self];
}

#pragma mark - Segue management
- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"ShowLocationViewController"]) {
    
        LocationViewController *controller = (LocationViewController *)[segue destinationViewController];
        
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        
        LocationLog *locationLog = (LocationLog *)[[self fetchedResultsController] objectAtIndexPath:indexPath];
        
        // to get the events, had to set the delegate as self. Set the location we want to view
        controller.delegate = self;
        controller.selectedLocation = locationLog.location;
        controller.locOperationMode = LocationREADONLY;

        // Perform deselection to avoid setting last selection when pushing from add button
        [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
    }
    else if ( [[segue identifier] isEqualToString:@"ShowLocationSViewController_ManualEntry"]) {
        LocationSViewController *controller = (LocationSViewController *)[segue destinationViewController];
        
        // to get the events, had to set the delegate as self. Set the location we want to view
        controller.delegate = self;
        controller.runInMode = ManualEntrySingleSelection;
    }
    else if ( [[segue identifier] isEqualToString:@"ShowManualEntryViewController"]) {
        /*
        ManualEntryViewController *controller = (ManualEntryViewController *)[segue destinationViewController];
        
        LocationLog *locationLog = (LocationLog *)[[self fetchedResultsController] objectAtIndexPath:self.tmpIndxPathToShareBetAPI];
        
        controller.selectedLocationLog = locationLog;
         */
        
         ManualEntryViewController *controller = (ManualEntryViewController *)[segue destinationViewController];
        controller.selectedLocationLog = self.tmpLogVar;
    }
}
#pragma mark - Fetched results controller

- (NSFetchedResultsController *)fetchedResultsController
{
    if (self.fetchedResultsControllerObj != nil) {
        return self.fetchedResultsControllerObj;
    }
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    // Edit the entity name as appropriate.
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"LocationLog" inManagedObjectContext:APP_DELEGATE_MGDOBJCNTXT];
    [fetchRequest setEntity:entity];
    
    // Set the batch size to a suitable number.
    [fetchRequest setFetchBatchSize:20];
    
    // Edit the sort key as appropriate.
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"created_on" ascending:NO];
    NSArray *sortDescriptors = @[sortDescriptor];
    
    [fetchRequest setSortDescriptors:sortDescriptors];
    
    // Filter Search Text: In task name and notes
    if ([self.uiSearchBar.text length] > 0 ) {
        NSPredicate *searchPredicate = [NSPredicate predicateWithFormat:@"location.name CONTAINS[cd] %@", self.uiSearchBar.text];
        [fetchRequest setPredicate:searchPredicate];
    }
    
    // Edit the section name key path and cache name if appropriate.
    // nil for section name key path means "no sections".
    NSFetchedResultsController *aFetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest managedObjectContext:APP_DELEGATE_MGDOBJCNTXT sectionNameKeyPath:@"entryDaySectionName" cacheName:nil];
    aFetchedResultsController.delegate = self;
    self.fetchedResultsControllerObj = aFetchedResultsController;

	NSError *error = nil;
	if (![self.fetchedResultsController performFetch:&error]) {
        // Replace this implementation with code to handle the error appropriately.
        // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
	    NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
	    abort();
	}
    
    // Show a toast alert if applicable (no location monitored and not shown an alert already
    if (self.fetchedResultsControllerObj != nil) {
        int count = (int)[[self.fetchedResultsControllerObj fetchedObjects] count];
        if ( count == 0 && !self.helpAlertAppeardOnce && [self.uiSearchBar.text length]==0) { // added searhbar len else message was coming during record filter
            
            /*
            // Make toast with an image
            [self.view makeToast:@"Entries would appear automatically on ENTER or EXIT from configured locations."
                        duration:15.0
                        position:@"center"
                           image:nil];
             */
            
            [AppUtilities showSimpleAlertWithMessage:@"Please leave the app running in background!!!\n\nYou will  see the time spent at each configured location as you move IN or OUT. " Title:@"IMPORTANT"];
            
            self.helpAlertAppeardOnce = YES;
        }
    }
    return aFetchedResultsController;
}

- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    // Configure the cell to show the Tasks's details
    TravelogCell *leCell = (TravelogCell *) cell;
    [leCell resetInitialText];
    
    // For simplicity of UI clear few static labels
    if (indexPath.row != 0) {
        leCell.inLbl.text  = @"";
        leCell.outLbl.text = @"";
    }
    
    LocationLog *locLog  = (LocationLog *)[self.fetchedResultsController objectAtIndexPath:indexPath];
    
    static NSDateFormatter *formatterDate = nil;
    if (formatterDate == nil) {
        formatterDate = [[NSDateFormatter alloc] init];
        [formatterDate setDateStyle:NSDateFormatterMediumStyle];
    }
    static NSDateFormatter *formatterTime = nil;
    if (formatterTime == nil) {
        formatterTime = [[NSDateFormatter alloc] init];
        [formatterTime setDateStyle:NSDateFormatterNoStyle];
        [formatterTime setTimeStyle:NSDateFormatterShortStyle];
    }
    
    leCell.locationName.text = locLog.location.name;
    NSString *tmpEnterStr = (locLog.did_enter == nil) ? APP_SPACED_HYPHEN : [formatterTime stringFromDate:locLog.did_enter];
    NSString *tmpExitStr  = (locLog.did_exit  == nil) ? APP_SPACED_HYPHEN : [formatterTime stringFromDate:locLog.did_exit];
    
    leCell.didEnterTime.text = tmpEnterStr;
    leCell.didExitTime.text = tmpExitStr;
    
    // Get a more readable time for showing up on UI
    leCell.duration.text = [AppUtilities getReadableTimeWithStartDate:locLog.did_enter WithEndDate:locLog.did_exit];
    
    UIImage *img = [[AppSharedData getInstance] getLocationIconImageByName:locLog.location.extra2_str];
    [leCell.locIconImageView setImage:img];
    
    // Added to enable receiving the editEntyBtnClick which handled later in this program
    leCell.delegate = self;
    leCell.locationLog = locLog;
    if ( locLog.did_enter != nil && locLog.did_exit != nil) {
        leCell.editEntryBtn.hidden = YES; // Hide this button if both the dates are present
    } else {
        leCell.editEntryBtn.hidden = NO;
    }
    
    // iOS7 onwards the color was very odd so setting up manually
    leCell.selectedBackgroundView            = [AppUtilities getSelectedBackgroundView];
    leCell.textLabel.highlightedTextColor    = [AppUtilities getSelectedTextHighlightColor];
    
    /*
    UIButton* pencil = [UIButton buttonWithType:UIButtonTypeCustom];
    [pencil setImage:[UIImage imageNamed:@"Pencil.png"] forState:UIControlStateNormal];
    pencil.frame = CGRectMake(5, 5, 5, 5);
    pencil.userInteractionEnabled = YES;
    [pencil addTarget:self action:@selector(didTapEditButton:)      forControlEvents:UIControlEventTouchDown];
    //    [leCell.accessoryView convertRect:CGRectMake(0, 0, 10, 10) fromView:leCell.accessoryView];
    //    leCell.accessoryView.frame = CGRectMake(0, 0, 10, 10);
    leCell.accessoryView = pencil;
     */
}

#pragma mark - Delage Method Implementation

- (void) locationViewControllerDidCancel: (LocationViewController *) controler {
    NSLog(@"LocationLogViewController: locationViewController performed CANCEL");
}
- (void) locationViewControllerDidFinish: (LocationViewController *) controller OnLocation:(Location *)location OperationType: (int) type {
    
    NSLog(@"LocationLogViewController: locationViewController performed FINISH");
    
    // No need to do anything with the location sent, simply reload
    
    [self.tableView reloadData];
}

#pragma mark - LocationSViewController Delegate APIs
-(void) locationSViewControllerDidCancel:(LocationSViewController *)controler {
    NSLog(@"LocationLogViewController: locationSViewController performed CANCEL");
}
-(void) locationSViewControllerDidFinish:(LocationSViewController *)controller OnLocation:(Location *)location LocationType:(int)type {
    NSLog(@"LocationLogViewController: locationSViewController performed FINISH");
}
/*
-(IBAction)buttonPressed:(id)sender {
    
    switch ([sender tag]) {
            
        case 0: {
            // Make toast
            [self.view makeToast:@"This is a piece of toast."];
            break;
        }
            
        case 1: {
            // Make toast with a title
            [self.view makeToast:@"This is a piece of toast with a title."
                        duration:3.0
                        position:@"top"
                           title:@"Toast Title"];
            
            break;
        }
            
        case 2: {
            // Make toast with an image
            [self.view makeToast:@"This is a piece of toast with an image."
                        duration:3.0
                        position:@"center"
                           image:[UIImage imageNamed:@"toast.png"]];
            break;
        }
            
        case 3: {
            // Make toast with an image & title
            [self.view makeToast:@"This is a piece of toast with a title & image"
                        duration:3.0
                        position:@"bottom"
                           title:@"Toast Title"
                           image:[UIImage imageNamed:@"toast.png"]];
            break;
        }
            
        case 4: {
            // Show a custom view as toast
            UIView *customView = [[[UIView alloc] initWithFrame:CGRectMake(0, 0, 80, 400)] autorelease];
            [customView setAutoresizingMask:(UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin)]; // autoresizing masks are respected on custom views
            [customView setBackgroundColor:[UIColor orangeColor]];
            
            [self.view showToast:customView
                        duration:2.0
                        position:@"center"];
            
            break;
        }
            
        case 5: {
            // Show an imageView as toast, on center at point (110,110)
            UIImageView *toastView = [[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"toast.png"]] autorelease];
            
            [self.view showToast:toastView
                        duration:2.0
                        position:[NSValue valueWithCGPoint:CGPointMake(110, 110)]]; // wrap CGPoint in an NSValue object
            
            break;
        }
            
        case 6: {
            if (_isShowingActivity) {
                [_activityButton setTitle:@"Hide Activity" forState:UIControlStateNormal];
                [self.view makeToastActivity];
            } else {
                [_activityButton setTitle:@"Show Activity" forState:UIControlStateNormal];
                [self.view hideToastActivity];
            }
            _isShowingActivity = !_isShowingActivity;
            break;
        }
            
        default: break;
            
    }
}
*/


#pragma mark - Section Header View Delegate
-(void) sectionHeaderViewFinish:(SectionHeaderView *)sectionHeaderView Section:(long)section Operation:(long)sectionOperation {
    // NOT USED YET
}


#pragma mark - TravelogCellDelgate API
-(void) notifyTravelogCelBtnClick:(LocationLog *)editLocationLog {
    self.tmpLogVar = editLocationLog; // First keep a temporary variable to use before doing the segue in the segue handling.
    [self performSegueWithIdentifier:@"ShowManualEntryViewController" sender:self];
}

- (IBAction)entryBtnClicked:(id)sender {
    // Manually
    
}
@end
