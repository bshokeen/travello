//
//  LocationIconCollViewController.m
//  Travello
//
//  Created by Balbir Shokeen on 2013/09/14.
//  Copyright (c) 2013 Dasheri. All rights reserved.
//

#import "LocationIconViewController.h"
#import "LocationIconCell.h"
#import "AppSharedData.h"
#import "AppConstants.h"
@interface LocationIconViewController ()

@end

@implementation LocationIconViewController

@synthesize delegate = _delegate;
@synthesize selectedLocIconName = _selectedLocIconName;


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Inform Flurry
    [Flurry logEvent:@"View Access: LocationIcon"];
    
    arrRecords = [[AppSharedData getInstance].locationIcons allKeys];
    noOfSection = [arrRecords count];
    
    if (self.selectedLocIconName == nil ) {
        self.selectedLocIconName = DEFAULT_LOCN_IMGNAME; // Hardcoding to select some image
    }
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    
    return YES;
}

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration{
    
    if ((toInterfaceOrientation == UIInterfaceOrientationLandscapeLeft) || (toInterfaceOrientation == UIInterfaceOrientationLandscapeRight)) {
        noOfSection = 4;
    }else{
        noOfSection = 3;
    }
    [self.collectionView reloadData];
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [arrRecords count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    LocationIconCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"LocationIconCell" forIndexPath:indexPath];
    
    NSString *key = [arrRecords objectAtIndex:indexPath.row];
    UIImage *img = [[AppSharedData getInstance].locationIcons objectForKey:key];
    
    [cell.locImageView setImage:img];

    if (![self.selectedLocIconName isEqualToString:key]) {
        [cell.locSelectedImageView setImage:nil];
    }
    
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    long idx = indexPath.row;
    
    NSString *key = [arrRecords objectAtIndex:idx];
    
    [self.delegate locationIconViewControllerDidFinish:self SelectedKey:key];
    
    [self.navigationController popViewControllerAnimated:YES];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
