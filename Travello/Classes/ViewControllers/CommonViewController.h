//
//  CommonViewController.h
//  Travello
//
//  Created by Balbir Shokeen on 2013/08/31.
//  Copyright (c) 2013 Balbir Shokeen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "CoreDataOperations.h"
#import "AppUtilities.h"
#import "AppSharedData.h"


@interface CommonViewController : UITableViewController <NSFetchedResultsControllerDelegate>

// This object is actually populated by the subclasses
@property (nonatomic, strong) NSFetchedResultsController *fetchedResultsControllerObj;

@property (nonatomic, strong) NSString *thisInterfaceEntityName;

- (void) forceReloadThisUI;
- (NSFetchedResultsController *)fetchedResultsController;

@end
