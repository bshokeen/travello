//
//  CommonViewController.m
//  Travello
//
//  Created by Balbir Shokeen on 2013/08/31.
//  Copyright (c) 2013 Balbir Shokeen. All rights reserved.
//

#import "CommonViewController.h"
#import "SectionHeaderView.h"
#import "TripCell.h"
#import "TravelogCell.h"
#import "RouteSCell.h"

@interface CommonViewController ()

@end

@implementation CommonViewController

@synthesize fetchedResultsControllerObj = _fetchedResultsControllerObj, thisInterfaceEntityName = _thisInterfaceEntityName;


- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

}

- (void) viewWillAppear:(BOOL)animated {
    // Added this line to fix the bug which was crashing the app when filter ui is selected on a group and try to add the task
    self.fetchedResultsControllerObj = nil;
    
    // Load the data on first run
    [self forceReloadThisUI];
}


- (void) viewWillDisappear:(BOOL)animated {
    // Added this line to fix the bug which was crashing the app when filter ui is selected on a group and try to add the task
    self.fetchedResultsControllerObj = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    
    return YES;
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    
    // Release any retained subviews of the main view.
    self.fetchedResultsControllerObj = nil;
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    NSUInteger count = [[[self fetchedResultsController] sections] count];
    NSLog(@"numberOfSectionsInTableView: %i", (int)count);
    return count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    id <NSFetchedResultsSectionInfo> sectionInfo = [[[self fetchedResultsController] sections] objectAtIndex:section];
    NSLog(@"noOfRowsInSection: END    Section:%i Returned Actual Row Count (%i).", (int)section, (int)[sectionInfo numberOfObjects]);
    return [sectionInfo numberOfObjects];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    /*     // Tried using TaskViewCell.xib, but found issue of setting segues, initializatin so leave it unless gain more knowledge
     if (cell == nil) {
     NSArray *nib =  [[NSBundle mainBundle] loadNibNamed:cellIdentifier owner:self options:nil];
     cell = (TaskViewCell *) [nib objectAtIndex:0];
     }*/
    
    // call to update the cell details
    [self configureCell:[tableView dequeueReusableCellWithIdentifier:cellIdentifier] atIndexPath:indexPath];
    return cell;
}

// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}


// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        NSLog(@"CommonViewController: commitEditingStyle: Starting Delete");
        NSManagedObject *obj = [[self fetchedResultsController] objectAtIndexPath:indexPath];
        
        if ( [self.thisInterfaceEntityName isEqualToString:@"Location"]) {
            // We have to delete the region or its monitoring
            // Stop monitoring the region, remove the radius overlay, and finally remove the annotation from the map.
            Location * location = (Location *) obj;
            if (location) {
                
                NSArray *locationLogs = [location.locationLogS allObjects];
                for (LocationLog *locLog in locationLogs) {
                    [CoreDataOperations deleteItemByEntityName:@"LocationLog" Item:locLog];
                }
                
                CLRegion *region = [[AppSharedData getInstance].appLocationMgr getRegion:location.region_identifier];
                [[AppSharedData getInstance].appLocationMgr stopMonitoringRegion:region];
            }
        }
        [CoreDataOperations deleteItemByEntityName:self.thisInterfaceEntityName Item:obj];
        // No need to delete the core data object here as it is deleted elsewhere.
    }
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }
    NSLog(@"CommonViewController: commitEditingStyle: FINISHED Operation");
}


// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}

// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}


#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here. Create and push another view controller.

}

#pragma mark - Fetched results controller

-(NSFetchedResultsController *) fetchedResultsController {
    
    @throw [NSException exceptionWithName:NSInternalInconsistencyException
                                   reason:[NSString stringWithFormat:@"You must override %@ in a subclass", NSStringFromSelector(_cmd)]
                                 userInfo:nil];
    return  nil;
}

- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller
{
    [self.tableView beginUpdates];
}

- (void)controller:(NSFetchedResultsController *)controller didChangeSection:(id <NSFetchedResultsSectionInfo>)sectionInfo
           atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type
{
    NSLog(@"Dynamic: didChangeSection: Type: %i, atIndexSection: %i ", (int)type, (int)sectionIndex);
    
    switch(type) {
        case NSFetchedResultsChangeInsert:
            [self.tableView insertSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [self.tableView deleteSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}

- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"Throw excetion: Need to override this method");
}

- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject
       atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type
      newIndexPath:(NSIndexPath *)newIndexPath
{
    NSLog(@"Dynamic: didChangeObject: Type: %i, atIdxSection: %i newIdxSection: %i atIdxRow: %i,  newIdxRow: %i", (int)type, (int)indexPath.section, (int)newIndexPath.section, (int)indexPath.row, (int)newIndexPath.row);

    
    UITableView *tableView = self.tableView;
    
    switch(type) {
        case NSFetchedResultsChangeInsert: {
            [tableView insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            NSLog(@"Inserted");
            
            // Logic to update count of travelog entries for a day on deleting the entries
            if ( [self.thisInterfaceEntityName isEqualToString:@"LocationLog"] ) {
                SectionHeaderView *view =  (SectionHeaderView *) [self.tableView viewWithTag:(SECTION_NUMBER_STARTFROM + indexPath.section)];
                
                if ( [view isKindOfClass:[ SectionHeaderView class]] ) {
                    [view updateCount: +1]; // update name for rows deleted arg
                }
                else {
                    NSLog(@"Error, unable to find the GroupHeaderSectionView, need to debug.");
                }
                
                // Clear the last cell static label
                TravelogCell *travelogCell = (TravelogCell*)[self.tableView cellForRowAtIndexPath:newIndexPath];
                if (travelogCell) {
                    travelogCell.inLbl.text = APP_BLANK;
                    travelogCell.outLbl.text = APP_BLANK;
                }
            }
            
            if ( [self.thisInterfaceEntityName isEqualToString:@"TripSummary"] ) { // Clear the last cell static label
                RoutesCell *lastCell = (RoutesCell*)[self.tableView cellForRowAtIndexPath:newIndexPath];
                if (lastCell) {
                    lastCell.startLbl.text = APP_BLANK;
                    lastCell.aroundLbl.text = APP_BLANK;
                }
            }
            if ( [self.thisInterfaceEntityName isEqualToString:@"Trip"] ) {  // Clear the last cell static label
                // Cell to be cleared off is still at the newIndexPath index i.e section=0 row=0
                TripCell *lastCell = (TripCell*)[self.tableView cellForRowAtIndexPath:newIndexPath];
                if(lastCell) {
                lastCell.startLbl.text = APP_BLANK;
                lastCell.aroundLbl.text = APP_BLANK;
                }
            }

            break;
        }
        case NSFetchedResultsChangeDelete: {
            [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            NSLog(@"Deleted");
            
            // Logic to update count of travelog entries for a day on deleting the entries
            if ( [self.thisInterfaceEntityName isEqualToString:@"LocationLog"] ) {
                SectionHeaderView *view =  (SectionHeaderView *) [self.tableView viewWithTag:(SECTION_NUMBER_STARTFROM + indexPath.section)];
                
                if ( [view isKindOfClass:[ SectionHeaderView class]] ) {
                    [view updateCount: -1]; // update name for rows deleted arg
                }
                else {
                    NSLog(@"Error, unable to find the GroupHeaderSectionView, need to debug.");
                }
            }

            break;
        }
        case NSFetchedResultsChangeUpdate: {
            [self configureCell:[tableView cellForRowAtIndexPath:indexPath] atIndexPath:indexPath];
            NSLog(@"Updated");
            break;
        }
        case NSFetchedResultsChangeMove: {
            [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            [tableView insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            NSLog(@"Moved");
            break;
        }
    }
}


// Implementing the above methods to update the table view in response to individual changes may have performance implications if a large number of changes are made simultaneously. If this proves to be an issue, you can instead just implement controllerDidChangeContent: which notifies the delegate that all section and object changes have been processed.

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller
{
    // In the simplest, most efficient, case, reload the table view.
    // [self.tableView reloadData];
    [self.tableView endUpdates];
}


#pragma mark - DB Operations


- (void) forceReloadThisUI
{
    [self.tableView reloadData];
    return;
    
}

@end
