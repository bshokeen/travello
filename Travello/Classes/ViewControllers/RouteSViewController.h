//
//  RouteSViewController.h
//  Travello
//
//  Created by Balbir Shokeen on 2013/12/04.
//  Copyright (c) 2013 Balbir Shokeen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CommonViewController.h"
#import "RouteViewController.h"
#import "RoutesCell.h"
#import "Route.h"

@interface RouteSViewController : CommonViewController <RouteViewControllerDelegate, UISearchBarDelegate>
@property (weak, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet UISearchBar *uiSearchBar;
@end
