//
//  RouteViewController.h
//  Travello
//
//  Created by Balbir Shokeen on 2013/12/04.
//  Copyright (c) 2013 Balbir Shokeen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RouteLog.h"
#import "Route.h"
#import "Location.h"
#import "LocationLog.h"
#import "LocationSViewController.h"

@protocol RouteViewControllerDelegate;

// Help running this view for selection or editing and going further to detail ui
typedef enum { ADD = 1, EDIT = 2 } ROUTEVC_RUN_IN_MODE;

@interface RouteViewController : UITableViewController <LocationSViewControllerDelegate>


@property (nonatomic) ROUTEVC_RUN_IN_MODE routeVCRunInMode;
@property(nonatomic,strong) RouteLog *routeLog;
@property (nonatomic, strong) Route *route;
@property(nonatomic, strong) id<RouteViewControllerDelegate> delegate;

@property (weak, nonatomic) IBOutlet UILabel *fromLocationLbl;
@property (weak, nonatomic) IBOutlet UILabel *toLocationLbl;

- (IBAction)saveBtnClick:(id)sender;

@end


@protocol RouteViewControllerDelegate <NSObject>

-(void) routeViewControllerDidFinish:(RouteViewController*) routeViewController FromLocation:(Location*)fromLocation ToLocation:(Location*)toLocation;

@end