//
//  LocationDetailViewController.m
//  Travello
//
//  Created by Balbir Shokeen on 2013/08/31.
//  Copyright (c) 2013 Balbir Shokeen. All rights reserved.
//

#import "LocationViewController.h"
#import "RegionAnnotation.h"
#import "RegionAnnotationView.h"
#import "AppSharedData.h"
#import "Flurry.h"

@interface LocationViewController ()

// Attributes generated or prepared to operate on this interface
@property (nonatomic, retain) RegionAnnotation *currentAnnotation;

// DidUpdateLocation keep updating users current location and we want to get it once time only when the UI Starts
@property (nonatomic) bool hasRecievedUserCurrentLocationOnce;

// Set the location name which is currently selected
@property (nonatomic, strong) NSString *currSelLocIconName;

@end



@implementation LocationViewController


@synthesize thisInterfaceEntityName = _thisInterfaceEntityName;
@synthesize delegate = _delegate, selectedLocation = _selectedLocation, locOperationMode=_locOperationMode;
@synthesize localSearch = _localSearch;
@synthesize currentAnnotation = _currentAnnotation, hasRecievedUserCurrentLocationOnce = _hasRecievedUserCurrentLocationOnce, currSelLocIconName = _currSelLocIconName;


- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    // Inform Flurry
    [Flurry logEvent:@"View Access: Location"];
    
    NSLog(@"LocationDetailViewController: viewDidLoad: Start.");
    
    self.navigationItem.rightBarButtonItem.title = @"Drop Pin";
    self.navigationItem.backBarButtonItem.title = @"Save";
    self.navigationController.navigationItem.title =@"Another";
    [self.navigationItem setRightBarButtonItem:nil animated:NO]; // Hide the right bar button item for now.
    
    self.thisInterfaceEntityName = @"Location";

    self.uitfLocnName.delegate = self;
    self.uitfLocnAddress.delegate = self;
    
    
    // if launched for specific location, loadUI for this location
    if ( self.locOperationMode == LocationADD  ) { // Start Map with current location
        
        self.hasRecievedUserCurrentLocationOnce = NO; // We require to collect user location once in this case
        //        [self.uitfLocnName becomeFirstResponder]; // As User to Enter the Name First
        
        // Move focus to the known location which can further be reset to accurate location in didUpdateLocatin
        CLLocationCoordinate2D regionCoordinate = [[AppSharedData getInstance].appLocationMgr getLocationCoordinate];
        [self moveMapFocusToCoordinate:regionCoordinate];
        
        // Handle hte image icon
//        self.currSelLocIconName = nil; // Image shown is different to let use know, an action needed to select image. if no selection done it would be a blank image
        // Earlier it was comign with help icon and eventually user was not selecting anything leaving it as a blank, so setting it to some image
        self.currSelLocIconName = DEFAULT_LOCN_IMGNAME;
        [self.locationIconBtn setImage:[UIImage imageNamed:self.currSelLocIconName] forState:UIControlStateNormal];
        
    } else if ( self.locOperationMode == LocationEDIT) {// Load from the available selected Location
        
        self.hasRecievedUserCurrentLocationOnce = YES; // we don't require to collect user location in this case.
        [self loadUIFromLocation: self.selectedLocation];
        
    } else if ( self.locOperationMode == LocationREADONLY ) {
        
        [self.view setUserInteractionEnabled:NO];
        
        self.hasRecievedUserCurrentLocationOnce = YES; // we don't require to collect user location in this case.
        [self loadUIFromLocation: self.selectedLocation];
        
    } else {
        NSLog(@"Location: Error unsupported mode");
    }

}

- (void)viewDidUnload {
    
	self.mkMapView = nil;
}
- (void) viewWillAppear:(BOOL)animated {
   // moved code back to load to avoid reseting from location object, specially when the current local variable values is changed by segued views.
}

# pragma mark Handle Finish/Cancel Invokation
-(void)willMoveToParentViewController:(UIViewController *)parent {
    //    NSLog(@"LocationDetailViewController: This VC has has been pushed popped OR covered");
    
    if (!parent) { // Perform operations only when moving back.
            NSLog(@"Location: Moving Back to Parent.");
        [self saveLocation];
    }
}


// Load data from a selected location.
- (void) loadUIFromLocation: (Location *) location {
    // Initialize the textfields
    self.uitfLocnName.text = location.name;
    self.uitfLocnAddress.text = location.address;
    
    self.currSelLocIconName = location.extra2_str; // Set current image variable to avoid reset on clicking back btn
    if ( self.currSelLocIconName == nil || [self.currSelLocIconName isEqualToString:@"Location_Help"]) {
        // Use the defualt image but do not save it in DB. Some values may have it already so doing a string equal check also.
        [self.locationIconBtn setImage:[UIImage imageNamed:@"Location_Help"] forState:UIControlStateNormal];
    } else {
        UIImage *img = [[AppSharedData getInstance] getLocationIconImageByName:self.currSelLocIconName];
        [self.locationIconBtn setImage:img forState:UIControlStateNormal];
    }

    // Initialize the Map Location - Get the monitored region associated to location being loaded, add annotation and move focus to that region.
    CLRegion *region = [[AppSharedData getInstance].appLocationMgr getRegion:self.selectedLocation.region_identifier];
    if (region) {
        self.currentAnnotation = [[RegionAnnotation alloc] initWithCLRegion:region Name:self.selectedLocation.name];
        [self.mkMapView  addAnnotation:self.currentAnnotation];
        
        // Focus map to the coordinate of the associated region
        CLLocationCoordinate2D regionCoordinate = self.currentAnnotation.coordinate;
        [self moveMapFocusToCoordinate:regionCoordinate];
        
        NSLog(@"Location: loadUIFromLocation: MATCHED - RegionID: %@ ", region.identifier);
        
    } else {
        NSLog(@"Location: loadUIFromLocation: NOMATCH - RegionID: %@ %@", region.identifier, self.selectedLocation.region_identifier);

        CLLocationCoordinate2D lasLocCoord = CLLocationCoordinate2DMake([self.selectedLocation.latitude doubleValue], [self.selectedLocation.longitude doubleValue]);
        [self moveMapFocusToCoordinate:lasLocCoord];
        
        /*if (![self.uitfLocnAddress.text isEqualToString:BLANK] )
            [self searchAddress:self.uitfLocnAddress.text AroundLocation:lasLocCoord]; */ // Option to search from address is not so cool option. I should use reverse geocode.

    }
}

/* Shift Focus of the map to the specified coordinate */
-(void) moveMapFocusToCoordinate: (CLLocationCoordinate2D) coordinate {
    MKCoordinateSpan span; span.latitudeDelta = DEFAULT_LATITUDE_DELTA; span.longitudeDelta = DEFAULT_LONGITUDE_DELTA;
    MKCoordinateRegion mkcregion = {coordinate, span};
    
    [self.mkMapView setCenterCoordinate:coordinate];
    [self.mkMapView setRegion:mkcregion animated:YES];
    NSLog(@"Location: MoveMapFocusToCoordinate: Lat:%f Lon:%f", coordinate.latitude, coordinate.longitude);
}


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) mapView:(MKMapView *)mapView didUpdateUserLocation:(MKUserLocation *)userLocation {
    // If it's a relatively recent event, turn off updates to save power
    NSDate* eventDate = userLocation.location.timestamp;
    NSTimeInterval howRecent = [eventDate timeIntervalSinceNow];
    if (abs(howRecent) < 15.0 && !self.hasRecievedUserCurrentLocationOnce) {
        // If the event is recent, do something with it.
        NSLog(@"Location: didUpdateUserLocation: Recent Entry - latitude %+.6f, longitude %+.6f\n",
              userLocation.location.coordinate.latitude,
              userLocation.location.coordinate.longitude);
        
        // Focus map to the latest user location coordinate
        CLLocationCoordinate2D latestCoordinate = userLocation.location.coordinate; // Get the latest coordinate
        [self moveMapFocusToCoordinate:latestCoordinate];
        
        // [self dropPinToCenter]; It is confusing to user, rather let him drop the pin when required
        self.hasRecievedUserCurrentLocationOnce = YES;
    }
}

- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id <MKAnnotation>)annotation {
    NSLog(@"Location: viewForAnnotation: Kind:%@ - Start", NSStringFromClass([annotation class]));
    
	if([annotation isKindOfClass:[RegionAnnotation class]]) {
		RegionAnnotation *thisAnnot = (RegionAnnotation *)annotation;
		NSString *annotationIdentifier = [thisAnnot title];
		RegionAnnotationView *regionView = (RegionAnnotationView *)[self.mkMapView dequeueReusableAnnotationViewWithIdentifier:annotationIdentifier];
		
		if (!regionView) {
			regionView = [[RegionAnnotationView alloc] initWithAnnotation:annotation];
			regionView.map = self.mkMapView;
          	
			// Create a button for the left callout accessory view of each annotation to remove the annotation and region being monitored.
			UIButton *removeRegionButton = [UIButton buttonWithType:UIButtonTypeCustom];
			[removeRegionButton setFrame:CGRectMake(0., 0., 25., 25.)];
			[removeRegionButton setImage:[UIImage imageNamed:@"RemoveRegion"] forState:UIControlStateNormal];
             
			regionView.leftCalloutAccessoryView = removeRegionButton;
		} else {
			regionView.annotation = annotation;
			regionView.theAnnotation = annotation;
		}
		
		// Update or add the overlay displaying the radius of the region around the annotation.
		[regionView updateRadiusOverlay];
		
		return regionView;
	}
	
	return nil;
}

- (MKOverlayView *)mapView:(MKMapView *)mapView viewForOverlay:(id <MKOverlay>)overlay {
    NSLog(@"Location: viewForOverlay: Kind:%@ - Start", NSStringFromClass([overlay class]));
    
	if([overlay isKindOfClass:[MKCircle class]]) {
        
		// Create the view for the radius overlay.
		MKCircleView *circleView = [[MKCircleView alloc] initWithOverlay:overlay];
		circleView.strokeColor = [UIColor purpleColor];
        circleView.lineWidth = 1; // reducing the width
		circleView.fillColor = [[UIColor purpleColor] colorWithAlphaComponent:0.2];
		
		return circleView;
	}
	return nil;
}

- (void)mapView:(MKMapView *)mapView annotationView:(MKAnnotationView *)annotationView didChangeDragState:(MKAnnotationViewDragState)newState fromOldState:(MKAnnotationViewDragState)oldState {
    
    NSLog(@"Location: annotationViewDidChangeDragState - START.");
    
	if([annotationView isKindOfClass:[RegionAnnotationView class]]) {
		RegionAnnotationView *regionView = (RegionAnnotationView *)annotationView;
		RegionAnnotation *regionAnnotation = (RegionAnnotation *)regionView.annotation;
		
		// If the annotation view is starting to be dragged, remove the overlay and stop monitoring the region.
		if (newState == MKAnnotationViewDragStateStarting) {
			[regionView removeRadiusOverlay];
			
			[[AppSharedData getInstance].appLocationMgr stopMonitoringRegion:regionAnnotation.region]; // Stop monitoring right away else identifier would be new and we cannot trace back this region. We need to start as well the new region else if app is stopped this region would not be in monitoring state.
            
            self.currentAnnotation = nil; 
		}
		
		// Once the annotation view has been dragged and placed in a new location, update and add the overlay and begin monitoring the new region.
		if (oldState == MKAnnotationViewDragStateDragging && newState == MKAnnotationViewDragStateEnding) {
			[regionView updateRadiusOverlay];
			
			CLRegion *newRegion = [[CLRegion alloc] initCircularRegionWithCenter:regionAnnotation.coordinate radius:DEFAULT_RADIUS identifier:[NSString stringWithFormat:@"%f, %f", regionAnnotation.coordinate.latitude, regionAnnotation.coordinate.longitude]];
			regionAnnotation.region = newRegion;
			
            // Generate the new address for the new location
            [self reverseGeoCoordinate:regionAnnotation];
			[[AppSharedData getInstance].appLocationMgr startMonitoringRegion:regionAnnotation.region]; // Starting here as old location is moved and there is no active monitoring
            self.currentAnnotation = regionAnnotation; //without setting this updated region we were getting duplicate region stored as at saving time also we were doing this
            
		}
	}
}

- (void)mapView:(MKMapView *)mapView annotationView:(MKAnnotationView *)view calloutAccessoryControlTapped:(UIControl *)control {
    NSLog(@"Location: calloutAccessoryControlTapped - START");
    
    RegionAnnotationView *regionView = (RegionAnnotationView *)view;
	RegionAnnotation *regionAnnotation = (RegionAnnotation *)regionView.annotation;
	
    [self removeRegionMonitoring:regionAnnotation];
	
}

-(void)mapView:(MKMapView *)mapView didSelectAnnotationView:(MKAnnotationView *)view {
//    ULog(@"Test");
}



// TOODO: Make use of this API all places where we need to remove a region monitoring. Ex: in move or delete accessory tap
- (void) removeRegionMonitoring: (RegionAnnotation *) regionAnnotation {
    
    // Stop monitoring the region, remove the radius overlay, and finally remove the annotation from the map.
	[[AppSharedData getInstance].appLocationMgr stopMonitoringRegion:regionAnnotation.region];
    
    for (id overlay in [self.mkMapView overlays]) {
		if ([overlay isKindOfClass:[MKCircle class]]) {
			MKCircle *circleOverlay = (MKCircle *)overlay;
			CLLocationCoordinate2D coord = circleOverlay.coordinate;
			
			if (coord.latitude == regionAnnotation.coordinate.latitude && coord.longitude == regionAnnotation.coordinate.longitude) {
				[self.mkMapView removeOverlay:overlay];
			}
		}
	}
    
    
	[self.mkMapView removeAnnotation:regionAnnotation];
    
    self.currentAnnotation = nil; // remove the annotation stored locally, which helps identify if annotation is already there or not
}

-(bool) validateBeforeSaving {
    bool validForSaving = YES;
    
    // Generate missing data
    if (self.currentAnnotation == nil || [self.uitfLocnName.text isEqualToString:@"" ] || [self.uitfLocnAddress.text isEqualToString:@""] ) {
        validForSaving = NO; // There has to be an annotation created or associated
    }
    // When services are disbaled try showing an alert to user that data is not saved
    /* It is still troubling by coming in wrong scenario, so disabling
    if ( !validForSaving && ![self.uitfLocnName.text isEqualToString:@"" ]) {
        ULog(@"Location Not Saved - Required Data OR Coordinates Not Available.");
    }*/
    
    // Check location proximilty during validation else we were able to edit and save in close proximity
    // Additionally checking regions in cache to allow user to create monitoring for regions in DB but not actually monitored
    NSString *identifier = self.currentAnnotation.region.identifier;
    CLLocationCoordinate2D coord = self.currentAnnotation.coordinate;
    bool locationExistOrTooClose = YES;
    if (self.locOperationMode == LocationADD ) {
     locationExistOrTooClose = [CoreDataOperations ifLocnExistsOrTooCloseForIdentifier:identifier OrCoordinate:coord ExcludeSelf:nil];
    }
    else if (self.locOperationMode == LocationEDIT ) { // Exclude Self as it is existing already and we edit it
     locationExistOrTooClose = [CoreDataOperations ifLocnExistsOrTooCloseForIdentifier:identifier OrCoordinate:coord ExcludeSelf:self.selectedLocation];
    }
//    bool isMonitoredAlso = [[AppSharedData getInstance].appLocationMgr regionIsMonitored:identifier]; // Checking monitoring status in the CoreData main api itself, so removed from here. Otherwise it was not checking if the already existing region is monitored or not.
//    if ( locationExistOrTooClose && isMonitoredAlso ) {
        if ( locationExistOrTooClose ) {
        self.uitfLocnAddress.placeholder = @"This location area is already being monitored.";
        ULog(@"This location area is already being monitored or too close to another location");
        
       validForSaving = NO;
    }
    
    return validForSaving;
}

#pragma mark - Save Location
- (void) saveLocation {
    
    if ( self.locOperationMode == LocationREADONLY) {
        
        // do nothing
        return;
    }
    //        [[self delegate] locationViewControllerDidCancel:self]; // We no longer doing cancelation on this UI
    
    // Perform Validations and accordingly save/add new object
    bool validForSaving = [self validateBeforeSaving];
    
    if (validForSaving) {
        Location *locationObj = nil;
        if ( self.locOperationMode == LocationADD ) { // Add a new Location Object
            // Perform Duplicate Check before storing
            
            
            if ([CoreDataOperations ifLocnExistsOrTooCloseForIdentifier:self.currentAnnotation.region.identifier OrCoordinate:self.currentAnnotation.coordinate ExcludeSelf:nil]) {
                self.uitfLocnAddress.placeholder = @"This location area is already being monitored.";
                ULog(@"This location area is already being monitored or too close to another location");
                
                return; // No Save and do nothing
            }
            
            else if ( [CoreDataOperations countLocations] < 20 ) { // only 20 regions are allowed
                locationObj = [CoreDataOperations addLocationWithName:self.uitfLocnName.text
                                                         WithLatitude:self.currentAnnotation.coordinate.latitude
                                                        WithLongitude:self.currentAnnotation.coordinate.longitude
                                                          WithAddress:self.uitfLocnAddress.text
                                                           WithRadius:DEFAULT_RADIUS
                                                             WithFlag:0
                                                        WithSortOrder:-1 // compute
                                                           WithStatus:0
                                                            WithDirty:0
                                                 WithRegionIdentifier:self.currentAnnotation.region.identifier
                                                       WithExtra1_Int:0
                                                       WithExtra2_Str:self.currSelLocIconName];
                
                // Inform Flurry
                //NSString *eventDetail = [@"Location Created: %@"  stringByAppendingString: locationObj.name];
                [Flurry logEvent:@"Location: Created"];
            }
            else { // > 20 locations not allowed
                NSLog(@"Location: Is not created as either it already exists or no more allowed");
//                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Note" message:@"Maximum allowed locations already created!\n You can delete and create a new one." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
//                [alert show];
                
//                return; // No Save and do nothing
            }
        } else if ( self.locOperationMode == LocationEDIT ){ // Update the existing object
            
            self.selectedLocation.name = self.uitfLocnName.text;
            self.selectedLocation.address = self.uitfLocnAddress.text;
            self.selectedLocation.latitude = [[NSDecimalNumber alloc] initWithDouble:self.currentAnnotation.coordinate.latitude];
            self.selectedLocation.longitude =  [[NSDecimalNumber alloc] initWithDouble:self.currentAnnotation.coordinate.longitude];
            self.selectedLocation.region_identifier = self.currentAnnotation.region.identifier;
            self.selectedLocation.extra2_str = self.currSelLocIconName;
        }
        else {
            
        }
        
        [CoreDataOperations saveContext];
        
        // Once Location is created add the region for monitoring here onwards
        CLRegion *region = self.currentAnnotation.region;
        [[AppSharedData getInstance].appLocationMgr startMonitoringRegion:region];
        
        // Inform the Parent over delegate to finish on their side
        [self.delegate locationViewControllerDidFinish:self OnLocation:locationObj OperationType:LocationADD];
    }
    else {
        // If not valid then call the cancel api to at least refresh the table. Case handling now is if you delete an region for existing location its tableview is not updating the monitoring icon.
        [self.delegate locationViewControllerDidCancel:self]; // Ideally validateSave should be fixed to handle this case. During save remove the region details on location, as of now we don't delete it from location.
    }
    // Final Clean Up - clear the current selectedlocation to avoid reuse
    self.selectedLocation = nil;
    
    NSLog(@"Location: Save Completed!");
}

// Added ability to save the item
- (IBAction)saveBtnClick:(id)sender {
    
    [self saveLocation];
    
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - TextField Return Handling
- (BOOL) textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder]; // Remove the keyboard on hit enter, you will have to uncheck autoenable return key
    
    if (textField == self.uitfLocnAddress && ![textField.text isEqualToString:APP_BLANK]) { // Check blank before performing search.
        
        [self searchAddress:textField.text AroundLocation:self.mkMapView.centerCoordinate];
    }
    else if ( textField == self.uitfLocnName && [self.uitfLocnAddress.text length] <= 0) {
        [self.uitfLocnAddress becomeFirstResponder]; // Move to next field as we using the return key as next
        // No need to move to next control normally rather move only if required
    }
    return YES;
}


#pragma mark - Search Operation

// Remove current annotation, region monitoring, region overlay
- (void) clearAnnotation_RegionMonitoring_Overlay: (RegionAnnotation *) regionAnnotation  {
    if (regionAnnotation) {
        [self.mkMapView removeAnnotation:regionAnnotation];
        [self removeRegionMonitoring:regionAnnotation];
    }
}

- (void) searchAddress: (NSString *) searchAddress AroundLocation:(CLLocationCoordinate2D) coordinate {
    
    // Before starting search remove current annotation, region monitoring, region overlay
    [self clearAnnotation_RegionMonitoring_Overlay: self.currentAnnotation];
    
    bool shownAlert = [[AppSharedData getInstance].appLocationMgr showAlertToUserOnServiceStatus];
    // we start the search if location services are not disabled
    if (shownAlert) {
        [self.navigationController popViewControllerAnimated:YES];
    }
    else {
        [self startSearch:searchAddress AroundLocation:coordinate];
    }
    
    NSLog(@"Location: Search Completed!");
}

-(void) startSearch: (NSString *)queryString AroundLocation:(CLLocationCoordinate2D) coordinate {
    
    // confine the map search area to the user's current location
    MKCoordinateRegion newRegion;
    newRegion.center.latitude = coordinate.latitude;
    newRegion.center.longitude = coordinate.longitude;
    
    // setup the area spanned by the map region:
    // we use the delta values to indicate the desired zoom level of the map,
    //      (smaller delta values corresponding to a higher zoom level)
    newRegion.span.latitudeDelta  = DEFAULT_LATITUDE_DELTA; //0.112872;
    newRegion.span.longitudeDelta = DEFAULT_LONGITUDE_DELTA; //0.109863;
    
    MKLocalSearchRequest *request = [[MKLocalSearchRequest alloc] init];
    
    request.naturalLanguageQuery = queryString;
    request.region = newRegion;
    
    MKLocalSearchCompletionHandler completionHandler = ^(MKLocalSearchResponse *response, NSError *error) {
        if (error != nil)  {
            NSString *errorStr = [[error userInfo] valueForKey:NSLocalizedDescriptionKey];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Could not find places"
                                                            message:errorStr
                                                           delegate:nil
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
            [alert show];
        }
        else {
            NSArray *items = [response mapItems];
            
             if (items == nil || [items count] != 1) {
                self.uitfLocnAddress.placeholder = @"NOT FOUND";
                self.uitfLocnAddress.text = @"";
            }
            else {
                self.uitfLocnAddress.placeholder = @"FOUND";
                
                if ([items count] == 1) {
                     MKMapItem *mapItem = [items objectAtIndex:0];
                    
                    // Reload the UI controls with updated address and name
                    [self refreshUIFieldsWithPlacemark:mapItem.placemark];

                    [self addRegion:mapItem]; // Add the region for monitoring
                }
                else {
                    self.title = @"All Places";
                    
                    NSLog(@"Array Items: %lu", (unsigned long)[items count]);
                    // add all the found annotations to the map
                    for (MKMapItem *item in items)  {
                        
                        RegionAnnotation *annotation = [[RegionAnnotation alloc] init];
                        annotation.coordinate = item.placemark.location.coordinate;
                        annotation.title = item.name;
                        annotation.url = item.url;
                        [self.mkMapView addAnnotation:annotation];
                    }
                    
                    // Focus map to the coordinate of the associated region
                    CLLocationCoordinate2D regionCoordinate = self.mkMapView.centerCoordinate;
                    [self moveMapFocusToCoordinate:regionCoordinate];
                }
            }
            //            [self.tableView reloadData];
        }
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        [self.loadAddressActivityView stopAnimating];
        [self.view setUserInteractionEnabled:YES];
    };
    
    self.localSearch = [[MKLocalSearch alloc] initWithRequest:request];
    
    [self.localSearch startWithCompletionHandler:completionHandler];
    [self.view setUserInteractionEnabled:NO]; 
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    [self.loadAddressActivityView startAnimating];
}

-(CLLocationCoordinate2D) getMapCenterCoordinate {
    // Create a new region based on the center of the map view.
    CLLocationCoordinate2D coord = CLLocationCoordinate2DMake(self.mkMapView.centerCoordinate.latitude, self.mkMapView.centerCoordinate.longitude);
    return coord;
}

-(CLRegion *) createRegionFromCoordinate: (CLLocationCoordinate2D) coord {
    
    CLRegion *newRegion = [[CLRegion alloc] initCircularRegionWithCenter:coord
                                                                  radius:DEFAULT_RADIUS
                                                              identifier:[NSString stringWithFormat:@"%f, %f", coord.latitude, coord.longitude]];
    return newRegion;
}

-(RegionAnnotation *) createAnnotationForRegion:(CLRegion *) region {
    // Create an annotation to show where the region is located on the map.
    Location *loc = [CoreDataOperations getLocationByIdentifier:region.identifier];
    RegionAnnotation *myRegionAnnotation = [[RegionAnnotation alloc] initWithCLRegion:region Name:loc.name];
    NSString *txt = self.uitfLocnName.text;
    if ( txt == nil || [txt isEqualToString:APP_BLANK] ) {
        myRegionAnnotation.title = APP_EMPTY; //It is not set yet
    } else {
        myRegionAnnotation.title = txt;
    }

    return myRegionAnnotation;
}



-(RegionAnnotation *) addAnnotationToMap: (CLLocationCoordinate2D) coordinate {
    
    CLRegion *region = [self createRegionFromCoordinate:coordinate];
    RegionAnnotation *regionAnnotation = [self createAnnotationForRegion:region];
    
    [self.mkMapView addAnnotation:regionAnnotation];
    
    // Focus map to the coordinate of the associated region
    CLLocationCoordinate2D regionCoordinate = regionAnnotation.coordinate;
    [self moveMapFocusToCoordinate:regionCoordinate];
    
    self.currentAnnotation = regionAnnotation;
    
    return regionAnnotation;
}



-(void) updateAddress: (NSString *) address {
    if ([self.uitfLocnAddress.text isEqualToString:@""]) {
        self.uitfLocnAddress.text = address;
    }
}

- (void) refreshUIFieldsWithPlacemark:(CLPlacemark *) placemark {
    NSString *subThoroughfare = placemark.subThoroughfare;
    NSString *thoroughfare    = placemark.thoroughfare;
    NSString *postalCode      = placemark.postalCode;
    NSString *locality        = placemark.locality;
    NSString *adminArea       = placemark.administrativeArea;
    NSString *country         = placemark.country;
    
    subThoroughfare = (subThoroughfare == nil ) ? APP_BLANK : [subThoroughfare stringByAppendingString:@", "];
    thoroughfare    = (thoroughfare == nil )    ? APP_BLANK : [thoroughfare stringByAppendingString:@", "];
    postalCode      = (postalCode == nil )      ? APP_BLANK : [postalCode stringByAppendingString:@" "];
    locality        = (locality == nil )        ? APP_BLANK : [locality stringByAppendingString:@", "];
    adminArea       = (adminArea == nil )       ? APP_BLANK : [adminArea stringByAppendingString:@", "];
    country         = (country == nil )         ? APP_BLANK : [country stringByAppendingString:@", "];
    

    NSString *get = [NSString stringWithFormat:@"%@%@%@%@%@",
                     subThoroughfare, thoroughfare, locality, adminArea, postalCode];
    
    self.uitfLocnAddress.text = [get stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    if ( self.locOperationMode == LocationADD) { // Change the name automatically only when we are in add mode in edit name is already there
        if ( [locality isEqualToString:APP_BLANK ]) {
            self.uitfLocnName.text = [adminArea stringByReplacingOccurrencesOfString:@"," withString:@""];
        }
        else {
            self.uitfLocnName.text = [locality stringByReplacingOccurrencesOfString:@"," withString:@""];
        }
    }
    NSLog(@"Location: refreshUIWithPlacemark: ST:%@ TF:%@ PC:%@ L:%@ AA:%@ C:%@", subThoroughfare, thoroughfare, postalCode, locality, adminArea, country);
}

-(void) reverseGeoCoordinate: (RegionAnnotation *) regionAnnotation {
    NSLog(@"Location: reverseGeoCoordinate: START RegionIdentifier: %@", regionAnnotation.region.identifier);
    
        CLGeocoder *geocoder = [[CLGeocoder alloc] init];
    
        CLLocationCoordinate2D coord = regionAnnotation.coordinate;
        CLLocation *location = [[CLLocation alloc] initWithLatitude:coord.latitude longitude:coord.longitude];
        
        [geocoder reverseGeocodeLocation:location completionHandler:^(NSArray *placemarks, NSError *error) {
            if (error == nil && [placemarks count] > 0) {
                CLPlacemark *placemark = [placemarks lastObject];
                
                [self refreshUIFieldsWithPlacemark:placemark];
                
                NSLog(@"Location: reverseGeoCoordinate: Callback - Found Address");
            } else {
                self.uitfLocnAddress.text = @"No Address Available";
                NSLog(@"Location: reverseGeoCoordinate: Callback - No Address Available");
            }
        } ];
}
// Drop Pin and start monitoring at the map center
- (IBAction)dropPinToCenter {
     NSLog(@"Location: DropPinToCenter: START");
    
    // Inform user about location services, if required
    bool shownAlert = [[AppSharedData getInstance].appLocationMgr showAlertToUserOnServiceStatus];

    if (shownAlert) {
        [self.navigationController popViewControllerAnimated:YES];
        NSLog(@"Location: DropPinToCenter: Finished - Region monitoring is not available.");
    }
    else {     // Add Region if region monitoring is available
        // Before starting search remove current annotation, region monitoring, region overlay
        [self clearAnnotation_RegionMonitoring_Overlay: self.currentAnnotation];
        
        CLLocationCoordinate2D coordinate = [self getMapCenterCoordinate];
        
        RegionAnnotation *addedAnnotation = [self addAnnotationToMap:coordinate];
        
        [self reverseGeoCoordinate:addedAnnotation];
        NSLog(@"Location: DropPinToCenter: Finished - Added new region for monitoring identifier:%@", addedAnnotation.region.identifier);
    }
}

/*
 This method creates a new region based on the center coordinate of the map view.
 A new annotation is created to represent the region and then the application starts monitoring the new region.
 */
- (IBAction)addRegion: (MKMapItem *) mapItem {
	if ([CLLocationManager regionMonitoringAvailable]) {
		// Create a new region based on the center of the map view.
		CLLocationCoordinate2D coord = CLLocationCoordinate2DMake(mapItem.placemark.coordinate.latitude, mapItem.placemark.coordinate.longitude);
		CLRegion *newRegion = [[CLRegion alloc] initCircularRegionWithCenter:coord
																	  radius:DEFAULT_RADIUS
																  identifier:[NSString stringWithFormat:@"%f, %f", mapItem.placemark.coordinate.latitude, mapItem.placemark.coordinate.longitude]];
		
		// Create an annotation to show where the region is located on the map.
		RegionAnnotation *myRegionAnnotation = [[RegionAnnotation alloc] initWithCLRegion:newRegion Name:self.uitfLocnName.text];
		
		[self.mkMapView addAnnotation:myRegionAnnotation];
		
        self.currentAnnotation = myRegionAnnotation;

        // Focus map to the coordinate of the associated region
        [self moveMapFocusToCoordinate:coord];

    }
	else {
		NSLog(@"Location: Region monitoring is not available.");
	}
    
    NSLog(@"Location: Added new region for monitoring");
}

#pragma mark - Segue management
- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"ShowLocationIconViewController"]) {
        
        LocationIconViewController *controller = (LocationIconViewController *)[segue destinationViewController];
        
        // to get the events, had to set the delegate as self. Set the location we want to view
        controller.delegate = self;
        controller.selectedLocIconName = self.currSelLocIconName;
    }
}


#pragma mark - LocationIconViewController
-(void) locationIconViewControllerDidCancel:(LocationIconViewController *)controler {
    
}

-(void) locationIconViewControllerDidFinish:(LocationIconViewController *)controller SelectedKey:(NSString *)selKey {
    
    UIImage *img = [[AppSharedData getInstance] getLocationIconImageByName:selKey];
    
    [self.locationIconBtn setImage:img forState:UIControlStateNormal];
    [self.locationIconBtn setImage:img forState:UIControlStateSelected];
    
    self.currSelLocIconName = selKey;
}

@end
