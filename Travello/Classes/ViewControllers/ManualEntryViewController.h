//
//  EditLocationVC.h
//  Travello
//
//  Created by Vivek Sehrawat on 11/4/13.
//  Copyright (c) 2013 Dasheri. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Location.h"
#import "LocationLog.h"

@interface ManualEntryViewController : UITableViewController

@property(nonatomic,strong) Location *selectedLocation;
@property (nonatomic, strong) LocationLog *selectedLocationLog;

@end
