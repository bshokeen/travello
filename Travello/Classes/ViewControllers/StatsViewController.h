//
//  StatsViewController.h
//  Travello
//
//  Created by Balbir Shokeen on 2013/11/26.
//  Copyright (c) 2013 Dasheri. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppUtilities.h"

@interface StatsViewController : UITableViewController

//Below PNChart Support
@property (weak, nonatomic) IBOutlet UIScrollView *chartScrollView;
@property (weak, nonatomic) IBOutlet UISegmentedControl *switchGraphSegment;



@end
