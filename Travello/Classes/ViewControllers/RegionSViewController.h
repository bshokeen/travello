//
//  AllRegionsViewController.h
//  Travello
//
//  Created by Balbir Shokeen on 2013/09/06.
//  Copyright (c) 2013 Balbir Shokeen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import <MapKit/MapKit.h>
#import "AppSharedData.h"
#import "RegionAnnotation.h"

@interface RegionSViewController : UITableViewController
@property (weak, nonatomic) IBOutlet MKMapView *mkMapView;
- (IBAction)deleteAllBtnClick:(id)sender;

@end
