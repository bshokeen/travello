//
//  EditLocationVC.m
//  Travello
//
//  Created by Vivek Sehrawat on 11/4/13.
//  Copyright (c) 2013 Dasheri. All rights reserved.
//

#import "ManualEntryViewController.h"
#import "CoreDataOperations.h"
#import "AppSharedData.h"

#define kPickerAnimationDuration    0.40   // duration for the animation to slide the date picker into view
#define kDatePickerTag              99     // view tag identifiying the date picker view

#define kTitleKey       @"title"   // key for obtaining the data source item's title
#define kDateKey        @"date"    // key for obtaining the data source item's date value

// keep track of which rows have date cells
#define kDateStartRow   1
#define kDateEndRow     2

#define DEFAULT_DATEDIFF_MINS 10

static NSString *kDateCellID = @"dateCell";     // the cells with the start or end date
static NSString *kDatePickerID = @"datePicker"; // the cell containing the date picker
static NSString *kOtherCell = @"otherCell";     // the remaining cells at the end

#pragma mark -

@interface ManualEntryViewController ()

@property (nonatomic, strong) NSArray *dataArray;
@property (nonatomic, strong) NSDateFormatter *dateFormatter;

// keep track which indexPath points to the cell with UIDatePicker
@property (nonatomic, strong) NSIndexPath *datePickerIndexPath;

@property (assign) NSInteger pickerCellRowHeight;

@property (nonatomic, strong) IBOutlet UIDatePicker *pickerView;

// this button appears only when the date picker is shown (iOS 6.1.x or earlier)
@property (nonatomic, strong) IBOutlet UIBarButtonItem *doneButton;

@property (nonatomic, retain) NSManagedObjectContext *managedObjectContext1;


@end

@implementation ManualEntryViewController
@synthesize selectedLocation;

/*! Primary view has been loaded for this view controller
 */
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Inform Flurry
    [Flurry logEvent:@"Tab Access: ManualEntry"];
    
    // HANDLING TRAVELOG ENTRY EDIT
    /* In this UI we using self.selectedLocationLog == nil => running in Add new entry mode
     * if not != nil than edit a Travelog entry more */
    if (self.selectedLocationLog != nil) { //UI is called to edit a locationLog instead of adding a new entry
        NSDate *didEnterDt     = [NSDate date];
        NSDate *didExitDt = nil;//[didEnterDt dateByAddingTimeInterval:60 * DEFAULT_DATEDIFF_MINS]; // Add 10 mins to current date
        
        if ( self.selectedLocationLog.did_enter != nil & self.selectedLocationLog.did_exit != nil) {
            didEnterDt = self.selectedLocationLog.did_enter;
            didExitDt  = self.selectedLocationLog.did_exit;
        }
        else if ( self.selectedLocationLog.did_enter == nil) { // Use enter date as 10 minute before the exit mode
            didExitDt = self.selectedLocationLog.did_exit;
            didEnterDt = [didExitDt dateByAddingTimeInterval: - 60 * DEFAULT_DATEDIFF_MINS];
        }
        else { // Add 10 minutes to exit date
            didEnterDt = self.selectedLocationLog.did_enter;
            didExitDt  = [didEnterDt dateByAddingTimeInterval: 60 * DEFAULT_DATEDIFF_MINS];
            
            //if the exitDt is more than current time use the current time as travel cannot be in future
            if([didExitDt compare:[NSDate date]] == NSOrderedDescending)
                didExitDt = [NSDate date];
        }
        
        NSString *locatioName = self.selectedLocationLog.location.name;
        NSString *strLocation=[NSString stringWithFormat:@"%@", locatioName];
        NSMutableDictionary *itemOne = [@{ kTitleKey : strLocation, } mutableCopy];
        NSMutableDictionary *itemTwo = [@{ kTitleKey : @"Reached:", kDateKey : didEnterDt } mutableCopy];
        NSMutableDictionary *itemThree = [@{ kTitleKey : @"Left:", kDateKey:didExitDt } mutableCopy];
        NSMutableDictionary *itemFour = [@{ kTitleKey : @"" } mutableCopy];
        
        self.dataArray = @[itemOne, itemTwo, itemThree, itemFour];
    }
    else {

        NSDate *leftAt     = [NSDate date];
        NSDate *reachedAt = [leftAt dateByAddingTimeInterval:-60 * DEFAULT_DATEDIFF_MINS]; // Reach 10 mins before current date

        NSString *locatioName = selectedLocation.name;
        NSString *strLocation=[NSString stringWithFormat:@"%@", locatioName];
        NSMutableDictionary *itemOne = [@{ kTitleKey : strLocation, } mutableCopy];
        NSMutableDictionary *itemTwo = [@{ kTitleKey : @"Reached:", kDateKey : reachedAt } mutableCopy];
        NSMutableDictionary *itemThree = [@{ kTitleKey : @"Left:", kDateKey:leftAt } mutableCopy];
        NSMutableDictionary *itemFour = [@{ kTitleKey : @"" } mutableCopy];
        
        self.dataArray = @[itemOne, itemTwo, itemThree, itemFour];
    }
    
    self.dateFormatter = [[NSDateFormatter alloc] init];
    self.dateFormatter.dateFormat = @"dd-MMM-yyyy hh:mm a";
    
    // obtain the picker view cell's height, works because the cell was pre-defined in our storyboard
    UITableViewCell *pickerViewCellToCheck = [self.tableView dequeueReusableCellWithIdentifier:kDatePickerID];
    self.pickerCellRowHeight = pickerViewCellToCheck.frame.size.height;
    
    // if the local changes while in the background, we need to be notified so we can update the date
    // format in the table view cells
    //
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(localeChanged:)
                                                 name:NSCurrentLocaleDidChangeNotification
                                               object:nil];
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:NSCurrentLocaleDidChangeNotification
                                                  object:nil];
}


#pragma mark - Locale

/*! Responds to region format or locale changes.
 */
- (void)localeChanged:(NSNotification *)notif
{
    // the user changed the locale (region format) in Settings, so we are notified here to
    // update the date format in the table view cells
    //
    [self.tableView reloadData];
}


#pragma mark - Utilities

/*! Returns the major version of iOS, (i.e. for iOS 6.1.3 it returns 6)
 */
NSUInteger DeviceSystemMajorVersion()
{
    static NSUInteger _deviceSystemMajorVersion = -1;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        
        _deviceSystemMajorVersion = [[[[[UIDevice currentDevice] systemVersion] componentsSeparatedByString:@"."] objectAtIndex:0] intValue];
    });
    
    return _deviceSystemMajorVersion;
}

#define EMBEDDED_DATE_PICKER (DeviceSystemMajorVersion() >= 7)

/*! Determines if the given indexPath has a cell below it with a UIDatePicker.
 
 @param indexPath The indexPath to check if its cell has a UIDatePicker below it.
 */
- (BOOL)hasPickerForIndexPath:(NSIndexPath *)indexPath
{
    BOOL hasDatePicker = NO;
    
    NSInteger targetedRow = indexPath.row + 1;
    NSIndexPath *idxPath  = [NSIndexPath indexPathForRow:targetedRow inSection:0];
    
    UITableViewCell *checkDatePickerCell = [self.tableView cellForRowAtIndexPath:idxPath];
    UIDatePicker    *checkDatePicker     = (UIDatePicker *)[checkDatePickerCell viewWithTag:kDatePickerTag];
    
    hasDatePicker = (checkDatePicker != nil);
    return hasDatePicker;
}

/*! Updates the UIDatePicker's value to match with the date of the cell above it.
 */
- (void)updateDatePicker
{
    if (self.datePickerIndexPath != nil)
    {
        UITableViewCell *associatedDatePickerCell = [self.tableView cellForRowAtIndexPath:self.datePickerIndexPath];
        
        UIDatePicker *targetedDatePicker = (UIDatePicker *)[associatedDatePickerCell viewWithTag:kDatePickerTag];
        if (targetedDatePicker != nil)
        {
            // we found a UIDatePicker in this cell, so update it's date value
            //
            NSDictionary *itemData = self.dataArray[self.datePickerIndexPath.row - 1];
            NSDate *useDate = [itemData valueForKey:kDateKey] ;
            [targetedDatePicker setDate:useDate animated:NO];
        }
    }
}

/*! Determines if the UITableViewController has a UIDatePicker in any of its cells.
 */
- (BOOL)hasInlineDatePicker
{
    return (self.datePickerIndexPath != nil);
}

/*! Determines if the given indexPath points to a cell that contains the UIDatePicker.
 
 @param indexPath The indexPath to check if it represents a cell with the UIDatePicker.
 */
- (BOOL)indexPathHasPicker:(NSIndexPath *)indexPath
{
    return ([self hasInlineDatePicker] && self.datePickerIndexPath.row == indexPath.row);
}

/*! Determines if the given indexPath points to a cell that contains the start/end dates.
 
 @param indexPath The indexPath to check if it represents start/end date cell.
 */
- (BOOL)indexPathHasDate:(NSIndexPath *)indexPath
{
    BOOL hasDate = NO;
    
    if ((indexPath.row == kDateStartRow) ||
        (indexPath.row == kDateEndRow || ([self hasInlineDatePicker] && (indexPath.row == kDateEndRow + 1))))
    {
        hasDate = YES;
    }
    
    return hasDate;
}


#pragma mark - UITableViewDataSource

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return ([self indexPathHasPicker:indexPath] ? self.pickerCellRowHeight : self.tableView.rowHeight);
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if ([self hasInlineDatePicker])
    {
        // we have a date picker, so allow for it in the number of rows in this section
        NSInteger numRows = self.dataArray.count;
        return ++numRows;
    }
    return self.dataArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = nil;
    
    NSString *cellID = kOtherCell;
    
    if ([self indexPathHasPicker:indexPath])
    {
        // the indexPath is the one containing the inline date picker
        cellID = kDatePickerID;     // the current/opened date picker cell
    }
    else if ([self indexPathHasDate:indexPath])
    {
        // the indexPath is one that contains the date information
        cellID = kDateCellID;       // the start/end date cells
    }
    
    cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    
    if (indexPath.row == 0)
    {
        // we decide here that first cell in the table is not selectable (it's just an indicator)
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    
    // if we have a date picker open whose cell is above the cell we want to update,
    // then we have one more cell than the model allows
    //
    NSInteger modelRow = indexPath.row;
    if (self.datePickerIndexPath != nil && self.datePickerIndexPath.row < indexPath.row)
    {
        modelRow--;
    }
    
    NSDictionary *itemData = self.dataArray[modelRow];
    
    // proceed to configure our cell
    if ([cellID isEqualToString:kDateCellID])
    {
        // we have either start or end date cells, populate their date field
        cell.textLabel.text = [itemData valueForKey:kTitleKey];
        cell.detailTextLabel.text = [self.dateFormatter stringFromDate:[itemData valueForKey:kDateKey]];
//        }
    }
    else if ([cellID isEqualToString:kOtherCell])
    {

        // this cell is a non-date cell, just assign it's text label
        cell.textLabel.text       = [itemData valueForKey:kTitleKey];
        cell.textLabel.textColor = [UIColor brownColor];
        cell.textLabel.textAlignment = NSTextAlignmentCenter;
        // Using Title Detail mode was not working as the extra blank was overwriting teh value.
//        cell.detailTextLabel.text = [itemData valueForKey:kDateKey];
//                NSLog(@"rest : %@ %@", [itemData valueForKey:kTitleKey], [itemData valueForKey:kDateKey] );
    }
	return cell;
}

/*! Adds or removes a UIDatePicker cell below the given indexPath.
 
 @param indexPath The indexPath to reveal the UIDatePicker.
 */
- (void)toggleDatePickerForSelectedIndexPath:(NSIndexPath *)indexPath
{
    [self.tableView beginUpdates];
    
    NSArray *indexPaths = @[[NSIndexPath indexPathForRow:indexPath.row + 1 inSection:0]];
    
    // check if 'indexPath' has an attached date picker below it
    if ([self hasPickerForIndexPath:indexPath])
    {
        // found a picker below it, so remove it
        [self.tableView deleteRowsAtIndexPaths:indexPaths
                              withRowAnimation:UITableViewRowAnimationFade];
    }
    else
    {
        // didn't find a picker below it, so we should insert it
        [self.tableView insertRowsAtIndexPaths:indexPaths
                              withRowAnimation:UITableViewRowAnimationFade];
    }
    
    [self.tableView endUpdates];
}

/*! Reveals the date picker inline for the given indexPath, called by "didSelectRowAtIndexPath".
 
 @param indexPath The indexPath to reveal the UIDatePicker.
 */
- (void)displayInlineDatePickerForRowAtIndexPath:(NSIndexPath *)indexPath
{
    // display the date picker inline with the table content
    [self.tableView beginUpdates];
    
    BOOL before = NO;   // indicates if the date picker is below "indexPath", help us determine which row to reveal
    if ([self hasInlineDatePicker])
    {
        before = self.datePickerIndexPath.row < indexPath.row;
    }
    
    BOOL sameCellClicked = (self.datePickerIndexPath.row - 1 == indexPath.row);
    
    // remove any date picker cell if it exists
    if ([self hasInlineDatePicker])
    {
        [self.tableView deleteRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:self.datePickerIndexPath.row inSection:0]]
                              withRowAnimation:UITableViewRowAnimationFade];
        self.datePickerIndexPath = nil;
    }
    
    if (!sameCellClicked)
    {
        // hide the old date picker and display the new one
        NSInteger rowToReveal = (before ? indexPath.row - 1 : indexPath.row);
        NSIndexPath *indexPathToReveal = [NSIndexPath indexPathForRow:rowToReveal inSection:0];
        
        [self toggleDatePickerForSelectedIndexPath:indexPathToReveal];
        self.datePickerIndexPath = [NSIndexPath indexPathForRow:indexPathToReveal.row + 1 inSection:0];
    }
    
    // always deselect the row containing the start or end date
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    [self.tableView endUpdates];
    
    // inform our date picker of the current date to match the current cell
    [self updateDatePicker];
}

/*! Reveals the UIDatePicker as an external slide-in view, iOS 6.1.x and earlier, called by "didSelectRowAtIndexPath".
 
 @param indexPath The indexPath used to display the UIDatePicker.
 */
- (void)displayExternalDatePickerForRowAtIndexPath:(NSIndexPath *)indexPath
{
    // first update the date picker's date value according to our model
    NSDictionary *itemData = self.dataArray[indexPath.row];
    [self.pickerView setDate:[itemData valueForKey:kDateKey] animated:YES];
    
    // the date picker might already be showing, so don't add it to our view
    if (self.pickerView.superview == nil)
    {
        CGRect startFrame = self.pickerView.frame;
        CGRect endFrame = self.pickerView.frame;
        
        // the start position is below the bottom of the visible frame
        startFrame.origin.y = self.view.frame.size.height;
        
        // the end position is slid up by the height of the view
        endFrame.origin.y = startFrame.origin.y - endFrame.size.height;
        
        self.pickerView.frame = startFrame;
        
        [self.view addSubview:self.pickerView];
        
        // animate the date picker into view
        [UIView animateWithDuration:kPickerAnimationDuration animations: ^{ self.pickerView.frame = endFrame; }
                         completion:^(BOOL finished) {
                             // add the "Done" button to the nav bar
                             self.navigationItem.rightBarButtonItem = self.doneButton;
                         }];
    }
}


#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    if (cell.reuseIdentifier == kDateCellID)
    {
        if (EMBEDDED_DATE_PICKER)
            [self displayInlineDatePickerForRowAtIndexPath:indexPath];
        else
            [self displayExternalDatePickerForRowAtIndexPath:indexPath];
    }
    else
    {
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
    }
}


#pragma mark - Actions

/*! User chose to change the date by changing the values inside the UIDatePicker.
 
 @param sender The sender for this action: UIDatePicker.
 */
- (IBAction)dateAction:(id)sender
{
    NSIndexPath *targetedCellIndexPath = nil;
    
    if ([self hasInlineDatePicker])
    {
        // inline date picker: update the cell's date "above" the date picker cell
        
        targetedCellIndexPath = [NSIndexPath indexPathForRow:self.datePickerIndexPath.row - 1 inSection:0];

        // HANDLING TRAVELOG ENTRY EDIT
        // keep the EXIT TIME relative only if we adding a new entry
        if (self.selectedLocationLog == nil) {
            // Update the exitTime date only if the date picker operated is of first/reached time
            if ( self.datePickerIndexPath.row == 2) { // HARDCODING
                NSIndexPath *exitDateIdxPath = [NSIndexPath indexPathForRow:(self.datePickerIndexPath.row +1) inSection:0];
                // Update the date of the next cell text label also
                UITableViewCell *exitCell = [self.tableView cellForRowAtIndexPath:exitDateIdxPath];
                UIDatePicker *exitCellDatePikr = sender;
                
                
                // update our data model
                NSMutableDictionary *exitCellDataItem = self.dataArray[exitDateIdxPath.row-1]; // selecting data cell from array where index value is 1 less
                NSDate *newDate = [exitCellDatePikr.date dateByAddingTimeInterval:60*DEFAULT_DATEDIFF_MINS];
                [exitCellDataItem setValue:newDate forKey:kDateKey];
                
                // update the cell's date string
                exitCell.detailTextLabel.text = [self.dateFormatter stringFromDate:newDate];
            }
        }
    }
    else
    {
        // external date picker: update the current "selected" cell's date
        targetedCellIndexPath = [self.tableView indexPathForSelectedRow];
    }
    
    UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:targetedCellIndexPath];
    UIDatePicker *targetedDatePicker = sender;
    
    // update our data model
    NSMutableDictionary *itemData = self.dataArray[targetedCellIndexPath.row];
    [itemData setValue:targetedDatePicker.date forKey:kDateKey];
    
    // update the cell's date string
    cell.detailTextLabel.text = [self.dateFormatter stringFromDate:targetedDatePicker.date];
}


/*! User chose to finish using the UIDatePicker by pressing the "Done" button, (used only for non-inline date picker), iOS 6.1.x or earlier
 
 @param sender The sender for this action: The "Done" UIBarButtonItem
 */
- (IBAction)doneAction:(id)sender
{
    BOOL success=FALSE;

    NSDate *EntryTime=[[self.dataArray objectAtIndex:1]objectForKey:@"date"];
    NSDate *ExitTime=[[self.dataArray objectAtIndex:2]objectForKey:@"date"];
    
    
    NSComparisonResult result = [EntryTime compare:ExitTime];
    
    if(result==NSOrderedAscending)
        success= TRUE;
    else if(result==NSOrderedDescending)
        success=FALSE;
    else {
        NSLog(@"ManualEntryViewController: Both dates are the same");
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"INVALID INPUT" message:@"One cannot reach or leave at the same time."
                                                       delegate:self cancelButtonTitle:NSLocalizedString(@"OK",@"OK") otherButtonTitles: nil];
        [alert show];
        return;
    }
    
    if (!success) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"INVALID INPUT" message:@"One cannot leave before you have reached."
                                                       delegate:self cancelButtonTitle:NSLocalizedString(@"OK",@"OK") otherButtonTitles: nil];
        [alert show];
        return;
    }
    
    // Travel cannot be in future
    NSComparisonResult futureEnter = [EntryTime compare:[NSDate date]];
    NSComparisonResult futureExit  = [ExitTime compare:[NSDate date]];
    if (futureEnter == NSOrderedDescending || futureExit == NSOrderedDescending) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"INVALID INPUT" message:@"Your entry is using future date and travel in future is not supported so far!"
                                                       delegate:self cancelButtonTitle:NSLocalizedString(@"OK",@"OK") otherButtonTitles: nil];
        [alert show];
        
//        success = false; // safety set, not required though
        return;
    }

    
    // Execution should come here only if all validations have passed
    
    // HANDLING TRAVELOG ENTRY EDIT
        if ( self.selectedLocationLog != nil ) { // We editing an existing entry so save it.
            self.selectedLocationLog.did_enter = EntryTime;
            self.selectedLocationLog.did_exit  = ExitTime;
            
            [CoreDataOperations saveContext];
            
            [self.navigationController popViewControllerAnimated:YES];
        }
        else { // We adding a new entry we need to create an entry
            [CoreDataOperations addLocationLogWithDidEnterTime:EntryTime WithDidExitTime:ExitTime WithDidEnterLatitude:0.0 WithDidEnterLongitude:0.0 WithDidExitLatitude:0.0 WithDidExitLongitude:0.0 WithLocnIdentifier:self.selectedLocation.region_identifier WithSortOrder:0 ManualEntry:YES];
            
            NSLog(@"Region: %@", self.selectedLocation.region_identifier);
            NSLog(@"Start time : %@ \n End time : %@",EntryTime,ExitTime);
            
            CGRect pickerFrame = self.pickerView.frame;
            pickerFrame.origin.y = self.view.frame.size.height;
            // animate the date picker out of view
            [UIView animateWithDuration:kPickerAnimationDuration animations: ^{ self.pickerView.frame = pickerFrame; }
                             completion:^(BOOL finished) {
                                 [self.pickerView removeFromSuperview];
                             }];
            
            NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
            [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
            
            /* Feature Add: 
             * Allow closing the open trip on manual entry, if the entry exit time is latest
             * We should do it only if the there isn't any other trip beyond these dates
             */
            
            if ( ! [CoreDataOperations tripExistForDateLaterThan:EntryTime]) {
    
                // First log as if entering a region
                [[AppSharedData getInstance].appLocationMgr processTrip_EnterRegion_EndTrip:self.selectedLocation.region_identifier EnterRegionEndTripTime:EntryTime];
                
                if ( ![CoreDataOperations tripExistForDateLaterThan:ExitTime] ) {
                    // Then log as if exiting the same region
                    [[AppSharedData getInstance].appLocationMgr processTrip_ExitRegion_StartTrip:self.selectedLocation.region_identifier ExitRegionStartTripTime:ExitTime];
                    
                }
            }
            
            //BugFix: After saving manual entry it was goign to trips and not StayAt
            [self.navigationController popToRootViewControllerAnimated:YES];
            /* AS the workflow is changed and moved log UI out of tab, this is not returning back, so disabled, kept for reference
            NSArray *array = [self.navigationController viewControllers];
            [self.navigationController popToViewController:[array objectAtIndex:0] animated:YES];
             */
        }

    // Inform Flurry
    [Flurry logEvent:@"Clicked: ManualEntry - Save"];
}

@end
