//
//  LocationIconCollViewController.h
//  Travello
//
//  Created by Balbir Shokeen on 2013/09/14.
//  Copyright (c) 2013 Dasheri. All rights reserved.
//

#import <UIKit/UIKit.h>


@protocol LocationIconViewControllerDelegate;


@interface LocationIconViewController : UICollectionViewController

{
    NSArray *arrRecords;
    long noOfSection;
}

@property (weak, nonatomic) id <LocationIconViewControllerDelegate> delegate;
@property (nonatomic, strong) NSString *selectedLocIconName;

@end


@protocol LocationIconViewControllerDelegate <NSObject>

- (void) locationIconViewControllerDidCancel: (LocationIconViewController *) controler;
- (void) locationIconViewControllerDidFinish: (LocationIconViewController *) controller SelectedKey:(NSString *) selKey;

@end

