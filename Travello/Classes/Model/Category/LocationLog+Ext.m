//
//  LocationLog+Ext.m
//  Travello
//
//  Created by Balbir Shokeen on 2013/09/06.
//  Copyright (c) 2013 Balbir Shokeen. All rights reserved.
//

#import "LocationLog+Ext.h"

@implementation LocationLog (Ext)

@dynamic primitiveCreatedOn, primitiveEntryDaySectionName;


#pragma mark Transient properties - Created On
- (NSString *)entryDaySectionName {
    
    // Create and cache the section identifier on demand.
    
    [self willAccessValueForKey:@"entryDaySectionName"];
    NSString *tmp = [self primitiveEntryDaySectionName];
    [self didAccessValueForKey:@"entryDaySectionName"];
    
    if (!tmp) {
   
        NSCalendar *calendar = [NSCalendar currentCalendar];
        
        if ([self created_on] == nil)
            return APP_EMPTY;
   
        // We store data as a number yyyy0mmdd so that it can be sorted as desired but while displaying we extract parts of it using mutliplation and division.
        NSDate *createdOn = [self created_on];
        NSDateComponents *components = [calendar components:(NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit) fromDate:createdOn];
        long longDate =  ((([components year] * 1000) + [components month]) * 100) + [components day];
        tmp = [NSString stringWithFormat:@"%ld",longDate] ;
        
        static NSArray *monthSymbols = nil;
        
        if (!monthSymbols) {
            NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
            [formatter setCalendar:[NSCalendar currentCalendar]];
            monthSymbols = [formatter shortMonthSymbols] ;
        }
        
        long numericSection = [tmp integerValue];
        long year = numericSection / 1000 /100;
        long tmpMonth = numericSection - (year * 1000 * 100);
        long month = tmpMonth/100;
        long day = tmpMonth - (month *100);
        
        
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        dateFormatter.dateFormat=@"EEE";
        NSString * dayString = [[dateFormatter stringFromDate:createdOn] capitalizedString];
        
        tmp = [NSString stringWithFormat:@"%@\t\t%li %@ %li", dayString, day, [monthSymbols objectAtIndex:month-1], year];
        
        
        [self setPrimitiveEntryDaySectionName:tmp];
    }
    return tmp;
}

#pragma mark Time stamp setter
- (void)setCreatedOn:(NSDate *)newDate {
    
    // If the time stamp changes, the section identifier become invalid.
    [self willChangeValueForKey:@"created_on"];
    [self setPrimitiveCreatedOn:newDate];
    
    [self didChangeValueForKey:@"created_on"];
    
    [self setPrimitiveEntryDaySectionName:nil];
}

#pragma mark Key path dependencies

+ (NSSet *)keyPathsForValuesAffectingEntryDaySectionName {
    // If the value of timeStamp changes, the section identifier may change as well.
    return [NSSet setWithObject:@"created_on"];
}


@end
