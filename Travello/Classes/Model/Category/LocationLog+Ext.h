//
//  LocationLog+Ext.h
//  Travello
//
//  Created by Balbir Shokeen on 2013/09/06.
//  Copyright (c) 2013 Balbir Shokeen. All rights reserved.
//

#import "LocationLog.h"

@interface LocationLog (Ext)

@property (nonatomic, retain) NSDate *primitiveCreatedOn;
@property (nonatomic, retain) NSString *primitiveEntryDaySectionName;

@end
