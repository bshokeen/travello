//
//  Trip+Ext.m
//  Travello
//
//  Created by Balbir Shokeen on 2014/01/19.
//  Copyright (c) 2014 Balbir Shokeen. All rights reserved.
//

#import "Trip+Ext.h"
#import "Location.h"

@implementation Trip (Ext)

@dynamic primitiveCreatedOn, primitiveSectionName, primitiveFrom_locname, primitiveTo_locname, primitiveTripName;


#pragma mark Transient properties - Section Name
- (NSString *)sectionName {
    
    // Create and cache the section identifier on demand.
    
    [self willAccessValueForKey:@"sectionName"];
    NSString *tmp = [self primitiveSectionName];
    [self didAccessValueForKey:@"sectionName"];
    
    if (!tmp) {
        
        NSCalendar *calendar = [NSCalendar currentCalendar];
        
        if ([self created_on] == nil)
            return APP_EMPTY;
        
        // We store data as a number yyyy0mmdd so that it can be sorted as desired but while displaying we extract parts of it using mutliplation and division.
        NSDate *createdOn = [self created_on];
        NSDateComponents *components = [calendar components:(NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit) fromDate:createdOn];
        long longDate = ((([components year] * 1000) + [components month]) * 100) + [components day];
        tmp = [NSString stringWithFormat:@"%ld", longDate] ;
        
        static NSArray *monthSymbols = nil;
        
        if (!monthSymbols) {
            NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
            [formatter setCalendar:[NSCalendar currentCalendar]];
            monthSymbols = [formatter shortMonthSymbols] ;
        }
        
        long numericSection = [tmp integerValue];
        long year = numericSection / 1000 /100;
        long tmpMonth = numericSection - (year * 1000 * 100);
        long month = tmpMonth/100;
        long day = tmpMonth - (month *100);
        
        
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        dateFormatter.dateFormat=@"EEE";
        NSString * dayString = [[dateFormatter stringFromDate:createdOn] capitalizedString];
        
        tmp = [NSString stringWithFormat:@"%@\t\t%ld %@ %ld", dayString, day, [monthSymbols objectAtIndex:month-1], year];
        
        
        [self setPrimitiveSectionName:tmp];
    }
    return tmp;
}

#pragma mark Time stamp setter
- (void)setCreatedOn:(NSDate *)newDate {
    
    // If the time stamp changes, the section identifier become invalid.
    [self willChangeValueForKey:@"created_on"];
    [self setPrimitiveCreatedOn:newDate];
    
    [self didChangeValueForKey:@"created_on"];
    
    [self setPrimitiveSectionName:nil];
}

#pragma mark Key path dependencies

+ (NSSet *)keyPathsForValuesAffectingSectionName {
    // If the value of timeStamp changes, the section identifier may change as well.
    return [NSSet setWithObject:@"created_on"];
}


#pragma mark Transient properties - Trip Name
- (NSString *)tripName {
    
    // Create and cache the section identifier on demand.
    
    [self willAccessValueForKey:@"tripName"];
    NSString *tmp = [self primitiveTripName];
    [self didAccessValueForKey:@"tripName"];
    
    if (!tmp) {
        
        NSString *fromLName = [self.fromLocation.name stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        NSString *toLName   = [self.toLocation.name stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        
        if (fromLName == nil) {
            fromLName = self.from_locname;
            if (fromLName == nil) {
                fromLName = APP_LOCN_UNKNOWN;
            }
        }
        if (toLName == nil) {
            toLName = self.to_locname;
            if (toLName == nil) {
                toLName = APP_LOCN_UNKNOWN;
            }
        }
        
        tmp = [fromLName stringByAppendingFormat:@" - %@", toLName];
        
        [self setPrimitiveTripName:tmp];
    }
    return tmp;
}


- (void)setFrom_locname:(NSString *)from_locname {
    
    // If the from_locname changes
    [self willChangeValueForKey:@"from_locname"];
    [self setPrimitiveFrom_locname:from_locname];
    [self didChangeValueForKey:@"from_locname"];
    
    [self setPrimitiveTripName:nil];
}

- (void)setTo_locname:(NSString *)to_locname {
    
    // If the from_locname changes
    [self willChangeValueForKey:@"to_locname"];
    [self setPrimitiveTo_locname:to_locname];
    [self didChangeValueForKey:@"to_locname"];
    
    [self setPrimitiveTripName:nil];
}
#pragma mark Key path dependencies

+ (NSSet *)keyPathsForValuesAffectingTripName {
    // If the value of timeStamp changes, the section identifier may change as well.
    return [NSSet setWithObjects:@"from_locname", @"to_locname", nil];
}


@end
