//
//  RouteLog+Ext.m
//  Travello
//
//  Created by Balbir Shokeen on 2013/12/05.
//  Copyright (c) 2013 Balbir Shokeen. All rights reserved.
//

#import "RouteLog+Ext.h"

@implementation RouteLog (Ext)

@dynamic primitiveCreatedOn, primitiveSectionName;


#pragma mark Transient properties - Created On
- (NSString *)sectionName {
    
    // Create and cache the section identifier on demand.
    
    [self willAccessValueForKey:@"sectionName"];
    NSString *tmp = [self primitiveSectionName];
    [self didAccessValueForKey:@"sectionName"];
    
    if (!tmp) {
        
        NSCalendar *calendar = [NSCalendar currentCalendar];
        
        if ([self created_on] == nil)
            return APP_EMPTY;
        
        // We store data as a number yyyy0mmdd so that it can be sorted as desired but while displaying we extract parts of it using mutliplation and division.
        NSDate *createdOn = [self created_on];
        NSDateComponents *components = [calendar components:(NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit) fromDate:createdOn];
        
        long dateLong = ((([components year] * 1000) + [components month]) * 100) + [components day];
        tmp = [NSString stringWithFormat:@"%ld", dateLong] ;
        
        static NSArray *monthSymbols = nil;
        
        if (!monthSymbols) {
            NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
            [formatter setCalendar:[NSCalendar currentCalendar]];
            monthSymbols = [formatter shortMonthSymbols] ;
        }
        
        long numericSection = [tmp integerValue];
        long year = numericSection / 1000 /100;
        long tmpMonth = numericSection - (year * 1000 * 100);
        long month = tmpMonth/100;
        long day = tmpMonth - (month *100);
        
        
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        dateFormatter.dateFormat=@"EEE";
        NSString * dayString = [[dateFormatter stringFromDate:createdOn] capitalizedString];
        
        tmp = [NSString stringWithFormat:@"%@\t\t%ld %@ %ld", dayString, day, [monthSymbols objectAtIndex:month-1], year];
        
        
        [self setPrimitiveSectionName:tmp];
    }
    return tmp;
}

#pragma mark Time stamp setter
- (void)setCreatedOn:(NSDate *)newDate {
    
    // If the time stamp changes, the section identifier become invalid.
    [self willChangeValueForKey:@"created_on"];
    [self setPrimitiveCreatedOn:newDate];
    
    [self didChangeValueForKey:@"created_on"];
    
    [self setPrimitiveSectionName:nil];
}

#pragma mark Key path dependencies

+ (NSSet *)keyPathsForValuesAffectingSectionName {
    // If the value of timeStamp changes, the section identifier may change as well.
    return [NSSet setWithObject:@"created_on"];
}


@end
