//
//  Trip+Ext.h
//  Travello
//
//  Created by Balbir Shokeen on 2014/01/19.
//  Copyright (c) 2014 Balbir Shokeen. All rights reserved.
//

#import "Trip.h"

@interface Trip (Ext)

@property (nonatomic, retain) NSDate *primitiveCreatedOn;
@property (nonatomic, retain) NSString *primitiveSectionName;
@property (nonatomic, retain) NSString *primitiveFrom_locname;
@property (nonatomic, retain) NSString *primitiveTo_locname;
@property (nonatomic, retain) NSString *primitiveTripName;

@end
