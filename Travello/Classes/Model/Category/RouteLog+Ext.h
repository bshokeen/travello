//
//  RouteLog+Ext.h
//  Travello
//
//  Created by Balbir Shokeen on 2013/12/05.
//  Copyright (c) 2013 Balbir Shokeen. All rights reserved.
//

#import "RouteLog.h"

@interface RouteLog (Ext)

@property (nonatomic, retain) NSDate *primitiveCreatedOn;
@property (nonatomic, retain) NSString *primitiveSectionName;


@end
