//
//  TripSummary.h
//  Travello
//
//  Created by Balbir Shokeen on 2014/01/20.
//  Copyright (c) 2014 Balbir Shokeen. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Location;

@interface TripSummary : NSManagedObject

@property (nonatomic, retain) NSString * to_locn_identifier;
@property (nonatomic, retain) NSDate * created_on;
@property (nonatomic, retain) NSString * extra1;
@property (nonatomic, retain) NSString * from_locn_identifier;
@property (nonatomic, retain) NSString * image_path;
@property (nonatomic, retain) NSNumber * image_type;
@property (nonatomic, retain) NSNumber * max_time;
@property (nonatomic, retain) NSNumber * min_time;
@property (nonatomic, retain) NSString * route_name;
@property (nonatomic, retain) NSNumber * sort_order;
@property (nonatomic, retain) NSNumber * total_distance;
@property (nonatomic, retain) NSNumber * total_time;
@property (nonatomic, retain) NSNumber * total_trips;
@property (nonatomic, retain) NSString * from_locn_name;
@property (nonatomic, retain) NSString * to_locn_name;
@property (nonatomic, retain) NSString * from_locn_img;
@property (nonatomic, retain) NSString * to_locn_img;
@property (nonatomic, retain) Location *fromLocation;
@property (nonatomic, retain) Location *toLocation;

@end
