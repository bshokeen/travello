//
//  RouteLog.m
//  Travello
//
//  Created by Balbir Shokeen on 2014/01/20.
//  Copyright (c) 2014 Balbir Shokeen. All rights reserved.
//

#import "RouteLog.h"
#import "Route.h"


@implementation RouteLog

@dynamic created_on;
@dynamic left_at;
@dynamic left_at_coordinate;
@dynamic modified_on;
@dynamic reached_at;
@dynamic reachted_at_coordinate;
@dynamic sectionName;
@dynamic route;

@end
