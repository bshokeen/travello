//
//  Trip.h
//  Travello
//
//  Created by Balbir Shokeen on 2014/01/26.
//  Copyright (c) 2014 Balbir Shokeen. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Location;

@interface Trip : NSManagedObject

@property (nonatomic, retain) NSDate * created_on;
@property (nonatomic, retain) NSNumber * finished;
@property (nonatomic, retain) NSDate * from_at;
@property (nonatomic, retain) NSString * from_locname;
@property (nonatomic, retain) NSString * from_region_identifier;
@property (nonatomic, retain) NSDate * modified_on;
@property (nonatomic, retain) NSString * sectionName;
@property (nonatomic, retain) NSDate * to_at;
@property (nonatomic, retain) NSString * to_locname;
@property (nonatomic, retain) NSString * to_region_identifier;
@property (nonatomic, retain) NSString * tripName;
@property (nonatomic, retain) NSString * from_locn_img;
@property (nonatomic, retain) NSString * to_locn_img;
@property (nonatomic, retain) Location *fromLocation;
@property (nonatomic, retain) Location *toLocation;

@end
