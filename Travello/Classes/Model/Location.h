//
//  Location.h
//  Travello
//
//  Created by Balbir Shokeen on 2014/01/20.
//  Copyright (c) 2014 Balbir Shokeen. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class LocationLog, Route, Trip, TripSummary;

@interface Location : NSManagedObject

@property (nonatomic, retain) NSString * address;
@property (nonatomic, retain) NSDate * created_on;
@property (nonatomic, retain) NSNumber * dirty;
@property (nonatomic, retain) NSNumber * extra1_int;
@property (nonatomic, retain) NSString * extra2_str;
@property (nonatomic, retain) NSNumber * flag;
@property (nonatomic, retain) NSString * image_path;
@property (nonatomic, retain) NSNumber * image_type;
@property (nonatomic, retain) NSDecimalNumber * latitude;
@property (nonatomic, retain) NSDecimalNumber * longitude;
@property (nonatomic, retain) NSDate * modified_on;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSDecimalNumber * radius;
@property (nonatomic, retain) NSString * region_identifier;
@property (nonatomic, retain) NSNumber * sort_order;
@property (nonatomic, retain) NSNumber * status;
@property (nonatomic, retain) NSSet *fromLocationTripS;
@property (nonatomic, retain) NSSet *fromRouteS;
@property (nonatomic, retain) NSSet *locationLogS;
@property (nonatomic, retain) NSSet *toLocationTripS;
@property (nonatomic, retain) NSSet *toRouteS;
@property (nonatomic, retain) TripSummary *fromLocnTripSummary;
@property (nonatomic, retain) TripSummary *toLocnTripSummary;
@end

@interface Location (CoreDataGeneratedAccessors)

- (void)addFromLocationTripSObject:(Trip *)value;
- (void)removeFromLocationTripSObject:(Trip *)value;
- (void)addFromLocationTripS:(NSSet *)values;
- (void)removeFromLocationTripS:(NSSet *)values;

- (void)addFromRouteSObject:(Route *)value;
- (void)removeFromRouteSObject:(Route *)value;
- (void)addFromRouteS:(NSSet *)values;
- (void)removeFromRouteS:(NSSet *)values;

- (void)addLocationLogSObject:(LocationLog *)value;
- (void)removeLocationLogSObject:(LocationLog *)value;
- (void)addLocationLogS:(NSSet *)values;
- (void)removeLocationLogS:(NSSet *)values;

- (void)addToLocationTripSObject:(Trip *)value;
- (void)removeToLocationTripSObject:(Trip *)value;
- (void)addToLocationTripS:(NSSet *)values;
- (void)removeToLocationTripS:(NSSet *)values;

- (void)addToRouteSObject:(Route *)value;
- (void)removeToRouteSObject:(Route *)value;
- (void)addToRouteS:(NSSet *)values;
- (void)removeToRouteS:(NSSet *)values;

@end
