//
//  TripSummary.m
//  Travello
//
//  Created by Balbir Shokeen on 2014/01/20.
//  Copyright (c) 2014 Balbir Shokeen. All rights reserved.
//

#import "TripSummary.h"
#import "Location.h"


@implementation TripSummary

@dynamic to_locn_identifier;
@dynamic created_on;
@dynamic extra1;
@dynamic from_locn_identifier;
@dynamic image_path;
@dynamic image_type;
@dynamic max_time;
@dynamic min_time;
@dynamic route_name;
@dynamic sort_order;
@dynamic total_distance;
@dynamic total_time;
@dynamic total_trips;
@dynamic from_locn_name;
@dynamic to_locn_name;
@dynamic from_locn_img;
@dynamic to_locn_img;
@dynamic fromLocation;
@dynamic toLocation;

@end
