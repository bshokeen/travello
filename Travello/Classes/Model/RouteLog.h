//
//  RouteLog.h
//  Travello
//
//  Created by Balbir Shokeen on 2014/01/20.
//  Copyright (c) 2014 Balbir Shokeen. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Route;

@interface RouteLog : NSManagedObject

@property (nonatomic, retain) NSDate * created_on;
@property (nonatomic, retain) NSDate * left_at;
@property (nonatomic, retain) NSDecimalNumber * left_at_coordinate;
@property (nonatomic, retain) NSDate * modified_on;
@property (nonatomic, retain) NSDate * reached_at;
@property (nonatomic, retain) NSDecimalNumber * reachted_at_coordinate;
@property (nonatomic, retain) NSString * sectionName;
@property (nonatomic, retain) Route *route;

@end
