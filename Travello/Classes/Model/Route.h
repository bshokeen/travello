//
//  Route.h
//  Travello
//
//  Created by Balbir Shokeen on 2014/01/20.
//  Copyright (c) 2014 Balbir Shokeen. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Location, RouteLog;

@interface Route : NSManagedObject

@property (nonatomic, retain) NSDate * created_on;
@property (nonatomic, retain) NSString * image_path;
@property (nonatomic, retain) NSNumber * image_type;
@property (nonatomic, retain) NSNumber * sort_order;
@property (nonatomic, retain) Location *fromLocation;
@property (nonatomic, retain) NSSet *routeLogS;
@property (nonatomic, retain) Location *toLocation;
@end

@interface Route (CoreDataGeneratedAccessors)

- (void)addRouteLogSObject:(RouteLog *)value;
- (void)removeRouteLogSObject:(RouteLog *)value;
- (void)addRouteLogS:(NSSet *)values;
- (void)removeRouteLogS:(NSSet *)values;

@end
