//
//  Route.m
//  Travello
//
//  Created by Balbir Shokeen on 2014/01/20.
//  Copyright (c) 2014 Balbir Shokeen. All rights reserved.
//

#import "Route.h"
#import "Location.h"
#import "RouteLog.h"


@implementation Route

@dynamic created_on;
@dynamic image_path;
@dynamic image_type;
@dynamic sort_order;
@dynamic fromLocation;
@dynamic routeLogS;
@dynamic toLocation;

@end
