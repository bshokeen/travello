//
//  LocationLog.h
//  Travello
//
//  Created by Balbir Shokeen on 2014/01/20.
//  Copyright (c) 2014 Balbir Shokeen. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Location;

@interface LocationLog : NSManagedObject

@property (nonatomic, retain) NSDate * created_on;
@property (nonatomic, retain) NSDate * did_enter;
@property (nonatomic, retain) NSDecimalNumber * did_enter_latitude;
@property (nonatomic, retain) NSDecimalNumber * did_enter_longitude;
@property (nonatomic, retain) NSDate * did_exit;
@property (nonatomic, retain) NSDecimalNumber * did_exit_latitude;
@property (nonatomic, retain) NSDecimalNumber * did_exit_longitude;
@property (nonatomic, retain) NSString * entryDaySectionName;
@property (nonatomic, retain) NSDate * modified_on;
@property (nonatomic, retain) NSNumber * sort_order;
@property (nonatomic, retain) Location *location;

@end
