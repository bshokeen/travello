//
//  Trip.m
//  Travello
//
//  Created by Balbir Shokeen on 2014/01/26.
//  Copyright (c) 2014 Balbir Shokeen. All rights reserved.
//

#import "Trip.h"
#import "Location.h"


@implementation Trip

@dynamic created_on;
@dynamic finished;
@dynamic from_at;
@dynamic from_locname;
@dynamic from_region_identifier;
@dynamic modified_on;
@dynamic sectionName;
@dynamic to_at;
@dynamic to_locname;
@dynamic to_region_identifier;
@dynamic tripName;
@dynamic from_locn_img;
@dynamic to_locn_img;
@dynamic fromLocation;
@dynamic toLocation;

@end
