//
//  Location.m
//  Travello
//
//  Created by Balbir Shokeen on 2014/01/20.
//  Copyright (c) 2014 Balbir Shokeen. All rights reserved.
//

#import "Location.h"
#import "LocationLog.h"
#import "Route.h"
#import "Trip.h"
#import "TripSummary.h"


@implementation Location

@dynamic address;
@dynamic created_on;
@dynamic dirty;
@dynamic extra1_int;
@dynamic extra2_str;
@dynamic flag;
@dynamic image_path;
@dynamic image_type;
@dynamic latitude;
@dynamic longitude;
@dynamic modified_on;
@dynamic name;
@dynamic radius;
@dynamic region_identifier;
@dynamic sort_order;
@dynamic status;
@dynamic fromLocationTripS;
@dynamic fromRouteS;
@dynamic locationLogS;
@dynamic toLocationTripS;
@dynamic toRouteS;
@dynamic fromLocnTripSummary;
@dynamic toLocnTripSummary;

@end
