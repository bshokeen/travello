//
//  LocationLog.m
//  Travello
//
//  Created by Balbir Shokeen on 2014/01/20.
//  Copyright (c) 2014 Balbir Shokeen. All rights reserved.
//

#import "LocationLog.h"
#import "Location.h"


@implementation LocationLog

@dynamic created_on;
@dynamic did_enter;
@dynamic did_enter_latitude;
@dynamic did_enter_longitude;
@dynamic did_exit;
@dynamic did_exit_latitude;
@dynamic did_exit_longitude;
@dynamic entryDaySectionName;
@dynamic modified_on;
@dynamic sort_order;
@dynamic location;

@end
