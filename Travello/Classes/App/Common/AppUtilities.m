//
//  AppUtilities.m
//  Travello
//
//  Created by Balbir Shokeen on 2013/08/31.
//  Copyright (c) 2013 Balbir Shokeen. All rights reserved.
//

#import "AppUtilities.h"

@implementation AppUtilities

+(NSString *) getImageName: (NSString *) imageTag {
    
    NSString *imgName = nil;
    
    if ([imageTag isEqualToString:@"deleteLog"]) {
        imgName = @"BT_Delete.png";
    }
    else if ([imageTag isEqualToString:@"editLog"]) {
        imgName = @"BT_Edit.png";
    }
    else if ([imageTag isEqualToString:@"removeRegion"])
        imageTag = @"RemoveRegion";
        
    return imgName;
}

+(void) showLocalNotification:(NSString *)message {
    // Show notifications if the settings allow
    if ( [[AppSharedData getInstance].appSettings showDidEnterExitNotifications]) {
        UILocalNotification *note = [[UILocalNotification alloc] init];
        note.alertBody= message;
        note.soundName = UILocalNotificationDefaultSoundName;
        [[UIApplication sharedApplication] presentLocalNotificationNow:note];
    }
}

+ (void) showSimpleAlertWithTitle:(NSString *) title WithMessage:(NSString *) message {
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title message:message delegate:self cancelButtonTitle:NSLocalizedString(APP_CANCEL, APP_CANCEL) otherButtonTitles: NSLocalizedString(APP_OK, APP_OK), nil];
    [alert show];
}
+ (void) showSimpleAlertWithMessage:(NSString *) message Title:(NSString *)title{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title message:message delegate:self cancelButtonTitle:nil otherButtonTitles: NSLocalizedString(APP_OK, APP_OK), nil];
    [alert show];
}

// Generic APIS for getting the background color and text color across the app
+(UIView *) getSelectedBackgroundView {
    UIView *selectedView = [[UIView alloc]init];
    //    selectedView.backgroundColor = [UIColor colorWithHue:0.612 saturation:.04 brightness:.96 alpha:1.0];
    selectedView.backgroundColor = [UIColor colorWithHue:0.557 saturation:1.0 brightness:1 alpha:1.0];
    return selectedView;
}

+(UIColor *) getSelectedTextHighlightColor {
    return [UIColor whiteColor];
}

// Insert Default Data if there are no records existing and not run once already
+ (void) runOnceAcrossDevice_InsertDefaultData {
    // Check if the default groups have been created or not
    
    if ( [[AppSharedData getInstance].appSettings runOnceAcrossDeviceInsertedDefaultData] == 0) {
        
        if ( [CoreDataOperations countLocations] == 0 ) {
            // Ensure no new records are added yet
            [CoreDataOperations insertDefaultData];
        }
    }
    
    //Save the setting, indicating it has been run once now
    [[AppSharedData getInstance].appSettings saveSettingForRunOnceAcrossDeviceInsertedDefaultData: 1];
}

+ (float)distanceBetweenA: (CLLocationCoordinate2D) pointACoord B:(CLLocationCoordinate2D) pointBCoord  {
    // Calculate Distance
    
    
    CLLocation *pointALocation = [[CLLocation alloc] initWithLatitude:pointACoord.latitude longitude:pointACoord.longitude];
    
    
    CLLocation *pointBLocation = [[CLLocation alloc] initWithLatitude:pointBCoord.latitude longitude:pointBCoord.longitude];
    
    float distanceMeters = [pointALocation distanceFromLocation:pointBLocation];
    
    return distanceMeters;
}

+ (NSString*)getReadableTimeWithStartDate:(NSDate*) startDate WithEndDate: (NSDate*)endDate {
    
    NSString *readableTime = nil;
    
    if (startDate != nil && endDate != nil) {
        readableTime = @""; // Initialize with some blank text
        
        NSCalendar *gregorianCalendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
        NSUInteger unitFlags = NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit | NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit;
        NSDateComponents *components = [gregorianCalendar components:unitFlags
                                                            fromDate:startDate
                                                              toDate:endDate
                                        
                                                             options:0];
        
        long year = [components year];
        long month= [components month];
        long day  = [components day];
        long hour = [components hour];
        long min  = [components minute];
        long sec  = [components second];
        
        if ( year > 0 )
            readableTime = [NSString stringWithFormat:@"%li Y  %li M  %li d  %li h  %li m", year, month, day, hour, min];
        else if ( month > 0)
            readableTime = [NSString stringWithFormat:@"%li M  %li d  %li h  %li m", month, day, hour, min];
        else if (day > 0) {
            readableTime = [NSString stringWithFormat:@"%li d  %li h  %li m", day, hour, min];
        }
        else if ( hour > 0 ) {
            readableTime = [NSString stringWithFormat:@"%li h  %li m", hour, min];
        }
        else if ( min > 0) {
            readableTime = [NSString stringWithFormat:@"%li m", min];
        }
        else {
            readableTime = [NSString stringWithFormat:@"%li s", sec];
        }
    }
    
    return readableTime;
}

+ (NSString*)getReadableTime:(double)timeInMilliSecs {
    
    NSDate *curDate = [NSDate date];
    NSDate *newDate    = [NSDate dateWithTimeInterval:timeInMilliSecs sinceDate:curDate];
    
    
    return [AppUtilities getReadableTimeWithStartDate:curDate WithEndDate:newDate];
}



+ (NSDate *) getDatePartOnlyFor:(NSDate *)inputDate {
    
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    // get year, month and day parts
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init] ;
    dateFormatter.dateFormat = @"yyyy-MM-dd HH:mm:ss ZZZ";
    
    // create a date object using custom dateComponents against current calendar
    NSDateComponents *todayCmpnt = [calendar components:(NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit) fromDate:inputDate];
 
    [dateFormatter dateFromString:
     [NSString stringWithFormat:@"%li-%li-%li 00:00:00 +0000",
      (long)[todayCmpnt year],
      (long)[todayCmpnt month],
      (long)[todayCmpnt day]]];
    
    NSDate *today = [calendar dateFromComponents:todayCmpnt];
    
    
    return today;
}

+ (NSDate *) getStartOfMonthDate:(NSDate*)inputDate {
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSCalendar *gregorian = [[NSCalendar alloc]
                             initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *monthdayComponents =
    [gregorian components:(NSYearCalendarUnit | NSMonthCalendarUnit) fromDate:inputDate];
    
    NSDate *startOfMonth = [calendar dateFromComponents:monthdayComponents];
    

    return startOfMonth;
}

+ (NSDate *) getStartOfNextDayDate:(NSDate *)inputDate {
    // Addition of day + 1 was not going to next month, so using this API to get right date
    NSDate *tomorrow = [NSDate dateWithTimeInterval:1*24*60*60 sinceDate:inputDate];
    return tomorrow;
}

+(NSDate *)getStartOfWeekDate:(NSDate *)inputDate {
    NSCalendar *gregorian = [[NSCalendar alloc]
                             initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *weekdayComponents =
    [gregorian components:(NSDayCalendarUnit | NSWeekdayCalendarUnit) fromDate:inputDate];
    NSInteger weekday = [weekdayComponents weekday];
    
    // Calendar day is starting from Sunday, so if you are on Monday it would give weekday as 2. To get end of the week 7 - 2 = 5, add 5 days in todays date. It gives the date for Saturday. But as I am searching data with time 00:00:00 I am adding 1 extra day to get the next date. As I don't want to start my week from Sunday rather Monday, so adding 1 more to shift from Sunday to Monday. Hence 7 - weekday + 2.
    NSDate *weekStart = [NSDate dateWithTimeInterval:-(weekday)*24*60*60 sinceDate:inputDate];
    
    
    return weekStart;
}

#pragma mark - General Purpose
+(NSMutableDictionary*) getTopDescendingItemCount__UNUSED_:(int)topX From:(NSMutableDictionary *)itemKeyValues {
    
    if (itemKeyValues == nil) {
        return nil;
    }
    
    NSMutableArray *allVALUES = [[NSMutableArray alloc] initWithArray:itemKeyValues.allValues];
    
    NSSortDescriptor *highestToLowest = [NSSortDescriptor sortDescriptorWithKey:@"self" ascending:NO];
    [allVALUES sortUsingDescriptors:[NSArray arrayWithObject:highestToLowest]];
    
    // Generate a map with values <array:keys> (array -> duplicate value but different names)
    NSMutableDictionary *reverseData = [[NSMutableDictionary alloc] init];
    for (id key in itemKeyValues) {
        
        NSString *value = [itemKeyValues objectForKey:key];
        
        NSMutableArray *storedValueAr = [reverseData objectForKey:value];
        if (storedValueAr) {
            [storedValueAr addObject:key];
        }
        else {
            storedValueAr = [[NSMutableArray alloc] init];
            [storedValueAr addObject:key];
            [reverseData setObject:storedValueAr forKey:value];
        }
    }
    
    // Now iterate in sorted manner and prepare the required data dictionary
    if (topX == -1)
        topX = (int)[reverseData count];
    
    NSMutableDictionary *topXOrderedByValues = [[NSMutableDictionary alloc]init];
    
    for (NSNumber *value in allVALUES ) {
        if (topX == 0)
            break;
        
        NSArray *keyS = [reverseData objectForKey:value];
        for (NSString *item in keyS) {
            
            NSMutableArray *orderedKeys   = [topXOrderedByValues objectForKey:@"orderedkeys"];
            NSMutableArray *orderedValues = [ topXOrderedByValues objectForKey:@"orderedvalues"];
            
            if (orderedKeys == Nil) {
                orderedKeys   = [[NSMutableArray alloc] init];
                orderedValues = [[NSMutableArray alloc] init];
                
                [topXOrderedByValues setObject:orderedKeys forKey:@"orderedkeys"];
                [topXOrderedByValues setObject:orderedValues forKey:@"orderedvalues"];
            }
            
            [orderedKeys addObject:item];
            [orderedValues addObject:value];
        }
        
        --topX;
    }
    
    // Reverse again to bring back in desired format
    //    topXOrderedByValues = [[NSMutableDictionary alloc] initWithObjects:topXOrderedByValues.allKeys forKeys:topXOrderedByValues.allValues];
    return topXOrderedByValues;
}

+(NSMutableDictionary*) getTopDescendingItemCount:(int)topX From:(NSMutableDictionary *)itemKeyValues {
    
    if (itemKeyValues == nil) {
        return nil;
    }
    
    NSMutableDictionary *topXOrderedByValues = [[NSMutableDictionary alloc]init];
    
    NSMutableArray *allVALUES = [[NSMutableArray alloc] initWithArray:itemKeyValues.allValues];
    
    NSSortDescriptor *highestToLowest = [NSSortDescriptor sortDescriptorWithKey:@"self" ascending:NO];
    [allVALUES sortUsingDescriptors:[NSArray arrayWithObject:highestToLowest]];
    
    // Generate a map with values <array:keys> (array -> duplicate value but different names)
    NSMutableDictionary *reverseData = [[NSMutableDictionary alloc] init];
    for (id key in itemKeyValues) {

        NSString *value = [itemKeyValues objectForKey:key];
        
        NSMutableArray *storedValueAr = [reverseData objectForKey:value];
        if (storedValueAr) {
            [storedValueAr addObject:key];
        }
        else {
            storedValueAr = [[NSMutableArray alloc] init];
            [storedValueAr addObject:key];
            [reverseData setObject:storedValueAr forKey:value];
        }
    }
    
    // Now iterate in sorted manner and prepare the required data dictionary
    if (topX == -1)
        topX = (int)[reverseData count];
    
    for (NSNumber *value in allVALUES ) {
        if (topX == 0)
            break;
        
        NSArray *keyS = [reverseData objectForKey:value];
        for (NSString *item in keyS) {
            [topXOrderedByValues setObject:item forKey:value];
        }
        
        --topX;
    }
    
    // Reverse again to bring back in desired format
    topXOrderedByValues = [[NSMutableDictionary alloc] initWithObjects:topXOrderedByValues.allKeys forKeys:topXOrderedByValues.allValues];
    return topXOrderedByValues;
}

#pragma mark - Utility Api - RunOnce
+(void) runOnceOperations {
 
    // Show the required alerts first time.
    if ( [[AppSharedData getInstance].appSettings showOneTimeAlerts] == 0 ) {
    
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"IMPORTANT" message:@"Please keep your device WIFI ON for more accurate tracking!" delegate:self cancelButtonTitle:@"Dismiss" otherButtonTitles: nil];
        
        [[AppSharedData getInstance].appSettings saveShowOneTimeAlerts:1];
        [alert show];
    }
    
    // Enable for taking screenshot or testing
//    [CoreDataOperations insertDefaultData];
}




@end
