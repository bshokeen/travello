//
//  AppSharedGlobalData.h
//  Travello
//
//  Created by Balbir Shokeen on 2013/08/31.
//  Copyright (c) 2013 Balbir Shokeen. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AppSettings.h"
#import "AppLocationMgr.h"
#import "AppInAppMgr.h"

#define APP_SHARED_GLOBAL_DATA ([AppSharedData getInstance])


@interface AppSharedData : NSObject



// Holds a AppSettings Variable to access all the settings, someinstances uses appsettings directly also (ex:dynamicviewData
@property (nonatomic, strong) AppSettings    *appSettings;
@property (nonatomic, strong) AppLocationMgr *appLocationMgr;
@property (nonatomic, strong) AppInAppMgr    *appInAppMgr;

@property (nonatomic, strong) NSMutableDictionary *locationIcons;

+ (AppSharedData *) getInstance;

// API for use related to location icons and image
-(NSString *) defaultLocationIconImageName;
- (UIImage *) getLocationIconImageByName:(NSString *)imageName;
@end
