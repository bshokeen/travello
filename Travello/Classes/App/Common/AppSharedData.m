//
//  AppSharedGlobalData.m
//  Travello
//
//  Created by Balbir Shokeen on 2013/08/31.
//  Copyright (c) 2013 Balbir Shokeen. All rights reserved.
//

#import "AppSharedData.h"

@implementation AppSharedData


@synthesize appSettings = _appSettings;
@synthesize appLocationMgr = _appLocationMgr;
@synthesize locationIcons = _locationIcons;

static AppSharedData *appSharedData = nil;

+ (AppSharedData *) getInstance
{
    
    @synchronized(self)
    {
        if (appSharedData == nil)
        {
            appSharedData = [[AppSharedData alloc] init];
        }
    }
    return appSharedData;
}

-(id) init {
    self = [super init];
    
    // Initialize the App Level Settings Manager
    self.appSettings = [[AppSettings alloc] init];

    // Initialize the App Level Location Manager
    self.appLocationMgr = [[AppLocationMgr alloc] init ];
    
    if ( INAPP_ENABLED) {
    // Initialize the In-App Manager with the In-App Purchase identifiers
    NSSet * productIdentifiers = [NSSet setWithObjects:     INAPP_ProdIdentifier_EXT_LOCN, nil];
    
    self.appInAppMgr = [[AppInAppMgr alloc] initWithProductIdentifiers:productIdentifiers];
    }
    
    self.locationIcons = [[NSMutableDictionary alloc] init];
    [self.locationIcons setObject:[UIImage imageNamed:@"Location_Smile"]     forKey:@"Location_Smile"];
    [self.locationIcons setObject:[UIImage imageNamed:@"Location_Home"]      forKey:@"Location_Home"];
    [self.locationIcons setObject:[UIImage imageNamed:@"Location_Work"]      forKey:@"Location_Work"];
    [self.locationIcons setObject:[UIImage imageNamed:@"Location_Star"]      forKey:@"Location_Star"];
    [self.locationIcons setObject:[UIImage imageNamed:@"Location_Meeting"]   forKey:@"Location_Meeting"];
    [self.locationIcons setObject:[UIImage imageNamed:@"Location_Handshake"] forKey:@"Location_Handshake"];
    [self.locationIcons setObject:[UIImage imageNamed:@"Location_Bank"]      forKey:@"Location_Bank"];
    [self.locationIcons setObject:[UIImage imageNamed:@"Location_Schedule"]  forKey:@"Location_Schedule"];
    [self.locationIcons setObject:[UIImage imageNamed:@"Location_Library"]  forKey:@"Location_Library"];
    [self.locationIcons setObject:[UIImage imageNamed:@"Location_School"]  forKey:@"Location_School"];
    [self.locationIcons setObject:[UIImage imageNamed:@"Location_Round"]  forKey:@"Location_Round"];
    [self.locationIcons setObject:[UIImage imageNamed:@"Location_Arrow"]  forKey:@"Location_Arrow"];
//    [self.locationIcons setObject:[UIImage imageNamed:@"Location_Help"]  forKey:@"Location_Help"]; // Not adding here to avoid being shown in teh icon selection list. only to be shown on UI to let user select and make selection.
//    [self.locationIcons setObject:[UIImage imageNamed:@"Location_Blank"]  forKey:@"Location_Blank"]; //Not adding this as it was not appearing at the desired place in the icon selection view and confusing.
    return self;
}

-(NSString *) defaultLocationIconImageName {
    return @"Location_Blank";
}

- (UIImage *) getLocationIconImageByName:(NSString *)imageName {
    
    if (imageName == nil) {
        imageName = [self defaultLocationIconImageName];
    }
    
    return [self.locationIcons objectForKey:imageName];
}


@end
