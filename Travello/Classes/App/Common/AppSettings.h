//
//  AppSettings.h
//  Travello
//
//  Created by Balbir Shokeen on 2013/08/31.
//  Copyright (c) 2013 Balbir Shokeen. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AppSettings : NSObject

// Run Once for all devices using this app by inserting the default data
-(int) runOnceAcrossDeviceInsertedDefaultData;

-(void) saveSettingForRunOnceAcrossDeviceInsertedDefaultData: (int) runOnceAcrossDeviceInsertedDefaultData;

-(int) showDidEnterExitNotifications;
-(void) saveSettingShowDidEnterExitNotifications: (int) showDidEnterExitNotifications;

- (int) autoDeleteOldLogs;
-(void) saveSettingForAutoDeleteOldLogs:(int) autoDeleteOldLogs;

-(int) showOneTimeAlerts;
-(void) saveShowOneTimeAlerts: (int) showOneTimeAlerts;

-(bool) getInAppProductKeyPurchaseStatus: (NSString *) inAppProductKey;
-(void) saveInAppProductKeyPurchaseStatus:(NSString *) productKey Value:(bool) boolValue;

@end
