//
//  AppUtilities.h
//  Travello
//
//  Created by Balbir Shokeen on 2013/08/31.
//  Copyright (c) 2013 Balbir Shokeen. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CoreDataOperations.h"
#import "AppSharedData.h"
#import "Location.h"

@interface AppUtilities : NSObject

// String processing APIs
+(NSString *) getImageName: (NSString *) imageTag;

// Notification APIS
+ (void) showLocalNotification: (NSString *) message;
+ (void) showSimpleAlertWithMessage:(NSString *) message Title:(NSString *)title;

//+ (CLRegion *) findMonitoredRegionByIdentifier_old:(NSString *) identifier;

// Common API to get the highlight color and respect text color
+(UIView *) getSelectedBackgroundView;
+(UIColor *) getSelectedTextHighlightColor;

// Core Location related
+ (float)distanceBetweenA: (CLLocationCoordinate2D) pointACoord B:(CLLocationCoordinate2D) pointBCoord ;

// DATE & TIME OPERATIONS
+ (NSString*)getReadableTimeWithStartDate:(NSDate*) startDate WithEndDate: (NSDate*)endDate;
+ (NSString*)getReadableTime:(double)timeInMilliSecs;

+ (NSDate *) getDatePartOnlyFor:(NSDate *)inputDate;
+ (NSDate *) getStartOfNextDayDate:(NSDate *) inputDate;
+ (NSDate *) getStartOfWeekDate:(NSDate *)inputDate;
+ (NSDate *) getStartOfMonthDate:(NSDate*)inputDate;

// General Purpose API
+(NSMutableDictionary*) getTopDescendingItemCount:(int) topX From:(NSMutableDictionary*)items;

// All the run once api are listed here
+ (void) runOnceOperations;

@end
