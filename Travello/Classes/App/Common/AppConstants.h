//
//  AppConstants.h
//  Travello
//
//  Created by Balbir Shokeen on 2013/08/31.
//  Copyright (c) 2013 Balbir Shokeen. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Flurry.h"

#define IS_PAD                     ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad)

#define APP_DELEGATE               ((AppDelegate *)[[UIApplication sharedApplication] delegate])
#define APP_DELEGATE_MGDOBJCNTXT   ([(AppDelegate*)[[UIApplication sharedApplication] delegate] managedObjectContext])

#define HEADER_HEIGHT        25
#define CELL_HEIGHT          56

// TODO: Use Test Flight Simulator Logging
#if DEBUG == 0

#define DLog(inputFrmt, ...) NSLog((@"%@" inputFrmt), ##__VA_ARGS__)

#else
#define DLog(...) // For now show logs in debug and iphone also ain testing mode
//    #define DLog(inputFrmt, ...) NSLog((@"" inputFrmt), ##__VA_ARGS__)
#endif

#define ALog(inputFrmt, ...) NSLog((@"%s [Line %d] " inputFrmt), __PRETTY_FUNCTION__, __LINE__, ##__VA_ARGS__)
#define ULog(inputFrmt, ...)  { UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:[NSString stringWithFormat:inputFrmt, ##__VA_ARGS__]  delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil]; [alert show]; }


#define APP_NEVER           NSLocalizedString(@"Never", @"Never")
#define APP_OK              NSLocalizedString(@"OK", @"OK")
#define APP_CANCEL          NSLocalizedString(@"Cancel", @"Cancel")
#define APP_EMPTY           NSLocalizedString(@"Empty", @"Empty")
#define APP_LOCN_UNKNOWN    NSLocalizedString(@"SOMEWHERE", @"SOMEWHERE")
#define APP_BLANK   @""
#define APP_SPACED_HYPHEN  @"-    -    -"
#define APP_HYPHEN @"-"
#define DEFAULT_ALLOWED_DISTANCE 150.0 // Increased the proximity distance from 100 to 150 though the overlay radius is kept to 100.
#define DEFAULT_RADIUS 100.0  // Using default radius for checking distance between points was still leading to intersecting regions, may be because of direct distance and distance on road is different. So road distance may be 143 but when circle was drawn they were interesecting points
#define DEFAULT_LATITUDE_DELTA  0.01
#define DEFAULT_LONGITUDE_DELTA 0.01
#define DEFAULT_LOCN_IMGNAME @"Location_Arrow" // Use default icon in Trips/Routes if one is not specified
#define DUP_EVENT_GAP 5.0
#define DAYS_90 90

#define LOCATION_S_TAB_INDEX 2


#define INAPP_ENABLED NO // This feature works in good cases but needed to be tested and handled for more scenarios
#define INAPP_ProdIdentifier_EXT_LOCN  @"com.dasheri.travello.unlocklocations"
//#define INAPP_ProdIdentifier_EXT_LOCN  @"com.dasheri.logman.extendlocation"
#define MAX_FREE_REGION_ALLOWED 3

#define SECTION_NUMBER_STARTFROM 9000 //Starting value for section header to identify them across the app for updates on it

#define SECTION_NUMBER_STARTFROM_ROUTE 10000 

// Below unused
//Transaction receipt Verification URL
//#define kURLforAppStoreReceiptVerification @"https://buy.itunes.apple.com/verifyReceipt" //Production
#define kURLforAppStoreReceiptVerification @"https://sandbox.itunes.apple.com/verifyReceipt" //Sandbox


@interface AppConstants : NSObject

extern NSString * const PREFS_MY_CONSTANT;

// Added for cloud integration
extern NSString * const APP_ICLOUD_APPID;
extern NSString * const APP_NAME;
extern NSString * const APP_SQLITE_DBNAME;
extern NSString * const APP_DATANOSYNC_FOLDERNAME;
extern NSString * const APP_DATA_LOG_FOLDERNAME;

// Default Settings Plist File
extern NSString * const APP_PLISTNAME;



@end
