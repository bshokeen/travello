//
//  AppSettings.m
//  Travello
//
//  Created by Balbir Shokeen on 2013/08/31.
//  Copyright (c) 2013 Balbir Shokeen. All rights reserved.
//

#import "AppSettings.h"


@interface AppSettings()

@property (nonatomic, strong) NSString *path;
@property (nonatomic, strong) NSMutableDictionary *savedDefaultValues;

@end


@implementation AppSettings

@synthesize path = _path;
@synthesize savedDefaultValues = _savedDefaultValues;

-(id) init {
    self = [super init];
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    self.path = [documentsDirectory stringByAppendingPathComponent:APP_PLISTNAME];
    
    if ( [self settingExists] ) {
        //To reterive the data from the plist
        self.savedDefaultValues = [[NSMutableDictionary alloc] initWithContentsOfFile: self.path];
    }
    else {
        // If the file doesn’t exist, create an empty dictionary
        self.savedDefaultValues = [[NSMutableDictionary alloc] init];
    }
    
    return self;
}

- (bool) settingExists {
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    if ( [fileManager fileExistsAtPath: self.path] )     {
        return YES;
    }
    else {
        return NO;
    }
}


-(int) runOnceAcrossDeviceInsertedDefaultData {
    if (![self.savedDefaultValues objectForKey:@"RunOnceAcrossDeviceInsertedDefaultData"]  )
        return 0; // Default Value if the attribute is not set
    else
        return [[self.savedDefaultValues objectForKey:@"RunOnceAcrossDeviceInsertedDefaultData"] intValue];
}

-(int) showDidEnterExitNotifications {
    if (![self.savedDefaultValues objectForKey:@"ShowDidEnterExitNotifications"]  )
        return 1; // Default Value if the attribute is not set, setting 1 as after install it was not showing alerts and would like to have alerts by default.
    else
        return [[self.savedDefaultValues objectForKey:@"ShowDidEnterExitNotifications"] intValue];
}

- (int) autoDeleteOldLogs {
    if (![self.savedDefaultValues objectForKey:@"AutoDeleteOldLogs"] )
        return 1; // Default Value if the attribute is not set
    else
        return [[self.savedDefaultValues objectForKey:@"AutoDeleteOldLogs"] intValue];
}


-(int) showOneTimeAlerts {
    if (![self.savedDefaultValues objectForKey:@"ShowOneTimeAlerts"] )
        return 0; // Default Value if the attribute is not set
    else
        return [[self.savedDefaultValues objectForKey:@"ShowOneTimeAlerts"] intValue];
}

-(bool) getInAppProductKeyPurchaseStatus: (NSString *) inAppProductKey {
    
    NSMutableDictionary *keyValue = [self.savedDefaultValues objectForKey:@"InAppProductKeyPurchaseStatus"];
    if (keyValue == nil) {
        keyValue = [[NSMutableDictionary alloc] init];
    }
    NSString *value = [keyValue objectForKey:inAppProductKey];
    
    return (value != nil && [value isEqualToString:@"YES"]) ? YES: NO;
}

-(void) saveSettingForRunOnceAcrossDeviceInsertedDefaultData: (int) runOnceAcrossDeviceInsertedDefaultData {
    [self.savedDefaultValues setObject:[NSNumber numberWithInt:runOnceAcrossDeviceInsertedDefaultData] forKey:@"RunOnceAcrossDeviceInsertedDefaultData"];
    [self.savedDefaultValues writeToFile:self.path atomically:YES];     //To insert the data into the plist
}

-(void) saveSettingShowDidEnterExitNotifications: (int) showDidEnterExitNotifications {
    [self.savedDefaultValues setObject:[NSNumber numberWithInt:showDidEnterExitNotifications] forKey:@"ShowDidEnterExitNotifications"];
    [self.savedDefaultValues writeToFile:self.path atomically:YES];     //To insert the data into the plist
}
-(void) saveSettingForAutoDeleteOldLogs:(int) autoDeleteOldLogs {
    [self.savedDefaultValues setObject:[NSNumber numberWithInt:autoDeleteOldLogs] forKey:@"AutoDeleteOldLogs"];
    [self.savedDefaultValues writeToFile:self.path atomically:YES];     //To insert the data into the plist
}

-(void) saveShowOneTimeAlerts:(int)showOneTimeAlerts {
    [self.savedDefaultValues setObject:[NSNumber numberWithInt:showOneTimeAlerts] forKey:@"ShowOneTimeAlerts"];
    [self.savedDefaultValues writeToFile:self.path atomically:YES];     //To insert the data into the plist
}

-(void) saveInAppProductKeyPurchaseStatus:(NSString *) productKey Value:(bool) boolValue {
    NSString *value = (boolValue) ? @"YES" : @"NO";
    NSMutableDictionary *keyValue = [self.savedDefaultValues objectForKey:@"InAppProductKeyPurchaseStatus"];
    if (keyValue == nil) {
        keyValue = [[NSMutableDictionary alloc] init];
        [self.savedDefaultValues setObject:keyValue forKey:@"InAppProductKeyPurchaseStatus"];
    }
    [keyValue setObject:value forKey:productKey];
    
    [self.savedDefaultValues writeToFile:self.path atomically:YES];     //To insert the data into the plist
}

@end
