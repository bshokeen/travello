//
//  AppConstants.m
//  Travello
//
//  Created by Balbir Shokeen on 2013/08/31.
//  Copyright (c) 2013 Balbir Shokeen. All rights reserved.
//

#import "AppConstants.h"

@implementation AppConstants

NSString * const PREFS_MY_CONSTANT = @"prefs_my_constant";

// Added for cloud integration
NSString * const APP_ICLOUD_APPID          = @"4A4QA9AFG.com.dasheri.Travello";
NSString * const APP_NAME                  = @"Travello";
NSString * const APP_SQLITE_DBNAME         = @"Travello.sqlite";
NSString * const APP_DATANOSYNC_FOLDERNAME = @"TravelloData.nosync";
NSString * const APP_DATA_LOG_FOLDERNAME   = @"TravelloLogs";

// Default Settings Plist File
NSString * const APP_PLISTNAME             = @"Travello.plist";

@end
