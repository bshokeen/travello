//
//  CoreDataOperations.m
//  Travello
//
//  Created by Balbir Shokeen on 2013/08/31.
//  Copyright (c) 2013 Balbir Shokeen. All rights reserved.
//

#import "CoreDataOperations.h"
#import "AppConstants.h"
#import "AppUtilities.h"
#import "Flurry.h"

@implementation CoreDataOperations

// COunt locations to assist adding limited regions
+ (int) countLocations {
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    [fetchRequest setEntity: [NSEntityDescription entityForName:@"Location" inManagedObjectContext:APP_DELEGATE_MGDOBJCNTXT]];
    
    // Execute the fetch
    NSError *error = nil;
    int count = (int)[APP_DELEGATE_MGDOBJCNTXT countForFetchRequest:fetchRequest error:&error];
    
    return count;
}

// Insert basic records into the Group & Task Tables with some default meaningful values
+ (void) insertDefaultData {
    

    int locSortOrder=1;

    Location *locHome = [CoreDataOperations addLocationWithName:@"Home" WithLatitude:-26.169546959544913 WithLongitude:28.268477013417442 WithAddress:@"Leith Road, Boksburg, Gauteng, 1459" WithRadius:100 WithFlag:0 WithSortOrder:locSortOrder++ WithStatus:0 WithDirty:0 WithRegionIdentifier:@"-26.169547, 28.268477" WithExtra1_Int:0 WithExtra2_Str:@"Location_Home"];

    Location *locOffice = [CoreDataOperations addLocationWithName:@"Office" WithLatitude:-26.150499335514542 WithLongitude:28.2223900873477 WithAddress:@"Griffiths Road, Boksburg, Gauteng, 1459" WithRadius:100 WithFlag:0 WithSortOrder:locSortOrder++ WithStatus:0 WithDirty:0 WithRegionIdentifier:@"-26.150499, 28.222390" WithExtra1_Int:0 WithExtra2_Str:@"Location_Work"];

    Location *locSchool = [CoreDataOperations addLocationWithName:@"Kids School" WithLatitude:-26.14619259207771 WithLongitude:28.292648792266846 WithAddress:@"Beryl Street, Benoni, GP, 1501" WithRadius:100 WithFlag:0 WithSortOrder:locSortOrder++ WithStatus:0 WithDirty:0 WithRegionIdentifier:@"-26.146193, 28.292649" WithExtra1_Int:0 WithExtra2_Str:@"Location_Meeting"];
    
//    Location *locCustomer = [CoreDataOperations addLocationWithName:@"Customer" WithLatitude:-26.10431999433785 WithLongitude:28.050439953804016 WithAddress:@"67, 67 Park Lane, Sandton, Gauteng, 2196" WithRadius:100 WithFlag:0 WithSortOrder:locSortOrder++ WithStatus:0 WithDirty:0 WithRegionIdentifier:@"-26.104320, 28.050440" WithExtra1_Int:0 WithExtra2_Str:@"Location_Handshake"];
    
    
    
    
    NSDateComponents *components = [[NSDateComponents alloc] init];
//    [components setDay:23]; Does not help as time is recoreded as insert time
    [components setHour:0];
    [components setMinute:0];
    [components setSecond:0];
    
//    NSDate *today=[NSDate date];
    NSDate *today = [[NSCalendar currentCalendar] dateFromComponents:components];
    NSDate *today1_8AM  = [today dateByAddingTimeInterval:(8*60*60)];
    NSDate *today1_8_15AM  = [today dateByAddingTimeInterval:(8*60*60)  + (15*60)];
    NSDate *today1_8_20AM  = [today dateByAddingTimeInterval:(8*60*60)  + (20*60)];
    NSDate *today1_10_10AM = [today dateByAddingTimeInterval:(10*60*60) + (10*60)];
    NSDate *today1_6_30PM = [today dateByAddingTimeInterval:(18*60*60)  + (30*60)];
    NSDate *today1_7PM = [today dateByAddingTimeInterval:(19*60*60) ];
    
    int sortOrder=1;
    [CoreDataOperations addLocationLogWithDidEnterTime:nil WithDidExitTime:today1_8AM WithDidEnterLatitude:0.0 WithDidEnterLongitude:0.0 WithDidExitLatitude:[locHome.latitude doubleValue] WithDidExitLongitude:[locHome.longitude doubleValue] WithLocnIdentifier:locHome.region_identifier WithSortOrder:sortOrder++ ManualEntry:NO];
    
    [CoreDataOperations addLocationLogWithDidEnterTime:today1_8_15AM WithDidExitTime:today1_8_20AM WithDidEnterLatitude:[locSchool.latitude doubleValue] WithDidEnterLongitude:[locSchool.longitude doubleValue] WithDidExitLatitude:[locSchool.latitude doubleValue] WithDidExitLongitude:[locSchool.longitude doubleValue] WithLocnIdentifier:locSchool.region_identifier WithSortOrder:sortOrder++ ManualEntry:NO];
   
    [CoreDataOperations addLocationLogWithDidEnterTime:today1_10_10AM WithDidExitTime:today1_6_30PM WithDidEnterLatitude:[locOffice.latitude doubleValue] WithDidEnterLongitude:[locOffice.longitude doubleValue] WithDidExitLatitude:[locOffice.latitude doubleValue] WithDidExitLongitude:[locOffice.longitude doubleValue] WithLocnIdentifier:locOffice.region_identifier WithSortOrder:sortOrder++ ManualEntry:NO];

    
    [CoreDataOperations addLocationLogWithDidEnterTime:today1_7PM WithDidExitTime:nil WithDidEnterLatitude:[locHome.latitude doubleValue] WithDidEnterLongitude:[locHome.longitude doubleValue] WithDidExitLatitude:[locHome.latitude doubleValue] WithDidExitLongitude:[locHome.longitude doubleValue] WithLocnIdentifier:locHome.region_identifier WithSortOrder:sortOrder++ ManualEntry:NO];

    [[AppSharedData getInstance].appLocationMgr processTrip_ExitRegion_StartTrip:locHome.region_identifier ExitRegionStartTripTime:today1_8AM];
    [[AppSharedData getInstance].appLocationMgr processTrip_EnterRegion_EndTrip:locSchool.region_identifier EnterRegionEndTripTime:today1_8_15AM];
    
    
    [[AppSharedData getInstance].appLocationMgr processTrip_ExitRegion_StartTrip:locSchool.region_identifier ExitRegionStartTripTime:today1_8_20AM];
    [[AppSharedData getInstance].appLocationMgr processTrip_EnterRegion_EndTrip:locOffice.region_identifier EnterRegionEndTripTime:today1_10_10AM];
    
    [[AppSharedData getInstance].appLocationMgr processTrip_ExitRegion_StartTrip:locOffice.region_identifier ExitRegionStartTripTime:today1_6_30PM];
    [[AppSharedData getInstance].appLocationMgr processTrip_EnterRegion_EndTrip:locHome.region_identifier EnterRegionEndTripTime:today1_7PM];

    }

#pragma mark - Core Data Entity Object Operations
+ (LocationLog *) addLocationLogWithDidEnterTime:(NSDate *)didEnterTime WithDidExitTime:(NSDate *)didExitTime WithDidEnterLatitude:(double)didEnterLatitude WithDidEnterLongitude:(double)didEnterLongitude WithDidExitLatitude:(double)didExitLatitude WithDidExitLongitude:(double)didExitLatutide WithLocnIdentifier:(NSString *) identifier WithSortOrder:(int) sortOrder ManualEntry:(bool)isManualEntry {
    
    LocationLog *locLog = (LocationLog *)[NSEntityDescription insertNewObjectForEntityForName:@"LocationLog" inManagedObjectContext:APP_DELEGATE_MGDOBJCNTXT];
    // [someError show];
    
    [locLog setDid_enter:didEnterTime];
    [locLog setDid_exit:didExitTime];
    [locLog setDid_enter_latitude:[[NSDecimalNumber alloc] initWithDouble:didEnterLatitude]];
    [locLog setDid_enter_longitude:[[NSDecimalNumber alloc] initWithDouble:didEnterLongitude]];
    
    if (isManualEntry) { // Bug Fix for Manual Entry UI. Entry should appear on the date it was related to
        [locLog setCreated_on:didEnterTime];
    } else {
        [locLog setCreated_on:[NSDate date]];
    }
    
    [locLog setModified_on:[NSDate date]];
    [locLog setSort_order:[NSNumber numberWithInt:sortOrder]]; // Not required but keeping consistency across similar entities
    
    // Find associated location using the available identifier
    Location *location = [CoreDataOperations getLocationByIdentifier:identifier];
    locLog.location = location;
    
    if (sortOrder > 0 ) {
        //use what the caller sent
    }
    else { //determine sort oder
        // Add Sort Order to each task in a way that each group has its own counter starting from 1. Here we just getting max sort order value of existing tasks and increment by 1
        int maxSortOrder = [CoreDataOperations getMaxSortOrder: @"LocationLog"];
        [locLog setSort_order: [NSNumber numberWithInt: maxSortOrder + 1]];
        
    }
    
    // Save the context.
    NSError *error = nil;
    //    if (![self.fetchedResultsController.managedObjectContext save:&error] ) {
    if (![APP_DELEGATE_MGDOBJCNTXT save:&error]) {
        // Replace this implementation with code to handle the error appropriately.
        // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    return locLog;

}

+(Location *) addLocationWithName:(NSString *)name WithLatitude:(double)latitude WithLongitude:(double)longitude WithAddress:(NSString *)address WithRadius:(double)radius WithFlag:(int)flag WithSortOrder:(int)sortOrder WithStatus:(int)status WithDirty:(int)dirty WithRegionIdentifier:(NSString*)regionIdentifier WithExtra1_Int:(int)extra1int WithExtra2_Str:(NSString *)extra2String {
    
    Location *newLocation = (Location *)[NSEntityDescription insertNewObjectForEntityForName:@"Location" inManagedObjectContext:APP_DELEGATE_MGDOBJCNTXT];
    // [someError show];
    
    [newLocation setName:name];
    [newLocation setLatitude:[[NSDecimalNumber alloc] initWithDouble:latitude]];
    [newLocation setLongitude:[[NSDecimalNumber alloc] initWithDouble:longitude]];
    [newLocation setAddress:address];
    [newLocation setRadius:[[NSDecimalNumber alloc] initWithDouble:radius]];
    [newLocation setFlag:[NSNumber numberWithInt:flag]];
    [newLocation setSort_order:[NSNumber numberWithInt:sortOrder]];
    [newLocation setStatus:[NSNumber numberWithInt:status]];
    [newLocation setDirty:[NSNumber numberWithInt:dirty]];
    [newLocation setExtra1_int: [NSNumber numberWithInt:extra1int]];
    [newLocation setExtra2_str:extra2String];
    [newLocation setRegion_identifier:regionIdentifier];

    [newLocation setCreated_on:[NSDate date]];
    [newLocation setModified_on:[NSDate date]];

    
    if (sortOrder > 0 ) {
        //use what the caller sent
    }
    else { //determine sort oder
        // Add Sort Order to each task in a way that each group has its own counter starting from 1. Here we just getting max sort order value of existing tasks and increment by 1
        int maxSortOrder = [CoreDataOperations getMaxSortOrder: @"Location"];
        [newLocation setSort_order: [NSNumber numberWithInt: maxSortOrder + 1]];
        
    }
     
    // Save the context.
    NSError *error = nil;
    //    if (![self.fetchedResultsController.managedObjectContext save:&error] ) {
    if (![APP_DELEGATE_MGDOBJCNTXT save:&error]) {
        // Replace this implementation with code to handle the error appropriately.
        // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    return newLocation;
}

+(RouteLog*)addRouteLogWithRoute:(Route*)route WithLeftAt:(NSDate *)leftAt WithReachedT:(NSDate *)reachedAt WithLeftAtCoordinate:(NSDecimalNumber *)leftAtCoordinate WithReachedAtCoordinate:(NSDecimalNumber *)reachedAtCoordinate WithCreatedOn:(NSDate *)createdOn WithModifiedOn:(NSDate *)modifiedON {
    
    RouteLog *newRouteLog = (RouteLog *)[NSEntityDescription insertNewObjectForEntityForName:@"RouteLog" inManagedObjectContext:APP_DELEGATE_MGDOBJCNTXT];
    // [someError show];
    
    [newRouteLog setRoute:route];
    [newRouteLog setLeft_at:leftAt];
    [newRouteLog setReached_at:reachedAt];
    [newRouteLog setLeft_at_coordinate:leftAtCoordinate];
    [newRouteLog setReachted_at_coordinate:reachedAtCoordinate];
    [newRouteLog setCreated_on:[NSDate date]];
    [newRouteLog setModified_on:[NSDate date]];
    
        
    // Save the context.
    NSError *error = nil;
    //    if (![self.fetchedResultsController.managedObjectContext save:&error] ) {
    if (![APP_DELEGATE_MGDOBJCNTXT save:&error]) {
        // Replace this implementation with code to handle the error appropriately.
        // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    return newRouteLog;
    
}

+(Route*) addRouteWithFromLocation:(Location *)fromLocation WithToLocation:(Location *)toLocation WithSortOrder:(int)sortOrder WithImagePath:(NSString *)imagePath WithImageType:(int)imageType {
    
    Route *newRoute = (Route *)[NSEntityDescription insertNewObjectForEntityForName:@"Route" inManagedObjectContext:APP_DELEGATE_MGDOBJCNTXT];
    // [someError show];
    
    [newRoute setFromLocation:fromLocation];
    [newRoute setToLocation:toLocation];
    [newRoute setSort_order:[NSNumber numberWithInt:sortOrder]];
    [newRoute setImage_path:imagePath];
    [newRoute setImage_type:[NSNumber numberWithInt:imageType]];
    [newRoute setCreated_on:[NSDate date]];
    
    if (sortOrder > 0 ) {
        //use what the caller sent
    }
    else { //determine sort oder
        // Add Sort Order to each task in a way that each group has its own counter starting from 1. Here we just getting max sort order value of existing tasks and increment by 1
        int maxSortOrder = [CoreDataOperations getMaxSortOrder: @"Route"];
        [newRoute setSort_order: [NSNumber numberWithInt: maxSortOrder + 1]];
        
    }
    
    // Save the context.
    NSError *error = nil;
    //    if (![self.fetchedResultsController.managedObjectContext save:&error] ) {
    if (![APP_DELEGATE_MGDOBJCNTXT save:&error]) {
        // Replace this implementation with code to handle the error appropriately.
        // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    return newRoute;
}

+(TripSummary *) addUpdateTripSummaryWithFromLocation:(Location*)fromLocation ToLocation:(Location*)toLocation FromLocnIdentifier: (NSString*)fromLocnIdentifier ToLocnIdentifer:(NSString*)toLocnIdentifier FromLocnName:(NSString*)fromLocnName ToLocnName:(NSString*)toLocnName RouteName:(NSString*)routeName TimeDuration:(double)timeDuration FromLocnImg:(NSString*)fromLocnImg ToLocnImg:(NSString*)toLocnImg Extra1:(NSString*)extra1 TotalDistance:(double)totalDistance {
    
    if (fromLocnIdentifier == nil || toLocnIdentifier == nil) {
        @throw [NSException exceptionWithName:NSInternalInconsistencyException
                                       reason:[NSString stringWithFormat:@"addUpdateRowSummary: Invalid Input sent - %@", NSStringFromSelector(_cmd)]
                                     userInfo:nil];
    }
    
    TripSummary *tripSummary = [self findTripSummaryWithFromLocnID:fromLocnIdentifier WithToLocnID:toLocnIdentifier];
    if (tripSummary == nil) { // Add a new summary record
        tripSummary = (TripSummary *)[NSEntityDescription insertNewObjectForEntityForName:@"TripSummary" inManagedObjectContext:APP_DELEGATE_MGDOBJCNTXT];
        // [someError show];
        
        int sortOrder = 0;
        
        [tripSummary setFromLocation:fromLocation];
        [tripSummary setToLocation:toLocation];
        [tripSummary setFrom_locn_identifier:fromLocnIdentifier];
        [tripSummary setTo_locn_identifier:toLocnIdentifier];
        [tripSummary setRoute_name:routeName];
        [tripSummary setFrom_locn_name:fromLocnName];
        [tripSummary setFrom_locn_img:fromLocnImg];
        [tripSummary setTo_locn_name:toLocnName];
        [tripSummary setTo_locn_img:toLocnImg];
        [tripSummary setTotal_time:[NSNumber numberWithDouble:timeDuration]];
        [tripSummary setTotal_trips:[NSNumber numberWithInt:1]];
        [tripSummary setMax_time:[NSNumber numberWithDouble:timeDuration]];
        [tripSummary setMin_time:[NSNumber numberWithDouble:timeDuration]];
        [tripSummary setCreated_on:[NSDate date]];
        [tripSummary setSort_order: [NSNumber numberWithInt:sortOrder]];
        [tripSummary setExtra1:extra1];
        [tripSummary setTotal_distance:[NSNumber numberWithDouble: totalDistance]];
    
    }
    else { // Update the existing record
        
        tripSummary.total_trips    = [NSNumber numberWithInt:[tripSummary.total_trips intValue] + 1];
        tripSummary.total_distance = [NSNumber numberWithInt:[tripSummary.total_distance doubleValue] + totalDistance];
        tripSummary.total_time     = [NSNumber numberWithInt:[tripSummary.total_time doubleValue] + timeDuration];
        
        if (timeDuration > [tripSummary.max_time doubleValue])
            tripSummary.max_time = [NSNumber numberWithDouble:timeDuration];
        if (timeDuration < [tripSummary.min_time doubleValue])
            tripSummary.min_time = [NSNumber numberWithDouble:timeDuration];
        
    }
    
    [CoreDataOperations saveContext];
    return tripSummary;
}

+(TripSummary *) findTripSummaryWithFromLocnID:(NSString*)fromLocnID WithToLocnID:(NSString*)toLocnID {
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    [fetchRequest setEntity:[NSEntityDescription entityForName:@"TripSummary" inManagedObjectContext:APP_DELEGATE_MGDOBJCNTXT]];
    
    fetchRequest.predicate = [NSPredicate predicateWithFormat:@"from_locn_identifier == %@ && to_locn_identifier == %@", fromLocnID, toLocnID];
    
    [fetchRequest setFetchLimit:1];
    // Execute the fetch
    NSError *error = nil;
    NSArray *fetchResults = [APP_DELEGATE_MGDOBJCNTXT executeFetchRequest:fetchRequest error:&error];
    
    if ( fetchResults && [fetchResults count] > 0)
        return [fetchResults objectAtIndex:0];
    else
        return nil;
}

+(BOOL) deleteTripUpdateTripSummaryFor:(Trip *)trip
{
    if (trip == Nil) {
        return NO;
    }
    
    TripSummary *tripSummary = [self findTripSummaryWithFromLocnID:trip.fromLocation.region_identifier  WithToLocnID:trip.toLocation.region_identifier];
    if (tripSummary != nil) { // Add a new summary record
        
        double totalDistance = 0;
        double timeDuration = [trip.to_at timeIntervalSinceDate:trip.from_at];
        
        tripSummary.total_trips    = [NSNumber numberWithInt:[tripSummary.total_trips intValue] - 1];
        tripSummary.total_distance = [NSNumber numberWithInt:[tripSummary.total_distance doubleValue] - totalDistance];
        tripSummary.total_time     = [NSNumber numberWithInt:[tripSummary.total_time doubleValue] - timeDuration];
        
        [CoreDataOperations saveContext];
        return YES;
    }
    else {
        return NO;
    }
}

// TRIP Handling
+ (Trip *) addTrip_IsFinished:(BOOL)finished FromLocn: (Location *)fromLocn ToLocn:(Location*)toLocn FromLocName:(NSString*)fromLocName FromAt:(NSDate*)fromAt FromRegionIdentifier:(NSString*)fromRegionIdentifier ToLocName:(NSString*)toLocName ToAt:(NSDate*)toAt  ToRegionIdentifier:(NSString*)toRegionIdentifier CreatedOn:(NSDate *)createdOn ModifiedOn:(NSDate *)modifiedON {
    
    Trip *newTrip = (Trip *)[NSEntityDescription insertNewObjectForEntityForName:@"Trip" inManagedObjectContext:APP_DELEGATE_MGDOBJCNTXT];
    // [someError show];

    [newTrip setFinished:[NSNumber numberWithBool:finished]];
    [newTrip setFromLocation:fromLocn];
    [newTrip setToLocation:toLocn];
    [newTrip setFrom_locname:fromLocName];
    [newTrip setFrom_at:fromAt];
    [newTrip setFrom_region_identifier:fromRegionIdentifier];
    [newTrip setTo_locname:toLocName];
    [newTrip setTo_at:toAt];
    [newTrip setTo_region_identifier:toRegionIdentifier];
    [newTrip setFrom_locn_img:fromLocn.extra2_str];
    [newTrip setTo_locn_img:toLocn.extra2_str];
    [newTrip setCreated_on:createdOn];
    [newTrip setModified_on:modifiedON];
    
    // Added to generate route/trip summary
    if (fromLocn !=nil && toLocn !=nil && finished) {
        // If we closing the entry then automatically add/update the existing related entry for this trip
        [self addUpdateTripSummaryWithFromLocation:newTrip.fromLocation
                                        ToLocation:newTrip.toLocation
                                FromLocnIdentifier:fromLocn.region_identifier
                                         ToLocnIdentifer:toLocn.region_identifier
                                            FromLocnName:newTrip.from_locname
                                              ToLocnName:newTrip.to_locname
                                               RouteName:newTrip.tripName
                                            TimeDuration:[newTrip.to_at timeIntervalSinceDate:newTrip.from_at]
                                             FromLocnImg:newTrip.fromLocation.image_path
                                               ToLocnImg:newTrip.toLocation.image_path
                                                  Extra1:nil TotalDistance:0];
    }
    
    // Save the context.
    NSError *error = nil;
    //    if (![self.fetchedResultsController.managedObjectContext save:&error] ) {
    if (![APP_DELEGATE_MGDOBJCNTXT save:&error]) {
        // Replace this implementation with code to handle the error appropriately.
        // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    return newTrip;
}


#pragma mark - Delete Operations
+(BOOL) deleteItemByEntityName:(NSString *)entityName Item:(NSManagedObject *)item  {
    
    int sortOrder = -1;
    
    if ( [entityName isEqualToString:@"Location"] ) {
         Location *le = (Location *) item;
         sortOrder = [le.sort_order intValue];
     }
     else if ( [entityName isEqualToString:@"LocationLog"]) {
         LocationLog *le = (LocationLog *) item;
         sortOrder = [le.sort_order intValue];
     }
     else if ( [entityName isEqualToString:@"RouteLog"]) {
//         RouteLog *le = (RouteLog *) item;
//         sortOrder = [le.sort_order integerValue];
     }
     else if ([entityName isEqualToString:@"Trip"]) {
         Trip *trip = (Trip*) item;
         [CoreDataOperations deleteTripUpdateTripSummaryFor:trip];
     }
     else if ( [entityName isEqualToString:@"Route"]) {
         Route *le = (Route *) item;
         sortOrder = [le.sort_order intValue];
     }
     else {
         NSLog(@"Throw Exception");
     
     }

    // Delete the managed object.
    NSManagedObjectContext *context = APP_DELEGATE_MGDOBJCNTXT;
    [context deleteObject:item];
    
    // Before Deleting update the sorting order of all the task beyond the sorting order of task being deleted. This prevent duplicate sort order and hence bad tableview data

    NSArray *fetchedResults = nil; // If it is RouteLog there ins't sort_order column so no need to adjust sorting order.
    if ( ![entityName isEqualToString:@"RouteLog"] && ![entityName isEqualToString:@"Trip"] )
        fetchedResults = [CoreDataOperations getItemsGreaterThanSortOrder:sortOrder EntityName:entityName ];
    
    // Save Context
    [CoreDataOperations saveContext];
    
    // Saving context after doing the sorting order fix was resulting in crash when first itme of a group is deleted as the item was unavailable
    // could have queried the results after saving but then required task.group name
    // so retrieving the groups first, save context of delete operation, then perform the update on sorting order and finally save this also.
    for (int i=0; i < [fetchedResults count]; i++) {
        
        [[fetchedResults objectAtIndex:i] setSort_order: [NSNumber numberWithInt:sortOrder + i]];
    }
    // Save Context
    [CoreDataOperations saveContext];
    
    return YES;
}

/*
 * Delete tasks older than x days
 */
+ (int) deleteOldLogs: (int) olderThan {
    
    NSLog(@"Auto Delete old Logs - START");
    // Delete the managed object.
    NSManagedObjectContext *context = APP_DELEGATE_MGDOBJCNTXT;
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    [fetchRequest setEntity:
     [NSEntityDescription entityForName:@"LocationLog" inManagedObjectContext:context]];
    
    
    NSDate *completedBeforeThisDate = [NSDate dateWithTimeIntervalSinceNow:-24*60*60*olderThan];
    [fetchRequest setPredicate: [NSPredicate predicateWithFormat: @"(created_on < %@)", completedBeforeThisDate]];
    
    // Execute the fetch
    NSError *error = nil;
    NSArray *fetchResults = [context
                             executeFetchRequest:fetchRequest error:&error];
    
    NSLog(@"Records Fetched for deletion are: %i", (int)[fetchResults count]);
    
    int i = 0;
    for (i= 0; i < [fetchResults count]; i++) {
        LocationLog *item = [fetchResults objectAtIndex:i];
        
        NSLog(@"CoreDataOperations: Deleting Older Task: Name: %@ Created on: %@ RegionIdentifier: %@", item.location.name, [item.created_on description], item.location.region_identifier);
        
        [CoreDataOperations deleteItemByEntityName:@"LocationLog" Item:item];
    }
    
    return i;
}

#pragma mark - Query Opertions

// Find the location using identifier, then get the last logentry for it
+ (Location *) getLocationByIdentifier: (NSString *) regionIdentifier {
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    [fetchRequest setEntity:[NSEntityDescription entityForName:@"Location" inManagedObjectContext:APP_DELEGATE_MGDOBJCNTXT]];
    
    NSSortDescriptor *orderByDescriptor_1 = [[NSSortDescriptor alloc] initWithKey:@"created_on" ascending:NO];
    
    NSArray *sortDescriptors = [[NSArray alloc] initWithObjects:orderByDescriptor_1, nil];
    [fetchRequest setSortDescriptors:sortDescriptors];
    
    fetchRequest.predicate = [NSPredicate predicateWithFormat:@"region_identifier == %@", regionIdentifier];
    
    [fetchRequest setFetchLimit:1];
    // Execute the fetch
    NSError *error = nil;
    NSArray *fetchResults = [APP_DELEGATE_MGDOBJCNTXT executeFetchRequest:fetchRequest error:&error];
    
    if ( fetchResults && [fetchResults count] > 0)
        return [fetchResults objectAtIndex:0];
    else
        return nil;
}

// Find the location using identifier, then get the last logentry for it
+ (LocationLog *) getLatestLocationLogByIdentifier: (NSString *) regionIdentifier {
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    [fetchRequest setEntity:[NSEntityDescription entityForName:@"LocationLog" inManagedObjectContext:APP_DELEGATE_MGDOBJCNTXT]];
    
    NSSortDescriptor *orderByDescriptor_1 = [[NSSortDescriptor alloc] initWithKey:@"created_on" ascending:NO];
    
    NSArray *sortDescriptors = [[NSArray alloc] initWithObjects:orderByDescriptor_1, nil];
    [fetchRequest setSortDescriptors:sortDescriptors];
    
    fetchRequest.predicate = [NSPredicate predicateWithFormat:@"did_exit==nil && location.region_identifier == %@", regionIdentifier];

    [fetchRequest setFetchLimit:1];

    // Execute the fetch
    NSError *error = nil;
    NSArray *fetchResults = [APP_DELEGATE_MGDOBJCNTXT executeFetchRequest:fetchRequest error:&error];
    
    if ( fetchResults && [fetchResults count] > 0)
        return [fetchResults objectAtIndex:0];
    else
        return nil;
}

+(bool) ifLocationExists: (NSString *) identifier {
    bool retVal = NO;
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    [fetchRequest setEntity: [NSEntityDescription entityForName:@"Location" inManagedObjectContext:APP_DELEGATE_MGDOBJCNTXT]];
    
    fetchRequest.predicate = [NSPredicate predicateWithFormat:@"region_identifier=%@", identifier];
    // Execute the fetch
    NSError *error = nil;
    NSUInteger count = [APP_DELEGATE_MGDOBJCNTXT countForFetchRequest:fetchRequest error:&error];
    
    if (count > 0) {
        retVal = YES;
    }
    
    return retVal;
}


+(bool) ifLocnExistsOrTooCloseForIdentifier: (NSString *) identifier OrCoordinate: (CLLocationCoordinate2D) sourceCoord ExcludeSelf: (Location *) editLocn {
    bool retVal = NO;
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    [fetchRequest setEntity: [NSEntityDescription entityForName:@"Location" inManagedObjectContext:APP_DELEGATE_MGDOBJCNTXT]];
    
    if (editLocn == nil) { // If a location is newly added and there is a with same coordinate, return it exists
        fetchRequest.predicate = [NSPredicate predicateWithFormat:@"region_identifier == %@", identifier];
    }
    else { // If a location is being edited and excluding it there is some other location with same coordinate, return it exists
        fetchRequest.predicate = [NSPredicate predicateWithFormat:@"region_identifier == %@ && self != %@", identifier, editLocn];
    }

    // Execute the fetch
    NSError *error = nil;
    NSUInteger count = [APP_DELEGATE_MGDOBJCNTXT countForFetchRequest:fetchRequest error:&error];

    if (count > 0) {
        retVal = YES;
    }
    else { // Perform additional check to see if any location is in close proximity.
     
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
        [fetchRequest setEntity:[NSEntityDescription entityForName:@"Location" inManagedObjectContext:APP_DELEGATE_MGDOBJCNTXT]];
        
        NSSortDescriptor *orderByDescriptor_1 = [[NSSortDescriptor alloc] initWithKey:@"created_on" ascending:NO];
        
        NSArray *sortDescriptors = [[NSArray alloc] initWithObjects:orderByDescriptor_1, nil];
        [fetchRequest setSortDescriptors:sortDescriptors];
        
        if (editLocn != nil) { // Exclude the object we trying to edit and check for distance range for other object
            fetchRequest.predicate = [NSPredicate predicateWithFormat:@"self != %@", editLocn];
        }
        
        // Execute the fetch
        NSError *error = nil;
        NSArray *fetchResults = [APP_DELEGATE_MGDOBJCNTXT executeFetchRequest:fetchRequest error:&error];
        
        for (Location *loc in fetchResults) {
            CLLocationCoordinate2D existCoor = CLLocationCoordinate2DMake([loc.latitude doubleValue], [loc.longitude doubleValue]);
            float distanceMeters = [AppUtilities distanceBetweenA:sourceCoord B:existCoor];

            if ( fabsf(distanceMeters) <= DEFAULT_ALLOWED_DISTANCE ) { // Newly added location is close the existing location & cannot be stored
                
                // Bug Fix: Also check that the location to which it is in close proximity is an actively monitored locn
                 bool isMonitoredAlso = [[AppSharedData getInstance].appLocationMgr regionIsMonitored:loc.region_identifier];
                if ( isMonitoredAlso) {
                    retVal = YES; // Existing region is actively monitored and close to newly creating location.
                }
                else {
                    retVal = NO; // Existing region is close by but NOT actively created so allowing creation of new region which will be close to another stored location.
                }
                break;
            }
        }
    }
    return retVal;
}

// If Nil get all unclosed entries
// else get unclosed entries of that region only
+ (NSArray *) getUnFinishedRouteLogs:(Route *) route {
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    [fetchRequest setEntity:[NSEntityDescription entityForName:@"RouteLog" inManagedObjectContext:APP_DELEGATE_MGDOBJCNTXT]];
    
    NSSortDescriptor *orderByDescriptor_1 = [[NSSortDescriptor alloc] initWithKey:@"created_on" ascending:NO];
    
    NSArray *sortDescriptors = [[NSArray alloc] initWithObjects:orderByDescriptor_1, nil];
    [fetchRequest setSortDescriptors:sortDescriptors];
    
    if (route == nil ) {
        fetchRequest.predicate = [NSPredicate predicateWithFormat:@"reached_at == nil && left_at != nil"];
     }
    else {
        // Entries matching to this route, requiring reaching tiem to fill in and are already having left_at time.
        fetchRequest.predicate = [NSPredicate predicateWithFormat:@"route == %@ && reached_at == nil && left_at != nil", route];
    }
    // Execute the fetch
    NSError *error = nil;
    NSArray *fetchResults = [APP_DELEGATE_MGDOBJCNTXT executeFetchRequest:fetchRequest error:&error];
    
    return fetchResults;

}


// TRIPS
+(NSArray *) getOpenTrips {
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    [fetchRequest setEntity:[NSEntityDescription entityForName:@"Trip" inManagedObjectContext:APP_DELEGATE_MGDOBJCNTXT]];
    
    NSSortDescriptor *orderByDescriptor_1 = [[NSSortDescriptor alloc] initWithKey:@"created_on" ascending:NO];
    
    NSArray *sortDescriptors = [[NSArray alloc] initWithObjects:orderByDescriptor_1, nil];
    [fetchRequest setSortDescriptors:sortDescriptors];
    
    fetchRequest.predicate = [NSPredicate predicateWithFormat:@"finished == 0"];
    
    // Execute the fetch
    NSError *error = nil;
    NSArray *fetchResults = [APP_DELEGATE_MGDOBJCNTXT executeFetchRequest:fetchRequest error:&error];
    
    NSLog(@"CoreDateOperations: OpenTrips Found: %i", (int)[fetchResults count]);
    return fetchResults;
}

/*
 * Find if there is any trip existing same or later than the input date
 */
+(bool) tripExistForDateLaterThan:(NSDate*) startOrEndDate {
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    [fetchRequest setEntity:[NSEntityDescription entityForName:@"Trip" inManagedObjectContext:APP_DELEGATE_MGDOBJCNTXT]];
    
    
    fetchRequest.predicate = [NSPredicate predicateWithFormat:@"from_at >= %@ || to_at >= %@ || created_on >= %@", startOrEndDate, startOrEndDate, startOrEndDate];
    
    // Execute the fetch
    NSError *error = nil;
     NSUInteger count = [APP_DELEGATE_MGDOBJCNTXT countForFetchRequest:fetchRequest error:&error];
   
    
    NSLog(@"CoreDateOperations: tripExistForDateLaterThan Found: %i", (int)count);
    
    if (count > 0) {
        return YES;
    }
    else
        return NO;
}


+(NSArray *) getItemsGreaterThanSortOrder: (int) sortOrder EntityName:(NSString *) entityName  {
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    [fetchRequest setEntity:[NSEntityDescription entityForName:entityName inManagedObjectContext:APP_DELEGATE_MGDOBJCNTXT]];
    
    NSSortDescriptor *orderByDescriptor_1 = [[NSSortDescriptor alloc] initWithKey:@"sort_order" ascending:YES];
    
    NSArray *sortDescriptors = [[NSArray alloc] initWithObjects:orderByDescriptor_1, nil];
    [fetchRequest setSortDescriptors:sortDescriptors];
    
    fetchRequest.predicate = [NSPredicate predicateWithFormat:@"sort_order > %i ", sortOrder];
    
    // Execute the fetch
    NSError *error = nil;
    NSArray *fetchResults = [APP_DELEGATE_MGDOBJCNTXT executeFetchRequest:fetchRequest error:&error];
    
    return fetchResults;
}

+ (int) getMaxSortOrder: (NSString *) entityName
{
    int retVal = 0;
    
    // create the fetch request to get all Employees matching the IDs
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    
    NSManagedObjectContext *context = APP_DELEGATE_MGDOBJCNTXT;
    
    [fetchRequest setEntity:
     [NSEntityDescription entityForName:entityName inManagedObjectContext:context]];
    
    /*
     fetchRequest.predicate = [NSPredicate predicateWithFormat:@"sort_order==max(sort_order)"];
     fetchRequest.sortDescriptors = [NSArray array];
     */
    NSSortDescriptor *orderByDescriptor = [[NSSortDescriptor alloc] initWithKey:@"sort_order" ascending:NO];
    
    NSArray *sortDescriptors = [[NSArray alloc] initWithObjects:orderByDescriptor, nil];
    [fetchRequest setSortDescriptors:sortDescriptors];
    
    [fetchRequest setFetchLimit:1];
        
    // Execute the fetch
    NSError *error = nil;
    NSArray *fetchResults = [context executeFetchRequest:fetchRequest error:&error];
    
    NSManagedObject *item = [fetchResults objectAtIndex:0];
    
    if (fetchRequest !=nil && [fetchResults count] > 0) {
        if ( [entityName isEqualToString:@"Location"] ) {
            Location *le = (Location *) item;
            retVal = [le.sort_order intValue];
        }
        else if ( [entityName isEqualToString:@"LocationLog"] ) {
            LocationLog *le = (LocationLog *) item;
            retVal = [le.sort_order intValue];
        }
        else if ( [entityName isEqualToString:@"Route"] ) {
            Route *le = (Route *) item;
            retVal = [le.sort_order intValue];
        }
        else {
            NSLog(@"Throw Exception");
            
        }
    }
    else {
        retVal = retVal; //DEFAULT VALUE
    }
    
    //    NSLog(@"getMaxSortOrder: Max Value: %i", retVal );
    return retVal;
}

+ (NSArray *) locationBetweenSortOrder:(int)sortOrderA AndSortOrder:(int)sortOrderB {
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    [fetchRequest setEntity:[NSEntityDescription entityForName:@"Location" inManagedObjectContext:APP_DELEGATE_MGDOBJCNTXT]];
    
    NSSortDescriptor *orderByDescriptor_1 = [[NSSortDescriptor alloc] initWithKey:@"sort_order" ascending:YES];
    
    NSArray *sortDescriptors = [[NSArray alloc] initWithObjects:orderByDescriptor_1, nil];
    [fetchRequest setSortDescriptors:sortDescriptors];
    
    fetchRequest.predicate = [NSPredicate predicateWithFormat:@"sort_order >= %i && sort_order <= %i", sortOrderA, sortOrderB];
    
    // Execute the fetch
    NSError *error = nil;
    NSArray *fetchResults = [APP_DELEGATE_MGDOBJCNTXT executeFetchRequest:fetchRequest error:&error];
    
    return fetchResults;
}

#pragma mark - Graph Data API

/* http://iphonedevelopment.blogspot.com/2010/11/nsexpression.html
 * http://stackoverflow.com/questions/4865686/group-by-equivalent-for-core-data
 * http://mattconnolly.wordpress.com/2012/06/21/ios-core-data-group-by-and-count-results/
 * 
 */
+ (NSMutableDictionary *) getGraphSummary:(int) selSegIdx {
    
    NSFetchRequest* fetch = [NSFetchRequest fetchRequestWithEntityName:@"LocationLog"];
    
    NSExpression *exitTime = [NSExpression expressionForKeyPath:@"did_exit"];
    NSExpression *enterTime = [NSExpression expressionForKeyPath:@"did_enter"];
    NSExpression *timeDiff = [NSExpression expressionForFunction:@"from:subtract:" arguments:@[exitTime, enterTime]];
    
    NSExpressionDescription *expDesc = [[NSExpressionDescription alloc] init];
    [expDesc setName:@"count"];
    [expDesc setExpression:timeDiff];
    [expDesc setExpressionResultType:NSDoubleAttributeType];
    /*
     NSExpression *keyPathExpression = [NSExpression expressionForKeyPath: @"created_on"]; // Does not really matter
     NSExpression *countExpression = [NSExpression expressionForFunction: @"count:"
     arguments: [NSArray arrayWithObject:keyPathExpression]];
     NSExpressionDescription *expressionDescription = [[NSExpressionDescription alloc] init];
     [expressionDescription setName: @"count"];
     [expressionDescription setExpression: countExpression];
     [expressionDescription setExpressionResultType: NSInteger32AttributeType];
     */
    
    
    [fetch setPropertiesToFetch:[NSArray arrayWithObjects: @"location.name", expDesc, nil]];
//    [fetch setPropertiesToGroupBy:[NSArray arrayWithObject:@"location.name"]]; // Removed Grouping and manually doing a summation now else it was only giving the last record
    [fetch setResultType:NSDictionaryResultType];
    
    // To do manual summation doing sorting
    NSSortDescriptor *orderByDescriptor_1 = [[NSSortDescriptor alloc] initWithKey:@"location.name" ascending:NO];
    NSArray *sortDescriptors = [[NSArray alloc] initWithObjects:orderByDescriptor_1, nil];
    [fetch setSortDescriptors:sortDescriptors];
    
    switch( selSegIdx) {
        case 0:
        {
            //FLURRY:
            [Flurry logEvent:@"Tab Summary: Today"];
            
            NSDate *today    = [AppUtilities getDatePartOnlyFor:[NSDate date]];
            NSDate *tomorrow = [AppUtilities getStartOfNextDayDate:today];
            
            NSPredicate *filterPredicate = [NSPredicate predicateWithFormat:@" (did_enter != nil && did_exit != nil && created_on >= %@ && created_on < %@)", today, tomorrow];
            
            [fetch setPredicate:filterPredicate];
            
            NSLog(@"Filter: Today: StartDate: %@, EndDate: %@", today, tomorrow);
            break;
        }
        case 1:
        {
            //FLURRY:
            [Flurry logEvent:@"Tab Summary: Week"];
            
            NSDate *today     = [AppUtilities getDatePartOnlyFor:[NSDate date]];
            NSDate *weekStart = [AppUtilities getStartOfWeekDate:today];
            
            
            NSPredicate *filterPredicate = [NSPredicate predicateWithFormat:@" (did_enter != nil && did_exit != nil && created_on >= %@ && created_on < %@)", weekStart, today];
            //NSPredicate *new1 = [NSPredicate predicateWithFormat:@"(done == 0)"];
            [fetch setPredicate:filterPredicate];
            
            NSLog(@"Filter: Week: StartDate: %@, EndDate: %@", weekStart, today);
            break;
        }
        case 2:
        {
            //FLURRY:
            [Flurry logEvent:@"Tab Summary: Month"];
            
            NSDate *today        = [AppUtilities getDatePartOnlyFor:[NSDate date]];
            NSDate *startOfMonth = [AppUtilities getStartOfMonthDate:today];
            
            NSPredicate *filterPredicate = [NSPredicate predicateWithFormat:@" (did_enter != nil && did_exit != nil && created_on >= %@ && created_on < %@)", startOfMonth, today];
            
            [fetch setPredicate:filterPredicate];
            
            NSLog(@"Filter: Month: start:%@ Today:%@", startOfMonth, today);
            break;
        }
        case 3:
        default:
        {
            //FLURRY:
            [Flurry logEvent:@"Tab Summary: Overall"];
            NSLog(@"Filter: No Due Set");
            
            break;
        }
    }
    
    
    NSError* error = nil;
    NSArray *results = [APP_DELEGATE_MGDOBJCNTXT executeFetchRequest:fetch
                                                               error:&error];
    
    NSMutableDictionary *retVal = [[NSMutableDictionary alloc] init];
    for (NSDictionary *a in results) {
        NSString *key = [a objectForKey:@"location.name"];
        NSNumber *val = [a objectForKey:@"count"];
        
        if (key == nil) {
            continue; // during testing found some cases where location was not mapped.
        }
        
        if (val == nil) {
            val = [NSNumber numberWithDouble:0.0];
        }
        else {
            val = [NSNumber numberWithDouble:[val doubleValue] / (60*60)];
        }
        
        // Start doing the consolidation
        NSNumber *oldValue = [retVal objectForKey:key];
        if (oldValue == nil)
            [retVal setObject:val forKey:key];
        else {
            NSNumber *newVal = [NSNumber numberWithDouble:([oldValue doubleValue] + [val doubleValue])];
            [retVal setObject:newVal forKey:key];
            oldValue = newVal;
        }
        NSLog(@"Graph Data Accumulating Key: %@ Value:%f NewValue: %f", key, [val doubleValue], [oldValue doubleValue]);
    }
    NSLog(@"Graph Summary: %@", retVal);
    return retVal;
    
}

+ (NSMutableDictionary *) getGraphSummary_count:(int) selSegIdx {
    
    NSFetchRequest* fetch = [NSFetchRequest fetchRequestWithEntityName:@"LocationLog"];
    
    NSExpression *keyPathExpression = [NSExpression expressionForKeyPath: @"created_on"]; // Does not really matter
    NSExpression *countExpression = [NSExpression expressionForFunction: @"count:"
                                                              arguments: [NSArray arrayWithObject:keyPathExpression]];
    NSExpressionDescription *expressionDescription = [[NSExpressionDescription alloc] init];
    [expressionDescription setName: @"count"];
    [expressionDescription setExpression: countExpression];
    [expressionDescription setExpressionResultType: NSInteger32AttributeType];
    
    
    [fetch setPropertiesToFetch:[NSArray arrayWithObjects: @"location.name", expressionDescription, nil]];
    [fetch setPropertiesToGroupBy:[NSArray arrayWithObject:@"location.name"]];
    [fetch setResultType:NSDictionaryResultType];
    
    switch( selSegIdx) {
        case 0:
        {
            //FLURRY:
            [Flurry logEvent:@"Tab Summary: Today"];
            
            NSDate *today    = [AppUtilities getDatePartOnlyFor:[NSDate date]];
            NSDate *tomorrow = [AppUtilities getStartOfNextDayDate:today];
            
            NSPredicate *filterPredicate = [NSPredicate predicateWithFormat:@" (created_on >= %@ && created_on < %@)", today, tomorrow];
            
            [fetch setPredicate:filterPredicate];
            
            NSLog(@"Filter: Today: StartDate: %@, EndDate: %@", today, tomorrow);
            break;
        }
        case 1:
        {
            //FLURRY:
            [Flurry logEvent:@"Tab Summary: Week"];
            
            NSDate *today     = [AppUtilities getDatePartOnlyFor:[NSDate date]];
            NSDate *weekStart = [AppUtilities getStartOfWeekDate:today];
            
            NSPredicate *filterPredicate = [NSPredicate predicateWithFormat:@" (created_on >= %@ && created_on < %@)", weekStart, today];
            //NSPredicate *new1 = [NSPredicate predicateWithFormat:@"(done == 0)"];
              [fetch setPredicate:filterPredicate];
            
            NSLog(@"Filter: Week: StartDate: %@, EndDate: %@", weekStart, today);
            break;
        }
        case 2:
        {
            //FLURRY:
            [Flurry logEvent:@"Tab Summary: Month"];
            
            NSDate *today        = [AppUtilities getDatePartOnlyFor:[NSDate date]];
            NSDate *startOfMonth = [AppUtilities getStartOfMonthDate:today];
            
            NSPredicate *filterPredicate = [NSPredicate predicateWithFormat:@" (created_on >= %@ && created_on < %@)", startOfMonth, today];
            
            [fetch setPredicate:filterPredicate];
            
            NSLog(@"Filter: Month: start:%@ Today:%@", startOfMonth, today);
            break;
        }
        case 3:
        default:
        {
            //FLURRY:
            [Flurry logEvent:@"Tab Summary: Overall"];
            NSLog(@"Filter: No Due Set");
        
            break;
        }
    }

    
    NSError* error = nil;
    NSArray *results = [APP_DELEGATE_MGDOBJCNTXT executeFetchRequest:fetch
                                                             error:&error];
    
    NSMutableDictionary *retVal = [[NSMutableDictionary alloc] init];
    for (NSDictionary *a in results) {
        [retVal setObject:[a objectForKey:@"count"] forKey:[a objectForKey:@"location.name"]];
    }
    return retVal;
    
}
+(NSNumber *)aggregateOperation:(NSString *)function onAttribute:(NSString *)attributeName withPredicate:(NSPredicate *)predicate inManagedObjectContext:(NSManagedObjectContext *)context
{
    NSExpression *ex = [NSExpression expressionForFunction:function
                                                 arguments:[NSArray arrayWithObject:[NSExpression expressionForKeyPath:attributeName]]];
    
    NSExpressionDescription *ed = [[NSExpressionDescription alloc] init];
    [ed setName:@"result"];
    [ed setExpression:ex];
    [ed setExpressionResultType:NSInteger64AttributeType];
    
    NSArray *properties = [NSArray arrayWithObject:ed];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setPropertiesToFetch:properties];
    [request setResultType:NSDictionaryResultType];
    
    if (predicate != nil)
        [request setPredicate:predicate];
    
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"LocationLog"
                                              inManagedObjectContext:context];
    [request setEntity:entity];
    
    
    NSArray *results = [context executeFetchRequest:request error:nil];
    NSDictionary *resultsDictionary = [results objectAtIndex:0];
    NSNumber *resultValue = [resultsDictionary objectForKey:@"result"];
    
    return resultValue;
    
}

// Route Graph Summary API

// TODO: Use better core data grouping technics to make it better performing and more scalable code

/* For Trip table,
 * Sort Data by from_locname & to_locname to enable manual sum
 * Filter data based on user selection on createdOn and avoid entries which are not having start & end time
 * Manually concatenate 
 * Note: Could not use transient property here */
+ (NSMutableDictionary *) getTrip_GraphSummary:(int) selSegIdx {
    
    NSFetchRequest* fetch = [NSFetchRequest fetchRequestWithEntityName:@"Trip"];
    
    NSExpression *startTime = [NSExpression expressionForKeyPath:@"from_at"];
    NSExpression *endTime   = [NSExpression expressionForKeyPath:@"to_at"];
    NSExpression *timeDiff = [NSExpression expressionForFunction:@"from:subtract:" arguments:@[endTime, startTime]];
    
    NSExpressionDescription *expDesc = [[NSExpressionDescription alloc] init];
    [expDesc setName:@"count"];
    [expDesc setExpression:timeDiff];
    [expDesc setExpressionResultType:NSDoubleAttributeType];
    /*
     NSExpression *keyPathExpression = [NSExpression expressionForKeyPath: @"created_on"]; // Does not really matter
     NSExpression *countExpression = [NSExpression expressionForFunction: @"count:"
     arguments: [NSArray arrayWithObject:keyPathExpression]];
     NSExpressionDescription *expressionDescription = [[NSExpressionDescription alloc] init];
     [expressionDescription setName: @"count"];
     [expressionDescription setExpression: countExpression];
     [expressionDescription setExpressionResultType: NSInteger32AttributeType];
     */
    
    
    [fetch setPropertiesToFetch:[NSArray arrayWithObjects: @"from_locname", @"to_locname", expDesc, nil]];
    //    [fetch setPropertiesToGroupBy:[NSArray arrayWithObject:@"location.name"]]; // Removed Grouping and manually doing a summation now else it was only giving the last record
    [fetch setResultType:NSDictionaryResultType];
    
    // To do manual summation doing sorting
    NSSortDescriptor *orderByDescriptor_1 = [[NSSortDescriptor alloc] initWithKey:@"from_locname" ascending:NO];
    NSSortDescriptor *orderByDescriptor_2 = [[NSSortDescriptor alloc] initWithKey:@"to_locname" ascending:NO];
    NSArray *sortDescriptors = [[NSArray alloc] initWithObjects:orderByDescriptor_1, orderByDescriptor_2, nil];
    [fetch setSortDescriptors:sortDescriptors];
    
   
    switch( selSegIdx) {
        case 0:
        {
            //FLURRY:
            [Flurry logEvent:@"Route Tab Summary: Today"];
            
            NSDate *today = [AppUtilities getDatePartOnlyFor:[NSDate date]];
            NSDate *tomorrow = [AppUtilities getStartOfNextDayDate:today];
            
            NSPredicate *filterPredicate = [NSPredicate predicateWithFormat:@" (fromLocation != nil && toLocation != nil && created_on >= %@ && created_on < %@)", today, tomorrow];
            
            [fetch setPredicate:filterPredicate];
            
            NSLog(@"Filter: Today: StartDate: %@, EndDate: %@", today, tomorrow);
            break;
        }
        case 1:
        {
            //FLURRY:
            [Flurry logEvent:@"Route Tab Summary: Week"];
            
            NSDate *today = [AppUtilities getDatePartOnlyFor:[NSDate date]];
            NSDate *weekStart = [AppUtilities getStartOfWeekDate:today];
            
            NSPredicate *filterPredicate = [NSPredicate predicateWithFormat:@" (fromLocation != nil && toLocation != nil && created_on >= %@ && created_on < %@)", weekStart, today];
            //NSPredicate *new1 = [NSPredicate predicateWithFormat:@"(done == 0)"];
            [fetch setPredicate:filterPredicate];
            
            NSLog(@"Filter: Week: StartDate: %@, EndDate: %@", weekStart, today);
            break;
        }
        case 2:
        {
            //FLURRY:
            [Flurry logEvent:@"Route Tab Summary: Month"];
            
            NSDate *today        = [AppUtilities getDatePartOnlyFor:[NSDate date]];
            NSDate *startOfMonth = [AppUtilities getStartOfMonthDate:today];
            
            NSPredicate *filterPredicate = [NSPredicate predicateWithFormat:@" (fromLocation != nil && toLocation != nil && created_on >= %@ && created_on < %@)", startOfMonth, today];
            
            [fetch setPredicate:filterPredicate];
            
            NSLog(@"Filter: Month: start:%@ Today:%@", startOfMonth, today);
            break;
        }
        case 3:
        default:
        {
            //FLURRY:
            [Flurry logEvent:@"Route Tab Summary: Overall"];
            
            // To remove teh entries forcelly closed adding the filter conditions for overall case as well.
            NSPredicate *filterPredicate = [NSPredicate predicateWithFormat:@" (fromLocation != nil && toLocation != nil )"];
            
            [fetch setPredicate:filterPredicate];

            NSLog(@"Filter: No Due Set");
            
            break;
        }
    }
    
     NSError* error = nil;
    @try {
        NSArray *results = [APP_DELEGATE_MGDOBJCNTXT executeFetchRequest:fetch
                                                                   error:&error];
        NSMutableDictionary *retVal = [[NSMutableDictionary alloc] init];
        for (NSDictionary *a in results) {
            NSString *key = [[a objectForKey:@"from_locname"] stringByAppendingFormat:@" - %@",[a objectForKey:@"to_locname"]];
            NSNumber *val = [a objectForKey:@"count"];
            
            if (key == nil) {
                continue; // during testing found some cases where location was not mapped.
            }
            
            if (val == nil) {
                val = [NSNumber numberWithDouble:0.0];
            }
            else {
                val = [NSNumber numberWithDouble:[val doubleValue] / (60*60)];
            }
            
            // Start doing the consolidation
            NSNumber *oldValue = [retVal objectForKey:key];
            if (oldValue == nil)
                [retVal setObject:val forKey:key];
            else {
                NSNumber *newVal = [NSNumber numberWithDouble:([oldValue doubleValue] + [val doubleValue])];
                [retVal setObject:newVal forKey:key];
                oldValue = newVal;
            }
            NSLog(@"Graph Data Accumulating Key: %@ Value:%f NewValue: %f", key, [val doubleValue], [oldValue doubleValue]);
        }
        
        NSLog(@"Graph Summary: %@", retVal);
        
        return retVal;
    }
    @catch (NSException *exception) {
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);

    }
    @finally {
        
    }
}

+ (NSMutableDictionary *) getTrip_GraphSummary_Average:(int)selSegIdx {
    NSFetchRequest* fetch = [NSFetchRequest fetchRequestWithEntityName:@"Trip"];
    
    NSExpression *startTime = [NSExpression expressionForKeyPath:@"from_at"];
    NSExpression *endTime   = [NSExpression expressionForKeyPath:@"to_at"];
    NSExpression *timeDiff = [NSExpression expressionForFunction:@"from:subtract:" arguments:@[endTime, startTime]];
    
    NSExpressionDescription *expDesc = [[NSExpressionDescription alloc] init];
    [expDesc setName:@"tripDuration"];
    [expDesc setExpression:timeDiff];
    [expDesc setExpressionResultType:NSDoubleAttributeType];
    
    
    NSPredicate *filterPredicate = [NSPredicate predicateWithFormat:@" (fromLocation != nil && toLocation != nil != nil"];
    [fetch setPredicate:filterPredicate];
    
    
    [fetch setPropertiesToFetch:[NSArray arrayWithObjects: @"from_locname", @"to_locname", expDesc, nil]];
    //    [fetch setPropertiesToGroupBy:[NSArray arrayWithObject:@"location.name"]]; // Removed Grouping and manually doing a summation now else it was only giving the last record
    [fetch setResultType:NSDictionaryResultType];
    
    // To do manual summation doing sorting
    NSSortDescriptor *orderByDescriptor_1 = [[NSSortDescriptor alloc] initWithKey:@"from_locname" ascending:NO];
    NSSortDescriptor *orderByDescriptor_2 = [[NSSortDescriptor alloc] initWithKey:@"to_locname" ascending:NO];
    NSArray *sortDescriptors = [[NSArray alloc] initWithObjects:orderByDescriptor_1, orderByDescriptor_2, nil];
    [fetch setSortDescriptors:sortDescriptors];
    
    
    NSError* error = nil;
    @try {
        NSArray *results = [APP_DELEGATE_MGDOBJCNTXT executeFetchRequest:fetch
                                                                   error:&error];
        NSMutableDictionary *keySum = [[NSMutableDictionary alloc] init];
        NSMutableDictionary *keyCount = [[NSMutableDictionary alloc] init];
        for (NSDictionary *a in results) {
            NSString *key = [[a objectForKey:@"from_locname"] stringByAppendingFormat:@" - %@",[a objectForKey:@"to_locname"]];
            NSNumber *val = [a objectForKey:@"timeDuration"];
            
            if (key == nil) {
                continue; // during testing found some cases where location was not mapped.
            }
            
            if (val == nil) {
                val = [NSNumber numberWithDouble:0.0];
            }
            else {
                val = [NSNumber numberWithDouble:[val doubleValue] / (60*60)];
            }
            
            // Start doing the consolidation
            NSNumber *oldValue = [keySum objectForKey:key];
            if (oldValue == nil) {
                [keySum setObject:val forKey:key];
            }
            else {
                NSNumber *newVal = [NSNumber numberWithDouble:([oldValue doubleValue] + [val doubleValue])];
                [keySum setObject:newVal forKey:key];
                oldValue = newVal;
            }
            // Also log the counts
            NSNumber *oldCountValue = [keyCount objectForKey:key];
            if (oldCountValue == nil) {
                [keyCount setObject:[NSNumber numberWithInt:0] forKey:key];
            }
            else {
                NSNumber *newVal = [NSNumber numberWithInt:([oldValue intValue] + 1)];
                [keyCount setObject:newVal forKey:key];
                oldCountValue = newVal;
            }
            NSLog(@"Graph Data Accumulating Key: %@ Value:%f NewValue: %f NewCountVal:%i", key, [val doubleValue], [oldValue doubleValue], [oldCountValue intValue]);
        }
        NSLog(@"Graph Summary: %@", keySum);
   
    }
    @catch (NSException *exception) {
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        
    }
    @finally {
        
    }

        return nil;
}

+ (NSMutableDictionary *) getRoute_GraphSummary_count:(int) selSegIdx {
    
    NSFetchRequest* fetch = [NSFetchRequest fetchRequestWithEntityName:@"Trip"];
    
    NSExpression *keyPathExpression = [NSExpression expressionForKeyPath: @"created_on"]; // Does not really matter
    NSExpression *countExpression = [NSExpression expressionForFunction: @"count:"
                                                              arguments: [NSArray arrayWithObject:keyPathExpression]];
    NSExpressionDescription *expressionDescription = [[NSExpressionDescription alloc] init];
    [expressionDescription setName: @"count"];
    [expressionDescription setExpression: countExpression];
    [expressionDescription setExpressionResultType: NSInteger32AttributeType];
    
    
    [fetch setPropertiesToFetch:[NSArray arrayWithObjects: @"tripName", expressionDescription, nil]];
    [fetch setPropertiesToGroupBy:[NSArray arrayWithObject:@"tripName"]];
    [fetch setResultType:NSDictionaryResultType];
    
    
    switch( selSegIdx) {
        case 0:
        {
            //FLURRY:
            [Flurry logEvent:@"Tab Summary: Today"];
            NSDate *today    = [AppUtilities getDatePartOnlyFor:[NSDate date]];
            NSDate *tomorrow = [AppUtilities getStartOfNextDayDate:today];
            
            NSPredicate *filterPredicate = [NSPredicate predicateWithFormat:@" (created_on >= %@ && created_on < %@)", today, tomorrow];
            
            [fetch setPredicate:filterPredicate];
            
            NSLog(@"Filter: Today: StartDate: %@, EndDate: %@", today, tomorrow);
            break;
        }
        case 1:
        {
            //FLURRY:
            [Flurry logEvent:@"Tab Summary: Week"];
            
            NSDate *today     = [AppUtilities getDatePartOnlyFor:[NSDate date]];
            NSDate *weekStart = [AppUtilities getStartOfWeekDate:today];
            
            NSPredicate *filterPredicate = [NSPredicate predicateWithFormat:@" (created_on >= %@ && created_on < %@)", weekStart, today];
            [fetch setPredicate:filterPredicate];
            
            NSLog(@"Filter: Week: StartDate: %@, EndDate: %@", weekStart, today);
            break;
        }
        case 2:
        {
            //FLURRY:
            [Flurry logEvent:@"Tab Summary: Month"];
            
            NSDate *today        = [AppUtilities getDatePartOnlyFor:[NSDate date]];
            NSDate *startOfMonth = [AppUtilities getStartOfMonthDate:today];
            
            NSPredicate *filterPredicate = [NSPredicate predicateWithFormat:@" (created_on >= %@ && created_on < %@)", startOfMonth, today];
            
            [fetch setPredicate:filterPredicate];
            
            NSLog(@"Filter: Month: start:%@ Today:%@", startOfMonth, today);
            break;
        }
        case 3:
        default:
        {
            //FLURRY:
            [Flurry logEvent:@"Tab Summary: Overall"];
            NSLog(@"Filter: No Due Set");
            
            break;
        }
    }
    
    
    NSError* error = nil;
    NSArray *results = [APP_DELEGATE_MGDOBJCNTXT executeFetchRequest:fetch
                                                               error:&error];
    
    NSMutableDictionary *retVal = [[NSMutableDictionary alloc] init];
    for (NSDictionary *a in results) {
        [retVal setObject:[a objectForKey:@"count"] forKey:[a objectForKey:@"tripName"]];
    }
    return retVal;
    
}

+ (NSFetchedResultsController *) genFetchReqCtrlFromSelConfig: (NSManagedObjectContext *) mgdObjContext {
    
    NSFetchedResultsController *nsFetchRqstCtrlr = nil;
    
    // Create and configure a fetch request with the Book entity.
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Location" inManagedObjectContext:mgdObjContext];
    [fetchRequest setEntity:entity];
    
    NSArray *sortDescriptors = nil;
    NSString *collapseOnSectionName = @"name";
    NSString *collapseOnOrderByField = @"name";
    BOOL collapseOnOrderDirection = YES;
    
    
    // Create the sort descriptors array.
    NSSortDescriptor *collapseOnDescriptor = [[NSSortDescriptor alloc]initWithKey:collapseOnOrderByField ascending:collapseOnOrderDirection];
    
    
    sortDescriptors = [[NSArray alloc] initWithObjects:
                       collapseOnDescriptor,
                       nil];
    [fetchRequest setSortDescriptors:sortDescriptors];
    
    
//    NSDate *today = [AppUtilities getDatePartOnlyFor:[NSDate date]];
    
    
    // TODO: Use cacheName
    // Create and initialize the fetch results controller.
    nsFetchRqstCtrlr = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
                                                           managedObjectContext:mgdObjContext
                                                             sectionNameKeyPath:collapseOnSectionName
                                                                      cacheName:nil];
    
    
    NSLog(@"Section: %lu %lu", (unsigned long)[[nsFetchRqstCtrlr sections] count], (unsigned long)[[nsFetchRqstCtrlr fetchedObjects] count]);
                           
//    id <NSFetchedResultsSectionInfo> sectionInfo = [[ [self nsFetchRqstCtrlr] sections] objectAtIndex:section];
    
    return nsFetchRqstCtrlr;
}

#pragma mark - SaveContext
+ (void) saveContext {
    // Delete the managed object.
    NSManagedObjectContext *context = APP_DELEGATE_MGDOBJCNTXT;
    NSError *error;
    if (![context save:&error]) {
        /*
         Replace this implementation with code to handle the error appropriately.
         
         abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
         */
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
}

@end

