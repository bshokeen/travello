//
//  HelpBubble.m
//  Travello
//
//  Created by Vivek Sehrawat on 10/31/13.
//  Copyright (c) 2013 Dasheri. All rights reserved.
//trying to see the changes

#import "HelpBubble.h"

static const CGFloat CSToastShadowOpacity       = 0.8;
static const CGFloat CSToastShadowRadius        = 6.0;
static const CGSize  CSToastShadowOffset        = { 4.0, 4.0 };


#define   DEGREES_TO_RADIANS(degrees)  ((3.14159265359 * degrees)/ 180)


@implementation HelpBubble
@synthesize detailStr;
@synthesize pointType;


- (id)initWithFrame:(CGRect)frame strTitle:(NSString *)title pointDirection:(TrianglePointType)pointingDirection
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        self.pointType=pointingDirection;
        self.detailStr=[title copy];
    }
    return self;
}

// Only override drawRect: if you perform custom drawing.
-(void)drawRect:(CGRect)rect {
    
    CGPoint firstTriPt;
    CGPoint secondTriPt;
    CGPoint thirdTriPt;
    CGFloat midPoint=(rect.size.width/2);
    CGFloat leftPoint=(rect.size.width-30.0);
    CGFloat rightPoint=(rect.origin.x+30.0);
    CGFloat downPt=(rect.size.height);
    CGFloat triangleFirstSideLength=10.0; // this is the length of triangle sides
    CGFloat triangleheight=10.0;   // this will determine the height of triangle and margin
    
    
    switch (pointType) {
        case UpperLeftPoint:
            firstTriPt=CGPointMake(rightPoint, 0);
            Upside=TRUE;
            break;
            
        case UpperMiddlePoint:
            firstTriPt=CGPointMake(midPoint, 0.0);
            Upside=TRUE;
            break;
            
        case UpperRightPoint:
            firstTriPt=CGPointMake(leftPoint, 0.0);
            Upside=TRUE;
            break;
            
        case DownLeftPoint:
            firstTriPt=CGPointMake(rightPoint, downPt);
            Upside=FALSE;
            break;
            
        case DownMiddlePoint:
            firstTriPt=CGPointMake(midPoint,downPt);
            Upside=FALSE;
            break;
            
        case DownRightPoint:
            firstTriPt=CGPointMake(leftPoint, downPt);
            Upside=FALSE;
            break;
        case Toast:
            
            
        default:
            break;
    }
    
    if (Upside) {
        secondTriPt=CGPointMake(firstTriPt.x-triangleFirstSideLength, firstTriPt.y+triangleheight);
        thirdTriPt=CGPointMake(firstTriPt.x+triangleFirstSideLength,firstTriPt.y+ triangleheight);
    }
    else{
        secondTriPt=CGPointMake(firstTriPt.x+triangleFirstSideLength, firstTriPt.y-triangleheight);
        thirdTriPt=CGPointMake(firstTriPt.x-triangleFirstSideLength, firstTriPt.y-triangleheight);
    }
    
    CGRect pathRect = CGRectInset(self.bounds, 1, triangleheight);
    UIBezierPath* rounRect = [UIBezierPath bezierPathWithRoundedRect:pathRect byRoundingCorners:UIRectCornerAllCorners cornerRadii:CGSizeMake(5, 5)];
    [[UIColor blackColor] setFill];
    [rounRect fill];
    
    UIBezierPath *triangle = [UIBezierPath bezierPath];
    [triangle moveToPoint:firstTriPt];
    [triangle addLineToPoint:secondTriPt];
    [triangle addLineToPoint:thirdTriPt];
    [triangle closePath];
    [[UIColor blackColor] setFill];
    [triangle fill];
    
    UILabel *lblDesc=[[UILabel alloc]init];
    lblDesc.frame=CGRectMake(rect.origin.x+5, triangleheight, rect.size.width-5, rect.size.height-2*triangleheight);
    lblDesc.text=self.detailStr;
    lblDesc.numberOfLines=0;
    lblDesc.textColor=[UIColor whiteColor];
    lblDesc.textAlignment=NSTextAlignmentCenter;
    lblDesc.font=[UIFont fontWithName:@"HelveticaNeue" size:18];
    lblDesc.lineBreakMode=NSLineBreakByCharWrapping;
    //    lblDesc.backgroundColor=[UIColor greenColor];
    
    [self addSubview:lblDesc];
    
    self.layer.shadowColor = [UIColor blackColor].CGColor;
    self.layer.shadowOpacity = CSToastShadowOpacity;
    self.layer.shadowRadius = CSToastShadowRadius;
    self.layer.shadowOffset = CSToastShadowOffset;
    
}

@end
