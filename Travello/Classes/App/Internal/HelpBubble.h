//
//  HelpBubble.h
//  Travello
//
//  Created by Vivek Sehrawat on 10/31/13.
//  Copyright (c) 2013 Dasheri. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum{
    UpperLeftPoint,
    UpperMiddlePoint,
    UpperRightPoint,
    DownLeftPoint,
    DownMiddlePoint,
    DownRightPoint,
    Toast,
    
}TrianglePointType;


@interface HelpBubble : UIView{
    BOOL Upside;
}
@property(nonatomic,retain)NSString *detailStr;
@property (nonatomic) TrianglePointType pointType;

- (id)initWithFrame:(CGRect)frame strTitle:(NSString *)title pointDirection:(TrianglePointType)pointingDirection;
@end
