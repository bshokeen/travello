//
//  CoreDataOperations.h
//  Travello
//
//  Created by Balbir Shokeen on 2013/08/31.
//  Copyright (c) 2013 Balbir Shokeen. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>
#import "AppDelegate.h"
#import "Location.h"
#import "LocationLog.h"
#import "Route.h"
#import "RouteLog.h"
#import "Trip.h"
#import "TripSummary.h"


@interface CoreDataOperations : NSObject

// Insert basic records with some default meaningful values
+ (void) insertDefaultData;

// Count Items in Location tabel
+ (int) countLocations;


// Core Data Entiry Operations - ADD
+ (LocationLog *) addLocationLogWithDidEnterTime: (NSDate *) didEnterTime WithDidExitTime: (NSDate *)didExitTime WithDidEnterLatitude:(double) didEnterLatitude WithDidEnterLongitude:(double) didEnterLongitude WithDidExitLatitude:(double) didExitLatitude WithDidExitLongitude: (double) didExitLatutide WithLocnIdentifier:(NSString *) identifier WithSortOrder:(int) sortOrder  ManualEntry:(bool)isManualEntry;

+ (Location *) addLocationWithName: (NSString *) name  WithLatitude:(double) latitude WithLongitude:(double) longitude WithAddress: (NSString *) address WithRadius: (double) radius WithFlag:(int) flag WithSortOrder: (int) sortOrder WithStatus: (int) status WithDirty:(int) dirty WithRegionIdentifier:(NSString*)WithRegionIdentifier WithExtra1_Int: (int) extra1int WithExtra2_Str:(NSString *) extra2String;

// Route & RouteLog - ADD
+ (RouteLog *) addRouteLogWithRoute:(Route*)route WithLeftAt:(NSDate *) leftAt WithReachedT:(NSDate *)reachedAt WithLeftAtCoordinate:(NSDecimalNumber *) leftAtCoordinate WithReachedAtCoordinate:(NSDecimalNumber*)reachedAtCoordinate WithCreatedOn:(NSDate *) createdOn WithModifiedOn:(NSDate *)modifiedON;

+ (Route *) addRouteWithFromLocation:(Location*)fromLocation WithToLocation:(Location*)toLocation WithSortOrder:(int)sortOrder WithImagePath:(NSString*)imagePath WithImageType:(int)imageType;

+(TripSummary *) addUpdateTripSummaryWithFromLocation:(Location*)fromLocation ToLocation:(Location*)toLocation FromLocnIdentifier: (NSString*)fromLocnIdentifier ToLocnIdentifer:(NSString*)toLocnIdentifier FromLocnName:(NSString*)fromLocnName ToLocnName:(NSString*)toLocnName RouteName:(NSString*)routeName TimeDuration:(double)timeDuration FromLocnImg:(NSString*)fromLocnImg ToLocnImg:(NSString*)toLocnImg Extra1:(NSString*)extra1 TotalDistance:(double)totalDistance;
+(TripSummary *) findTripSummaryWithFromLocnID:(NSString*)fromLocnID WithToLocnID:(NSString*)toLocnID;
+(BOOL) deleteTripUpdateTripSummaryFor:(Trip*)tripSummary;

// Trip Handling
+ (Trip *) addTrip_IsFinished:(BOOL)finished FromLocn: (Location *)fromLocn ToLocn:(Location*)toLocn FromLocName:(NSString*)fromLocName FromAt:(NSDate*)fromAt FromRegionIdentifier:(NSString*)fromRegionIdentifier ToLocName:(NSString*)toLocName ToAt:(NSDate*)toAt  ToRegionIdentifier:(NSString*)toRegionIdentifier CreatedOn:(NSDate *)createdOn ModifiedOn:(NSDate *)modifiedON;

// Delete
+ (BOOL) deleteItemByEntityName: (NSString *) entityName Item: (NSManagedObject *) item;
+ (int) deleteOldLogs: (int) olderThan;

// Query Objects
+ (Location *) getLocationByIdentifier: (NSString *) regionIdentifier;
+ (LocationLog *) getLatestLocationLogByIdentifier: (NSString *) regionIdentifier;
+ (NSArray *) getItemsGreaterThanSortOrder: (int) sortOrder EntityName:(NSString *) entityName;

// Trips
+(NSArray *) getOpenTrips;
+(bool) tripExistForDateLaterThan:(NSDate*) startOrEndDate;

+ (int) getMaxSortOrder: (NSString *) entityName;
+(bool) ifLocationExists: (NSString *) identifier;
+(bool) ifLocnExistsOrTooCloseForIdentifier: (NSString *) identifier OrCoordinate: (CLLocationCoordinate2D) sourceCoord ExcludeSelf: (Location *) editLocn;

// Route: Logs where reached time is nil
+ (NSArray *) getUnFinishedRouteLogs:(Route *) route;

+ (NSArray *) locationBetweenSortOrder:(int)sortOrderA AndSortOrder:(int)sortOrderB;


+(NSMutableDictionary *) getGraphSummary:(int) selSegIdx;
+ (NSMutableDictionary *) getTrip_GraphSummary:(int) selSegIdx;

+ (void) saveContext;

@end
