//
//  AppLocationMgr.m
//  Travello
//
//  Created by Balbir Shokeen on 2013/09/04.
//  Copyright (c) 2013 Balbir Shokeen. All rights reserved.
//

#import "AppLocationMgr.h"
#import "AppUtilities.h"

@interface AppLocationMgr()

//Add a location manager property to this app delegate
@property (nonatomic, strong) CLLocationManager *locationManager;
@property (nonatomic, strong) NSDate   *lastRegionTimestamp;
@property (nonatomic, strong) NSString *lastDidEnterRegionID;

@end

@implementation AppLocationMgr

@synthesize locationManager = _locationManager, lastRegionTimestamp = _lastDidEnterExitTimestamp;
@synthesize mapItemList = _mapItemList;
@synthesize keyIdentiferValRegion = _keyIdentiferValRegion;

-(id) init {
    self = [super init];
    
    NSLog(@"AppLocationMgr: Location Manager - STARTING");
    // Setup Location Manager for Location Tracking
    // Create location manager with filters set for battery efficiency.
	self.locationManager = [[CLLocationManager alloc] init];
	self.locationManager.delegate = self;

    self.locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters;
    self.locationManager.distanceFilter = kCLDistanceFilterNone;//kCLLocationAccuracyHundredMeters; // Setting it to none to get the update as early as possible. need to verify performance with it.
    // Setting to None, was probably, giving unwanted exit/enter. If near home we setup location, it consistently used to get home in/out events, so setting it to 10 meters to get meaninful exit/enter
    self.locationManager.distanceFilter = kCLLocationAccuracyNearestTenMeters;
	
    
    // Initialize the local storage for the tracked regions
    self.keyIdentiferValRegion = [[NSMutableDictionary alloc] init];
    NSArray *regions = [[self.locationManager monitoredRegions] allObjects];
    
    for (int i = 0; i < [regions count]; i++) {
        CLRegion *region = [regions objectAtIndex:i];
        NSLog(@"AppLocationMgr: Region ID:%@", region.identifier);
        
        [self addRegion:region];
    }
    
    if ( [CLLocationManager isMonitoringAvailableForClass:[CLRegion class]])  {
        NSLog(@"AppLocationMgr: Check Device is Location Aware & Supported - YES.");
    }
    else {
        ULog(@"This device is Not supported! You need a location aware device to use this app.");
    }
    
    
    // Initialize the lastDidEnterExitTimestamp with current time during load
    self.lastRegionTimestamp = [NSDate date];
    
    // Alert user on service status
    [self showAlertToUserOnServiceStatus];
    
    NSLog(@"AppLocationMgr: Regions Loaded:%lu Accurancy:%f DistanceFilter:%f", (unsigned long)[self.keyIdentiferValRegion count], self.locationManager.desiredAccuracy, self.locationManager.distanceFilter);
    return self;
}

-(void) start {
    // Start updating location changes.
	[self.locationManager startUpdatingLocation];
    NSLog(@"AppLocationMgr: Location Manager - START recieving Location Updates");
}

-(void) stop {
    [self.locationManager stopUpdatingLocation];
    NSLog(@"AppLocationMgr: Location Manager - STOP  recieving Location Updates");

}

-(CLLocation *) getCurrentLocation {
     //Make sure location services are enabled before requesting the location
     if([CLLocationManager locationServicesEnabled]) {
         return self.locationManager.location;
     }
     else {
         return nil;
     }
}

- (void)gotoLocation
{
    // start off by default in San Francisco
    MKCoordinateRegion newRegion;
    newRegion.center.latitude = 37.786996;
    newRegion.center.longitude = -122.440100;
    newRegion.span.latitudeDelta = 0.112872;
    newRegion.span.longitudeDelta = 0.109863;
    
//    [self.mkLocationMapView setRegion:newRegion animated:YES];
}

#pragma mark - CLLocationManagerDelegate

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error {
	NSLog(@"AppLocationMgr: didFailWithError: %@", error);
}


- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation {
	NSLog(@"AppLocationMgr: didUpdateToLocation %@ from %@", newLocation, oldLocation);
	
	// Work around a bug in MapKit where user location is not initially zoomed to.
//	if (oldLocation == nil) {
		// Zoom to the current user location.
//		MKCoordinateRegion userLocation = MKCoordinateRegionMakeWithDistance(newLocation.coordinate, 100.0, 150.0);
//		[self.mkLocationMapView setRegion:userLocation animated:YES];
//	}
    
    // remember for later the user's current location
//    self.userLocation = newLocation.coordinate;
    
    //	[manager stopUpdatingLocation]; // we only want one update
    
    //    manager.delegate = nil;         // we might be called again here, even though we
    // called "stopUpdatingLocation", remove us as the delegate to be sure
}


- (void)locationManager:(CLLocationManager *)manager didEnterRegion:(CLRegion *)region  {
	NSString *event = [NSString stringWithFormat:@"didEnterRegion %@ at %@\n", region.identifier, [NSDate date]];
    
	NSLog(@"AppLocationMgr: didEnterRegion: regionIdentifier: %@", region.identifier);
    
	[self updateWithEvent:event Type:1 Identifier:region.identifier];
    
}


- (void)locationManager:(CLLocationManager *)manager didExitRegion:(CLRegion *)region {
	NSString *event = [NSString stringWithFormat:@"didExitRegion %@ at %@\n", region.identifier, [NSDate date]];
    
	NSLog(@"AppLocationMgr: didExitRegion:  regionIdentifier: %@", region.identifier);
	
    [self updateWithEvent:event Type:2 Identifier:region.identifier];
    
}


- (void)locationManager:(CLLocationManager *)manager monitoringDidFailForRegion:(CLRegion *)region withError:(NSError *)error {
	NSString *event = [NSString stringWithFormat:@"monitoringDidFailForRegion %@: %@\n", region.identifier, error];
    
	NSLog(@"AppLocationMgr: monitoringDidFailForRegion: regionIdentifier: %@", region.identifier);
    
	[self updateWithEvent:event Type:0 Identifier:region.identifier];
}

- (void) locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status {
    
}

- (void) locationManager:(CLLocationManager *)manager didStartMonitoringForRegion:(CLRegion *)region {
    NSLog(@"%@", [NSString stringWithFormat:@"didStartMonitoringForRegion %@\n", region.identifier]);
}

/*
 This method adds the region event to the events array and updates the icon badge number.
 */
- (void)updateWithEvent:(NSString *)event Type:(int) type Identifier:(NSString *) identifier {
	// Add region event to the updates array.
	NSLog(@"Monitor: Event:%@", event);
	
    NSLog(@"AppLocationMgr: Distance:%f Accuracy:%f", self.locationManager.desiredAccuracy, self.locationManager.distanceFilter);
    
    switch (type) {
        case 0: { // MONITORING FAILED FOR REGION
            // Try to display a message for testing
            Location *loc = [CoreDataOperations getLocationByIdentifier:identifier];
            
            NSString *locName = APP_EMPTY;
            if ( loc) {
                locName = loc.name;
            }
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Note" message:@"Adding more location now allowed, reached maximum limit!" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
            [alert show];
            NSLog(@"Moniotring Failed for Loc Name:%@ Region Identifier: %@", locName, identifier);
            break;
        }
        case 1: { // DID ENTER CASE - Start a new log entry
            
            // Inform Flurry
            [Flurry logEvent:@"Region: DID_ENTER"];

            // Handling of LocationLog Entries
            [self processLocationLogForEnterCase:identifier];
            
            // Logic to handle Routes
            [self processRoutesForEnterCase: identifier];
            
            // TRIP HANDLING MECHANISM
            [self processTrip_EnterRegion_EndTrip:identifier EnterRegionEndTripTime:[NSDate date]]; // Use current time, allowing historical entry else where so giving date here

            break;
        }
        case 2: { // DID EXIT CASE - Update an existing entry with exit time
            
            // Inform Flurry
            [Flurry logEvent:@"Region: DID_EXIT"];
            
            // Handling for LocationLog Entries
            [self processLocationLogForExitCase:identifier];
            
            // Handling for Route Support
            [self processRoutesForExitCase: identifier];
            
            // TRIP HANDLING MECHANISM
            [self processTrip_ExitRegion_StartTrip:identifier ExitRegionStartTripTime:[NSDate date]]; // Use current time, allowing historical entry else where so giving date here

            break;
        }
        default:
            break;
    }
    
	// Update the icon badge number.
	[UIApplication sharedApplication].applicationIconBadgeNumber++;
}

-(void) processLocationLogForEnterCase:(NSString*) identifier {
    
    // Avoid duplicate event fire, gap of this event and previous one is more than 5 secounds (DUP_EVENT_GAP).
    // For consistency ensure this event has a valid identifier
    NSDate *firedAt = [NSDate date];
    
    // Add a new LocationLog entery each time you ENTER a region
    LocationLog *locLog = [CoreDataOperations addLocationLogWithDidEnterTime:firedAt WithDidExitTime:nil WithDidEnterLatitude:0.0 WithDidEnterLongitude:0.0 WithDidExitLatitude:0.0 WithDidExitLongitude:0.0 WithLocnIdentifier:identifier WithSortOrder:0 ManualEntry:NO];
    
    
    // Show a User notification
    [AppUtilities showLocalNotification:[NSString stringWithFormat:@"Around: %@", locLog.location.name]];
    
    // Record this time for checking subsequent duplicates
    self.lastRegionTimestamp = firedAt;
}

-(void) processLocationLogForExitCase:(NSString*) identifier {
    NSDate *firedAt = [NSDate date];
    
    // Find last entry of location associated with this monitoring region and update EXIT time there
    LocationLog *lastEntry = [CoreDataOperations getLatestLocationLogByIdentifier:identifier];
    if (lastEntry) { // Update
        lastEntry.did_exit    = firedAt;
        lastEntry.modified_on = firedAt;
        
        [CoreDataOperations saveContext]; // Save
        
        // Notify User of Save
        [AppUtilities showLocalNotification:[NSString stringWithFormat:@"Left From: %@", lastEntry.location.name]];
        
        // Record this time for checking subsequent duplicates
        
        
        //                NSLog(@"EXIT: %@ %@ %i", lastEntry.did_enter, firedAt, [self getSecondsGapBetween:lastEntry.did_enter EndDate:firedAt]);
    }
    else { // Either there is no entry requing time update or it is a duplicate event
        LocationLog *locLog = [CoreDataOperations addLocationLogWithDidEnterTime:nil WithDidExitTime:firedAt WithDidEnterLatitude:0.0 WithDidEnterLongitude:0.0 WithDidExitLatitude:0.0 WithDidExitLongitude:0.0 WithLocnIdentifier:identifier   WithSortOrder:0 ManualEntry:NO];
        
        
        //Show User notification
        [AppUtilities showLocalNotification:[NSString stringWithFormat:@"Left From: %@", locLog.location.name]];
        
        // Record this time for checking subsequent duplicates
        self.lastRegionTimestamp = firedAt;
        
        
        //                NSLog(@"EXIT2: %@ %@ %i", lastEntry.did_enter, firedAt, [self getSecondsGapBetween:lastEntry.did_enter EndDate:firedAt]);
    }

}

-(void) processRoutesForEnterCase:(NSString*) identifier {
    NSDate *firedAt = [NSDate date];
    
    self.lastRegionTimestamp = firedAt;
    Location *loc = [CoreDataOperations getLocationByIdentifier:identifier];
    bool foundMatch = NO;
    
    NSLog(@"Routes Associated to this Location: %lu", (unsigned long)[loc.toRouteS count]);
    
    // For Location ENTERED - find routes where it an END location
    for (Route *route in loc.toRouteS) { // Process every route using this location as end route
        NSLog(@"Route: From:%@ To:%@", route.fromLocation.name, route.toLocation.name);
        
        // Find a route find entries which don't have entries with start time but not end time
        NSArray *routeLogSArr = [CoreDataOperations getUnFinishedRouteLogs:route];
        NSLog(@"Unfinished Entries Found: %lu", (unsigned long)[routeLogSArr count]);
        
        for (RouteLog *routeLogItem in routeLogSArr) {
            NSLog(@"RouteLog: ItemRoute - From: %@ To:%@ Start:%@ End:%@ ", routeLogItem.route.fromLocation.name, routeLogItem.route.toLocation.name, routeLogItem.left_at, routeLogItem.reached_at);
            
            // for open entry if its route matches update the end time. Else it is stray entry delete it.
            if ( routeLogItem.route.fromLocation == route.fromLocation && routeLogItem.route.toLocation == route.toLocation) {
                // We have found a match to which the route matches and can be used to complete the reachedAtTime
                routeLogItem.reached_at = firedAt;
                foundMatch = YES;
                NSLog(@"REACHED: UPDATE ROUTE LOG: Match Found: Route:%@ to %@ leftAt:%@ ReachedAT:%@", route.fromLocation.name, route.toLocation.name, routeLogItem.left_at, routeLogItem.reached_at );
                [CoreDataOperations saveContext];
            }
            else {
                // We no longer need to retain the stray entry as this route is not the one user
                [CoreDataOperations deleteItemByEntityName:@"RouteLog" Item:routeLogItem];
                NSLog(@"REACHED: DELETE ROUTE LOG: Route:%@ to %@ leftAt:%@ ReachedAT:%@", route.fromLocation.name, route.toLocation.name, routeLogItem.left_at, routeLogItem.reached_at);
            }
        }
        
        // Finally Clean the stray entries as the delete in above logic does not clear the stray entries
        NSArray *strayEntries = [CoreDataOperations getUnFinishedRouteLogs:nil];
        for (RouteLog *item in strayEntries) {
            [CoreDataOperations deleteItemByEntityName:@"RouteLog" Item:item];
            NSLog(@"REACHED: DELETE ROUTE LOG: Route:%@ to %@ leftAt:%@ ReachedAT:%@", route.fromLocation.name, route.toLocation.name, item.left_at, item.reached_at);
        }
        
        /*
         // ADDING A STRAY ENTRYING
         if (!foundMatch) { // If no match is found at least insert an entry for visibility
         [CoreDataOperations addRouteLogWithRoute:route WithLeftAt:nil WithReachedT:firedAt WithLeftAtCoordinate:nil WithReachedAtCoordinate:nil WithCreatedOn:nil WithModifiedOn:nil];
         NSLog(@"REACHED: ADD ROUTE LOG: %@ to %@ LeftAt:%@ ReachedAT:%@", route.fromLocation.name, route.toLocation.name, nil, firedAt);
         }
         */
    }
}
-(void) processRoutesForExitCase:(NSString*) identifier {
    NSDate *firedAt = [NSDate date];
    Location *loc = [CoreDataOperations getLocationByIdentifier:identifier];
    for (Route *route in loc.fromRouteS) {
        [CoreDataOperations addRouteLogWithRoute:route WithLeftAt:firedAt WithReachedT:nil WithLeftAtCoordinate:nil WithReachedAtCoordinate:nil WithCreatedOn:nil WithModifiedOn:nil];
        NSLog(@"STARTED: ADD ROUTE LOG: %@ to %@ LeftAt:%@ ReachedAT:%@", route.fromLocation.name, route.toLocation.name, firedAt, nil );
        
    }
}

-(void) processTrip_EnterRegion_EndTrip:(NSString*)identifier EnterRegionEndTripTime:(NSDate*)enterRegionEndTripTime {
    // TRIP HANDLING MECHANISM
    
    // Case A: First record: NIL - ENTER, no record of trip and we get an Enter to a region
    // Case B: First record: NIL - EXIT , no record of trip and we get an EXIT from a region
    // Case C1: Second record: ENTER REGION (on top of NIL - ENTER)
    // Case C2: Second record: ENTER REGION (on top of NIL - EXIT)
    // Case D1: Second record: EXIT Region  (on top of NIL - ENTER)
    // Case D2: Second record: EXIT Region  (on top of NIL - EXIT)
    // Case E1: Third record  : ENTER REGION (on top of ENTER - NIL)
    // Case E2: Third record  : ENTER REGION (on top of EXIT - NIL)
    // Case E3: Third record  : EXIT REGION (on top of ENTER - NIL)
    // Case E4: Third record  : EXIT REGION (on top of EXIT - NIL)
    
    // Enter a region imply reached, see if there is an existing entry and close a trip, else close half trip
    Location *enterLocation = [CoreDataOperations getLocationByIdentifier:identifier];
    
    if (enterLocation == nil) { // Unknown Location, not expected, log as unknown
        NSLog(@"AppLocationMgr: ENTR LOCN: TRIP  END is clean but to Location not found SKIP ENTER REGION");
        /*
         [CoreDataOperations addTrip_IsFinished:YES
         FromLocn:nil // As we have no source location found set it as nil
         ToLocn:nil // Not assigned yet
         FromLocName:nil // As we marking closed give it a label
         FromAt:nil
         FromRegionIdentifier:nil
         ToLocName:@"NOT_FOUND"
         ToAt:[NSDate date]
         ToRegionIdentifier:@"0, 0"
         CreatedOn:[NSDate date] ModifiedOn:[NSDate date]];
         */
    }
    else {
        
        NSArray *openTrips = [CoreDataOperations getOpenTrips];
        if (openTrips == nil && [openTrips count] == 0) { // There are no trips open to close, so create an unknown start to known END trip
            NSLog(@"AppLocationMgr: ENTR LOCN: TRIP  END Without START:");
            
            [CoreDataOperations addTrip_IsFinished:YES
                                          FromLocn:nil // As we have no source location found set it as nil
                                            ToLocn:enterLocation
                                       FromLocName:APP_LOCN_UNKNOWN //@"UNKNOWN START" // As we marking closed give it a label
                                            FromAt:enterRegionEndTripTime
                              FromRegionIdentifier:@"0, 0"
                                         ToLocName:enterLocation.name
                                              ToAt:enterRegionEndTripTime
                                ToRegionIdentifier:enterLocation.region_identifier                                         CreatedOn:enterRegionEndTripTime ModifiedOn:[NSDate date]];
        }
        else {
            // check if there is only 1 trip to close - ACTUAL CASE TO BE HANDLED
            if ( [openTrips count] == 1 ) {
                NSLog(@"AppLocationMgr: ENTR LOCN: TRIP END is clean, End Last Trip");
                
                Trip *trip = [openTrips objectAtIndex:0];
                trip.finished   = [NSNumber numberWithBool:YES];
                trip.toLocation = enterLocation;
                trip.to_locname = enterLocation.name;
                trip.to_at      = enterRegionEndTripTime;
                trip.to_region_identifier = enterLocation.region_identifier;
                trip.modified_on = [NSDate date];
                
                // Add/Update TripSummary
                [CoreDataOperations addUpdateTripSummaryWithFromLocation:trip.fromLocation
                                                              ToLocation:trip.toLocation
                                                      FromLocnIdentifier:trip.fromLocation.region_identifier
                                                         ToLocnIdentifer:trip.toLocation.region_identifier
                                                            FromLocnName:trip.fromLocation.name
                                                              ToLocnName:trip.toLocation.name
                                                               RouteName:trip.tripName
                                                            TimeDuration:[trip.to_at timeIntervalSinceDate:trip.from_at]
                                                             FromLocnImg:trip.fromLocation.image_path
                                                               ToLocnImg:trip.toLocation.image_path
                                                                  Extra1:nil TotalDistance:0];
                [CoreDataOperations saveContext];
            }
            else { // else there are stray entries which needs to be closed as well
                NSLog(@"AppLocationMgr: ENTR LOCN: TRIP END Last Trip closing other open trips");
                for (Trip *trip in openTrips) {
                    
                    trip.finished = [NSNumber numberWithBool:YES];
                    
                    NSLog(@"AppLocationMgr: ENTR LOCN: Clear Pending Trip: FromLocation:%@ FromCoord:%@ FromTime:%@ ToLocation:%@ ToTime:%@ ToCoordinate:%@"
                         ,APP_LOCN_UNKNOWN //, @"UNKNOWN START"
                         , trip.from_at, trip.from_region_identifier, [trip.toLocation name], trip.to_at, trip.to_region_identifier);
                }
                
                [CoreDataOperations saveContext];
            }
        }
    }
}
-(void) processTrip_ExitRegion_StartTrip: (NSString *)identifier ExitRegionStartTripTime:exitRegionStartTripTime {
    // Exit from a region is START a TRIP
    Location *exitLocn = [CoreDataOperations getLocationByIdentifier:identifier];
    if (exitLocn == nil) { // Unknown Location, not expected, log as unknown
        NSLog(@"AppLocationMgr: EXIT LOCN: TRIP START is clean but from Location not found, SKIP EXIT REGION");
        /*
         [CoreDataOperations addTrip_IsFinished:YES
         FromLocn:nil // As we have no source location found set it as nil
         ToLocn:nil // Not assigned yet
         FromLocName:@"NOT_FOUND" // As we marking closed give it a label
         FromAt:[NSDate date]
         FromRegionIdentifier:@"0, 0"
         ToLocName:nil
         ToAt:nil
         ToRegionIdentifier:nil
         CreatedOn:[NSDate date] ModifiedOn:[NSDate date]];
         */
    }
    else { // Exit Location found, treat as START of a trip
        Location *fromLocation = exitLocn;
        
        NSArray *openTrips = [CoreDataOperations getOpenTrips];
        
        if (openTrips != nil && [openTrips count] > 0) { // There should not be any open trips
            NSLog(@"AppLocationMgr: EXIT LOCN: TRIP START Not clean, closing the open tripSSSS");
            
            for (Trip *trip in openTrips) {
                
                trip.finished    = [NSNumber numberWithBool:YES];
                trip.to_locname  = APP_LOCN_UNKNOWN; // @"CLOSEDTRIP"; To make it simple on UI using a common work
                trip.to_at       = exitRegionStartTripTime; // don't know the time so using what we know
                trip.to_region_identifier = @"0, 0";
                trip.modified_on = [NSDate date];
                
                NSLog(@"AppLocationMgr: EXIT LOCN: Clear Pending Trip: FromLocation:%@ FromCoord:%@ FromTime:%@ ToLocation:%@ ToTime:%@ ToCoordinate:%@", [trip.fromLocation name], trip.from_at, trip.from_region_identifier
                     , APP_LOCN_UNKNOWN //, @"CLOSEDTRIP"
                     , trip.to_at, trip.to_region_identifier);
            }
            
            [CoreDataOperations saveContext];
        }
        // NOW ADD The starting trip details
        NSLog(@"AppLocationMgr: EXIT LOCN: TRIP START is clean: Start The Trip From:%@ TO:<Blank>", fromLocation.name );
        
        [CoreDataOperations addTrip_IsFinished:NO
                                      FromLocn:fromLocation // As we have no source location found set it as nil
                                        ToLocn:nil // Not assigned yet
                                   FromLocName:fromLocation.name
                                        FromAt:exitRegionStartTripTime
                          FromRegionIdentifier:fromLocation.region_identifier
                                     ToLocName:nil
                                          ToAt:nil
                            ToRegionIdentifier:nil
                                     CreatedOn:exitRegionStartTripTime ModifiedOn:[NSDate date]];
        
    }
}


// Temporarily added to see if by checking the time in entry exit we can ignore entries, coming due to network switch from wifi to cellular
-(NSUInteger) getSecondsGapBetween:(NSDate*) startDate EndDate:(NSDate*)endDate {
    
    NSCalendar *gregorianCalendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSUInteger unitFlags = NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit | NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit;
    NSDateComponents *components = [gregorianCalendar components:unitFlags
                                                        fromDate:startDate
                                                          toDate:endDate
                                    
                                                         options:0];
    return  [components second];
}

- (bool) isDuplicate:(int) eventType LastEventTime:(NSDate *) timeStamp RegionID:(NSString *)identifier {
    switch (eventType) {
        case 1:
        {
            
        }
            break;
        case 2:
        {
//             if (abs(timeS) > DUP_EVENT_GAP) { // it does not seem to be a duplicate entry so log it anyway for debug and
                 return YES;
//             }
            break;
        }
        default:
            break;
    }
    return nil;
}

-(CLLocationCoordinate2D) getLocationCoordinate {
    return self.locationManager.location.coordinate;
}

#pragma mark - region handling api's
-(void) addRegion :( CLRegion *) region {
    if (region)
//        [self stopMonitoringRegion:region];
        [self.keyIdentiferValRegion setObject:region forKey:region.identifier];
}

-(CLRegion *) getRegion: (NSString *) regionIdentifier {
    CLRegion *region = [self.keyIdentiferValRegion objectForKey:regionIdentifier];
    return region;
}

-(NSArray *) getAllRegions {
    return [self.keyIdentiferValRegion allValues];
}

- (NSInteger) getAllRegionsCount {
    return  [self.keyIdentiferValRegion count];
}

-(bool) regionIsMonitored: (NSString *) regionIdentifier {
    CLRegion *region = [self getRegion:regionIdentifier];
    if (region == nil ) {
        return NO;
    }
    else {
        return YES;
    }
}

-(void) startMonitoringRegion: (CLRegion *) region {
    // Check if the region is already existing and we again starting up. If so remove the first one and then add the new one.
    CLRegion *existingRegion = [self getRegion:region.identifier];
    if (existingRegion) { // Make an attemp to stop older monitoring and start a newer one
        [self stopMonitoringRegion:region];
    }

    [self.keyIdentiferValRegion setObject:region forKey:region.identifier]; //before starting add it to local cache
    [self.locationManager startMonitoringForRegion:region];
}

-(void) stopMonitoringRegion: (CLRegion *) region {
    CLRegion *existingRegion = [self getRegion:region.identifier];
    if (existingRegion) { // check if the object exists else it might crash
        [self.keyIdentiferValRegion removeObjectForKey:region.identifier];
    }
    
    [self.locationManager stopMonitoringForRegion:region];
}

#pragma mark - Check Service Status

// check to see if Location Services is enabled, there are two state possibilities:
// 1) disabled for entire device, 2) disabled just for this app
-(BOOL) showAlertToUserOnServiceStatus {
    
    bool showAlert = YES;
    
    NSString *causeStr = nil;
    // check whether location services are enabled on the device
    if ([CLLocationManager locationServicesEnabled] == NO) {
        
        causeStr = @"device";
    }
    // check the application’s explicit authorization status:
    else if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusDenied || [CLLocationManager authorizationStatus] == kCLAuthorizationStatusRestricted)  { // kCLAuthorizationStatusDenied
        causeStr = @"app";
    }
    else {
        showAlert = NO;
    }
    
    if (causeStr != nil)  {
        NSString *alertMessage = [NSString stringWithFormat:@"Please refer to \"Settings\" app to turn on Location Services for this %@.", causeStr];
        
        UIAlertView *servicesDisabledAlert = [[UIAlertView alloc] initWithTitle:@"Location Services Disabled"
                                                                        message:alertMessage
                                                                       delegate:nil
                                                              cancelButtonTitle:@"OK"
                                                              otherButtonTitles:nil];
        [servicesDisabledAlert show];
    }
    
    return showAlert;
}

-(void) logLocationMgrAttributes {
    
    NSLog(@"AppLocationMgr: Distance:%f Accuracy:%f", self.locationManager.desiredAccuracy, self.locationManager.distanceFilter);
}

@end
