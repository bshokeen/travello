//
//  AppLocationMgr.h
//  Travello
//
//  Created by Balbir Shokeen on 2013/09/04.
//  Copyright (c) 2013 Balbir Shokeen. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>
#import <MapKit/MapKit.h>
//#import "AppUtilities.h"
#import "CoreDataOperations.h"


@interface AppLocationMgr : NSObject <CLLocationManagerDelegate>

// Implementing Search
@property (nonatomic, strong) NSArray *mapItemList;

// Track all regions in cache for quick access usage in the Locations view for showing which locations are tracked or not
@property (nonatomic, strong) NSMutableDictionary *keyIdentiferValRegion;

//- (void) searchAddress: (NSString *)searchAddress AroundLocation:(CLLocationCoordinate2D) coordinate;
//- (void) startSearch: (NSString *)queryString AroundLocation:(CLLocationCoordinate2D) coordinate;

-(CLLocationCoordinate2D) getLocationCoordinate;

// Region Monitoring related
-(void) addRegion :( CLRegion *) region;
-(CLRegion *) getRegion: (NSString *) regionIdentifier;
-(NSArray *) getAllRegions;
- (NSInteger) getAllRegionsCount;
-(bool) regionIsMonitored: (NSString *) regionIdentifier;

-(void) startMonitoringRegion: (CLRegion *) region;
-(void) stopMonitoringRegion: (CLRegion *) region;

-(BOOL) showAlertToUserOnServiceStatus;

// TO enable logging manual entry for trip, exposing this API
-(void) processTrip_EnterRegion_EndTrip:(NSString*)identifier EnterRegionEndTripTime:(NSDate*)enterRegionEndTripTime;
-(void) processTrip_ExitRegion_StartTrip: (NSString *)identifier ExitRegionStartTripTime:exitRegionStartTripTime;

-(void) logLocationMgrAttributes;
@end
